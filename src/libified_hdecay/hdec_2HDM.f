!     Extracted from hdecay 6.60 on 12.03.2021
      subroutine hdec_2hdm(tgbet)
      implicit double precision (a-h,o-z)
      double precision lamb_hdec
      complex*16 cfacq_hdec,cfacsq_hdec,ckofq_hdec,ckofq,cfacq,cfacq0
      integer ivegas(4)
      dimension xx(4),yy(4)
      dimension amchar(2),amneut(4),xmneut(4),
     .          ac1(2,2),ac2(2,2),ac3(2,2),
     .          an1(4,4),an2(4,4),an3(4,4),
     .          acnl(2,4),acnr(2,4),
     .          amst(2),amsb(2),amsl(2),
     .          amsu(2),amsd(2),amse(2),amsn(2),amsn1(2),
     .          gltt(2,2),glbb(2,2),glee(2,2),
     .          ghtt(2,2),ghbb(2,2),ghee(2,2),
     .          gctb(2,2),gcen(2,2)
      dimension gmst(2),gmsb(2),gmsl(2),gmsu(2),gmsd(2),gmse(2),
     .          gmsn(2),gmsn1(2)
      dimension hlbrsc(2,2),hlbrsn(4,4),hhbrsc(2,2),
     .          hhbrsn(4,4),habrsc(2,2),habrsn(4,4),hcbrsu(2,4),
     .          hhbrst(2,2),hhbrsb(2,2),hcbrstb(2,2)
      dimension whlch(2,2),whlne(4,4),whhch(2,2),whhne(4,4),
     .          whach(2,2),whane(4,4),whccn(2,4),
     .          whhst(2,2),whhsb(2,2),whhstau(2,2),whcstb(2,2),
     .          whlst(2,2),whlsb(2,2),whlstau(2,2)
      dimension whhst0(2,2),whhsb0(2,2)
      dimension whlgd(4),whcgd(2),whhgd(4),whagd(4)
      dimension agdl(4),agda(4),agdh(4),agdc(2)
      dimension slhaneut(4),slhaxneut(4),slhachar(2),slhau(2,2),
     .          slhav(2,2),slhaz(4,4),xmchar(2)
      dimension xglbb(2,2),xghbb(2,2),xgctb(2,2)
      dimension xhgg(3),xhqq(3)
      complex*16 cf,cg,ci1,ci2,ca,cb,ctt,ctb,ctc,ctw,clt,clb,clc,clw,
     .           cat,cab,cac,caw,cah,cth,clh,cx1,cx2,cax1,cax2,ctl,cal,
     .           csl,csq,csb1,csb2,cst1,cst2,csl1,csl2,
     .           cxl,cxq,cxb1,cxb2,cxt1,cxt2,cxl1,cxl2
      complex*16 csel,cser,csul,csur,csdl,csdr,
     .           cxel,cxer,cxul,cxur,cxdl,cxdr
      complex*16 cat0,cab0,cac0,cxul0,cxur0,cxdl0,cxdr0,cxb10,cxb20,
     .           cxt10,cxt20
      complex*16 cle
      complex*16 cttp,ctbp,ctep,cltp,clbp,clep,catp,cabp,caep
      complex*16 cctt0,ctt1,ctt2,cat1,cat2,ctrun,cdct,cdcta
      complex*16 catp0,cabp0
      complex*16 cat00,cab00,cac00,cal00,caw00
      complex*16 catp00,cabp00,caep00
      complex*16 ctot,cdt,cdb,ctot0,cdt0,cdb0
      common/hmass_hdec/amsm,ama,aml,amh,amch,amar
      common/hmassr_hdec/amlr,amhr
      common/chimass_hdec/amchi
      common/masses_hdec/ams,amc,amb,amt
      common/msbar_hdec/amcb,ambb
      common/als_hdec/xlambda,amc0,amb0,amt0,n0
      common/param_hdec/gf,alph,amtau,ammuon,amz,amw
      common/ckmpar_hdec/vtb,vts,vtd,vcb,vcs,vcd,vub,vus,vud
      common/break_hdec/amel,amer,amsq,amur,amdr,al,au,ad,amu,am2
      common/breakglu_hdec/amglu
      common/wzwdth_hdec/gamc0,gamt0,gamt1,gamw,gamz
      common/model_hdec/imodel
      common/onshell_hdec/ionsh,ionwz,iofsusy
      common/oldfash_hdec/nfgg
      common/flag_hdec/ihiggs,nnlo,ipole
      common/sm4_hdec/amtp,ambp,amnup,amep,ism4,iggelw
      common/fermiophobic_hdec/ifermphob
      common/widthsm_hdec/smbrb,smbrl,smbrm,smbrs,smbrc,smbrt,smbrg,
     .               smbrga,smbrzga,smbrw,smbrz,smwdth
      common/widthsm4_hdec/smbrnup,smbrep,smbrbp,smbrtp
      common/widtha_hdec/abrb,abrl,abrm,abrs,abrc,abrt,abrg,abrga,
     .              abrzga,abrz,awdth
      common/widthhl_hdec/hlbrb,hlbrl,hlbrm,hlbrs,hlbrc,hlbrt,hlbrg,
     .               hlbrga,hlbrzga,hlbrw,hlbrz,hlbra,hlbraz,hlbrhw,
     .               hlwdth
      common/widthhh_hdec/hhbrb,hhbrl,hhbrm,hhbrs,hhbrc,hhbrt,hhbrg,
     .               hhbrga,hhbrzga,hhbrw,hhbrz,hhbrh,hhbra,hhbraz,
     .               hhbrhw,hhwdth
      common/widthhc_hdec/hcbrb,hcbrl,hcbrm,hcbrbu,hcbrs,hcbrc,hcbrt,
     .               hcbrw,hcbra,hcwdth
      common/wisusy_hdec/hlbrsc,hlbrsn,hhbrsc,hhbrsn,habrsc,habrsn,
     .              hcbrsu,hlbrcht,hhbrcht,habrcht,hlbrnet,hhbrnet,
     .              habrnet,hcbrcnt,hlbrsl,hhbrsl,hcbrsl,habrsl,habrst,
     .              habrsb,hhbrsq,hhbrst,hhbrsb,hhbrsqt,hcbrsq,hcbrstb,
     .              hcbrsqt,hlbrsq,hlbrsqt
      common/wisfer_hdec/bhlslnl,bhlslel,bhlsler,bhlsqul,bhlsqur,
     .              bhlsqdl,bhlsqdr,bhlst(2,2),bhlsb(2,2),bhlstau(2,2),
     .              bhhslnl,bhhslel,bhhsler,bhhsqul,bhhsqur,bhhsqdl,
     .              bhhsqdr,bhhst(2,2),bhhsb(2,2),bhhstau(2,2),
     .              bhastau,bhasb,bhast,
     .              bhcsl00,bhcsl11,bhcsl21,bhcsq,bhcstb(2,2)
      common/smass_hdec/amneut,xmneut,amchar,amst,amsb,amsl,
     .              amsu,amsd,amse,amsn,amsn1
      common/coup_hdec/gat,gab,glt,glb,ght,ghb,gzah,gzal,
     .            ghhh,glll,ghll,glhh,ghaa,glaa,glvv,ghvv,
     .            glpm,ghpm,b,a
      common/goldst_hdec/axmpl,axmgd,igold
      common/wigold_hdec/hlbrgd,habrgd,hhbrgd,hcbrgd
      common/slha_gaug_hdec/slhaneut,slhaxneut,slhachar,slhau,slhav,
     .                      slhaz,xmchar
      common/slha_vals_hdec/islhai,islhao
      common/breakscale_hdec/susyscale
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      common/david/qsusy1,qsusy2,loop
      common/sqnlo_hdec/ymsb(2),styb,ctyb,ylbb(2,2),yhbb(2,2),yabb,
     .                  ymst(2),styt,ctyt,yltt(2,2),yhtt(2,2),yatt
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      common/cpsm_hdec/cpw,cpz,cptau,cpmu,cpt,cpb,cpc,cps,
     .                 cpgaga,cpgg,cpzga,icoupelw
      common/cpsm4_hdec/cptp,cpbp,cpnup,cpep
c mmm changed 21/8/13
      common/thdm_hdec/tgbet2hdm,alph2hdm,amhl2hdm,amhh2hdm,
     .     amha2hdm,amhc2hdm,am12sq,a1lam2hdm,a2lam2hdm,a3lam2hdm,
     .     a4lam2hdm,a5lam2hdm,itype2hdm,i2hdm,iparam2hdm
      common/thdm_coup_hdec/gllep,ghlep,galep
      common/width_hc_add/hcbrcd,hcbrts,hcbrtd
      common/width_2hdm/hcbrwhh,hhbrchch,hlbrchch,abrhhaz,abrhawphm
      common/decparameters/amhi,amhj,amhk,gamtotj,gamtotk
      common/omit_elw_hdec/ioelw

      external hvhinteg
c end mmm changed 21/8/13
      hvv(x,y)= gf/(4.d0*pi*dsqrt(2.d0))*x**3/2.d0*beta_hdec(y)
     .            *(1.d0-4.d0*y+12.d0*y**2)
      aff(x,y)= gf/(4*pi*dsqrt(2.d0))*x**3*y*(beta_hdec(y))
      hff(x,y)= gf/(4*pi*dsqrt(2.d0))*x**3*y*(beta_hdec(y))**3
      cff(z,tb,x,y)= gf/(4*pi*dsqrt(2.d0))*z**3*lamb_hdec(x,y)
     .              *((1.d0-x-y)*(x*tb**2+y/tb**2)-4.d0*x*y)
      hv(v)=3.d0*(1.d0-8.d0*v+20.d0*v**2)/dsqrt((4.d0*v-1.d0))
     .      *dacos((3.d0*v-1.d0)/2.d0/dsqrt(v**3))
     .      -(1.d0-v)*(47.d0/2.d0*v-13.d0/2.d0+1.d0/v)
     .      -3.d0/2.d0*(1.d0-6.d0*v+4.d0*v**2)*dlog(v)
      hvh(x,y)=0.25d0*( (1-x)*(-2+4*x-2*x**2+9*y+9*x*y-6*y**2)
     .        /(3*y)-2*(1-x-x**2+x**3-3*y-2*x*y-3*x**2*y+3*y**2
     .        +3*x*y**2-y**3)*(-pi/2- datan((1-2*x+x**2-y-x*y)/
     .         ((1-x)*dsqrt(-1.d0+2*x+2*y-(x-y)**2))))/dsqrt(-1.d0
     .         +2*x-(x-y)**2+2*y)-(1+x**2-2*y-2*x*y+y**2)*dlog(x))

c     hvh(x,y)=0.25d0*( (1-x)*(5.d0*(1.d0+x)-4.d0*y-2.d0/y*
c    .     (-1.d0+2.d0*x+2.d0*y-(x-y)**2))/3
c    .        -2*(1-x-x**2+x**3-3*y-2*x*y-3*x**2*y+3*y**2
c    .        +3*x*y**2-y**3)*(-pi/2- datan((1-2*x+x**2-y-x*y)/
c    .         ((1-x)*dsqrt(-1.d0+2*x+2*y-(x-y)**2))))/dsqrt(-1.d0
c    .         +2*x-(x-y)**2+2*y)-(1+x**2-2*y-2*x*y+y**2)*dlog(x))

      qcd0(x) = (1+x**2)*(4*sp_hdec((1-x)/(1+x))+2*sp_hdec((x-1)/(x+1))
     .        - 3*dlog((1+x)/(1-x))*dlog(2/(1+x))
     .        - 2*dlog((1+x)/(1-x))*dlog(x))
     .        - 3*x*dlog(4/(1-x**2)) - 4*x*dlog(x)
      hqcdm(x)=qcd0(x)/x+(3+34*x**2-13*x**4)/16/x**3*dlog((1+x)/(1-x))
     .        + 3.d0/8/x**2*(7*x**2-1)
      aqcdm(x)=qcd0(x)/x + (19+2*x**2+3*x**4)/16/x*dlog((1+x)/(1-x))
     .        + 3.d0/8*(7-x**2)
      hqcd(x)=(4.d0/3*hqcdm(beta_hdec(x))
     .        +2*(4.d0/3-dlog(x))*(1-10*x)/(1-4*x))*ash/pi
c    .       + (29.14671d0 + x*(-93.72459d0+12)
     .       + (29.14671d0
     .         +ratcoup*(1.570d0 - 2*dlog(higtop)/3
     .                                     + dlog(x)**2/9))*(ash/pi)**2
     .       + (164.14d0 - 25.77d0*5 + 0.259d0*5**2)*(ash/pi)**3
     .       +(39.34d0-220.9d0*5+9.685d0*5**2-0.0205d0*5**3)*(ash/pi)**4
      aqcd(x)=(4.d0/3*aqcdm(beta_hdec(x))
     .        +2*(4.d0/3-dlog(x))*(1-6*x)/(1-4*x))*ash/pi
     .       + (29.14671d0 + ratcoup*(23/6.d0 - dlog(higtop)
     .                                     + dlog(x)**2/6))*(ash/pi)**2
     .       + (164.14d0 - 25.77d0*5 + 0.259d0*5**2)*(ash/pi)**3
     .       +(39.34d0-220.9d0*5+9.685d0*5**2-0.0205d0*5**3)*(ash/pi)**4
      qcdh(x)=1.d0+hqcd(x)
      qcda(x)=1.d0+aqcd(x)
      tqcdh(x)=1.d0+4.d0/3*hqcdm(beta_hdec(x))*ash/pi
      tqcda(x)=1.d0+4.d0/3*aqcdm(beta_hdec(x))*ash/pi
      qcdc(x,y)=1.d0+4/3.d0*ash/pi*(9/4.d0 + (3-2*x+2*y)/4*dlog(x/y)
     .         +((1.5d0-x-y)*lamb_hdec(x,y)**2+5*x*y)/2/lamb_hdec(x,y)
     .         /(1-x-y)*dlog(xi_hdec(x,y)*xi_hdec(y,x))
     .         + bij_hdec(x,y))
     .         + ash/pi*(2*(4/3.d0-dlog(x))
     .         - (x*2*(4/3.d0-dlog(x)) + y*2*(4/3.d0-dlog(y)))/(1-x-y)
     .         - (x*2*(4/3.d0-dlog(x))*(1-x+y)
     .           +y*2*(4/3.d0-dlog(y))*(1+x-y))/lamb_hdec(x,y)**2)
      qcdci(x,y)=1.d0+4/3.d0*ash/pi*(3 + (y-x)/2*dlog(x/y)
     .         +(2*(1-x-y)+lamb_hdec(x,y)**2)/2/lamb_hdec(x,y)
     .         *dlog(xi_hdec(x,y)*xi_hdec(y,x))
     .         + bij_hdec(x,y))
     .         + ash/pi*(2*(4/3.d0-dlog(x)) + 2*(4/3.d0-dlog(y))
     .         - (x*2*(4/3.d0-dlog(x))*(1-x+y)
     .           +y*2*(4/3.d0-dlog(y))*(1+x-y))/lamb_hdec(x,y)**2)
      qcdcm(x,y)=1.d0+4/3.d0*ash/pi*(9/4.d0 + (3-2*x+2*y)/4*dlog(x/y)
     .         +((1.5d0-x-y)*lamb_hdec(x,y)**2+5*x*y)/2/lamb_hdec(x,y)
     .         /(1-x-y)*dlog(4*x*y/(1-x-y+lamb_hdec(x,y))**2)
     .         + bij_hdec(x,y))
      qcdcmi(x,y)=1.d0+4/3.d0*ash/pi*(3 + (y-x)/2*dlog(x/y)
     .         +(2*(1-x-y)+lamb_hdec(x,y)**2)/2/lamb_hdec(x,y)
     .         *dlog(4*x*y/(1-x-y+lamb_hdec(x,y))**2)
     .         + bij_hdec(x,y))
c------------------------------------------------------------------------
c--running x yukawa coupling
      qcdcm1(x,y)=1.d0+4/3.d0*ash/pi*(9/4.d0 + (3-2*x+2*y)/4*dlog(x/y)
     .         +((1.5d0-x-y)*lamb_hdec(x,y)**2+5*x*y)/2/lamb_hdec(x,y)
     .         /(1-x-y)*dlog(4*x*y/(1-x-y+lamb_hdec(x,y))**2)
     .         + bij_hdec(x,y))
     .         + ash/pi*(2*(4/3.d0-dlog(x)))
      qcdcmi1(x,y)=1.d0+4/3.d0*ash/pi*(3 + (y-x)/2*dlog(x/y)
     .         +(2*(1-x-y)+lamb_hdec(x,y)**2)/2/lamb_hdec(x,y)
     .         *dlog(4*x*y/(1-x-y+lamb_hdec(x,y))**2)
     .         + bij_hdec(x,y))
     .         + ash/pi*(2*(4/3.d0-dlog(x)))
c------------------------------------------------------------------------
      cqcd(z,tb,x,y,r)= gf/(4*pi*dsqrt(2.d0))*z**3*lamb_hdec(x,y)
     .              *((1.d0-x-y)*(x*tb**2*r**2*qcdc(x,y)
     .                           +y/tb**2*qcdc(y,x))
     .               -4.d0*x*y*r*qcdci(x,y))
      cqcdm(z,tb,x,y,r)= gf/(4*pi*dsqrt(2.d0))*z**3*lamb_hdec(x,y)
     .              *((1.d0-x-y)*(x*tb**2*r**2*qcdcm1(x,y)
     .                           +y/tb**2*qcdcm(y,x))
     .               -4.d0*x*y*r*qcdcmi1(x,y))
c mmm changed 21/8/13
      cqcd2hdm(z,gcpd,gcpu,x,y,r)=
     .     gf/(4*pi*dsqrt(2.d0))*z**3*lamb_hdec(x,y)
     .     *((1.d0-x-y)*(x*gcpd**2*r**2*qcdc(x,y)
     .                  +y*gcpu**2*qcdc(y,x))
     .     -4.d0*gcpd*gcpu*x*y*r*qcdci(x,y))
      cqcdm2hdm(z,gcpd,gcpu,x,y,r)=
     .     gf/(4*pi*dsqrt(2.d0))*z**3*lamb_hdec(x,y)
     .     *((1.d0-x-y)*(x*gcpd**2*r**2*qcdcm1(x,y)
     .                  +y*gcpu**2*qcdcm(y,x))
     .     -4.d0*gcpd*gcpu*x*y*r*qcdcmi1(x,y))
c end mmm changed 21/8/13
      elw(amh,amf,amfp,qf,ai3f)=ioelw*(
     .        elwfull_hdec(amh,amf,amfp,qf,ai3f,amw)
     .      + alph/pi*qf**2*hqcdm(beta_hdec(amf**2/amh**2))
     .      - gf*amh**2/16.d0/pi**2/dsqrt(2.d0)*2.117203d0)
      elw0(amh,amf,qf,acf)=ioelw*(alph/pi*3.d0/2*qf**2
     .                              *(3.d0/2-dlog(amh**2/amf**2))
     .      +gf/8/dsqrt(2.d0)/pi**2*(acf*amt**2
     .        +amw**2*(3*dlog(cs)/ss-5)+amz**2*(0.5d0
     .          -3*(1-4*ss*dabs(qf))**2)))
      cf(ca) = -cdlog(-(1+cdsqrt(1-ca))/(1-cdsqrt(1-ca)))**2/4
      cg(ca) = cdsqrt(1-ca)/2*cdlog(-(1+cdsqrt(1-ca))/(1-cdsqrt(1-ca)))
      ci1(ca,cb) = ca*cb/2/(ca-cb)
     .           + ca**2*cb**2/2/(ca-cb)**2*(cf(ca)-cf(cb))
     .           + ca**2*cb/(ca-cb)**2*(cg(ca)-cg(cb))
      ci2(ca,cb) = -ca*cb/2/(ca-cb)*(cf(ca)-cf(cb))
      hggqcd(asg,nf)=1.d0+asg/pi*(95.d0/4.d0-nf*7.d0/6.d0)
      hggqcd2(asg,nf,amh,amt)=1.d0+asg/pi*(95.d0/4.d0-nf*7.d0/6.d0)
     . +(asg/pi)**2*(149533/288.d0-363/8.d0*zeta2-495/8.d0*zeta3
     .              +19/8.d0*dlog(amh**2/amt**2)
     . +nf*(-4157/72.d0+11/2.d0*zeta2+5/4.d0*zeta3
     . +2/3.d0*dlog(amh**2/amt**2))
     . +nf**2*(127/108.d0-1/6.d0*zeta2))
     . +(asg/pi)**3*(467.683620788d0+122.440972222d0*dlog(amh**2/amt**2)
     .              +10.9409722222d0*dlog(amh**2/amt**2)**2)
      phggqcd(asg,nf)=1.d0+asg/pi*(73/4.d0-7/6.d0*nf)
      phggqcd2(asg,nf,amh,amt)=1.d0+asg/pi*(73/4.d0-7/6.d0*nf)
     . +(asg/pi)**2*(37631/96.d0-363/8.d0*zeta2-495/8.d0*zeta3
     . +nf*(-7189/144.d0+11/2.d0*zeta2+5/4.d0*zeta3)
     . +nf**2*(127/108.d0-1/6.d0*zeta2))
     . +(asg/pi)**3*(-212.447364638d0)
      dhggqcd(asg,nf)=1.d0+asg/pi*(21.d0-nf*7.d0/6.d0)
      dhggqcd2(asg,nf,amh,amt)=1.d0+asg/pi*(21.d0-nf*7.d0/6.d0)
     . +(asg/pi)**2*(32531/72.d0-363/8.d0*zeta2-495/8.d0*zeta3
     .              +19/16.d0*dlog(amh**2/amt**2)
     . +nf*(-15503/288.d0+11/2.d0*zeta2+5/4.d0*zeta3
     .     +1/3.d0*dlog(amh**2/amt**2))
     . +nf**2*(127/108.d0-1/6.d0*zeta2))
     . +(asg/pi)**3*(63.7474683529d0+53.3715277778d0*dlog(amh**2/amt**2)
     .              +5.47048611111d0*dlog(amh**2/amt**2)**2)
      sggqcd(asg)=asg/pi*7.d0/2.d0
      aggqcd(asg,nf)=1.d0+asg/pi*(97.d0/4.d0-nf*7.d0/6.d0)
      aggqcd2(asg,nf,ama,amt)=1.d0+asg/pi*(97.d0/4.d0-nf*7.d0/6.d0)
     . +(asg/pi)**2*(237311/864.d0-529/24.d0*zeta2-445/8.d0*zeta3
     . +5*dlog(ama**2/amt**2))
      hffself(amh)=1.d0+ioelw*(
     .              gf*amh**2/16.d0/pi**2/dsqrt(2.d0)*2.117203d0
     .            -(gf*amh**2/16.d0/pi**2/dsqrt(2.d0))**2*32.6567d0)
      hvvself(amh)=1.d0+ioelw*(
     .              gf*amh**2/16.d0/pi**2/dsqrt(2.d0)*2.800952d0
     .            +(gf*amh**2/16.d0/pi**2/dsqrt(2.d0))**2*62.0308d0)
c     wtop0(x,asg,nf)=asg/pi*(-2/3.d0*(pi**2+2*sp_hdec(x)-2*sp_hdec(1-x)
c    .               +(4*x*(1-x-2*x**2)*dlog(x)
c    .                +2*(1-x)**2*(5+4*x)*dlog(1-x)
c    .                -(1-x)*(5+9*x-6*x**2))
c    .               /2/(1-x)**2/(1+2*x)))
      wtop(x,asg,nf)=asg/pi*(-2/3.d0*(pi**2+2*sp_hdec(x)-2*sp_hdec(1-x)
     .               +(4*x*(1-x-2*x**2)*dlog(x)
     .                +2*(1-x)**2*(5+4*x)*dlog(1-x)
     .                -(1-x)*(5+9*x-6*x**2))
     .               /2/(1-x)**2/(1+2*x)))
     .       + (asg/2/pi)**2*(-110.4176d0+7.978d0*nf)
c     chtop0(x)=-4/3.d0*((5/2.d0-1/x)*dlog(1-x)+x/(1-x)*dlog(x)
c    .            +sp_hdec(x)-sp_hdec(1-x)+pi**2/2-9/4.d0)
      chtop(x)=8/3.d0*(sp_hdec(1-x)-x/2/(1-x)*dlog(x)
     .        + dlog(x)*dlog(1-x)/2+(1-2.5d0*x)/2/x*dlog(1-x)-pi**2/3
     .        +9/8.d0)
      chtop1(x)=8/3.d0*(sp_hdec(1-x)-x/2/(1-x)*dlog(x)
     .        + dlog(x)*dlog(1-x)/2+(1-2.5d0*x)/2/x*dlog(1-x)-pi**2/3
     .        +17/8.d0)

      pi=4d0*datan(1d0)
      ss=1.d0-(amw/amz)**2
      cs=1.d0-ss

      zeta2 = pi**2/6
      zeta3 = 1.202056903159594d0
      zeta4 = pi**4/90
      zeta5 = 1.03692775514337d0

      if(ihiggs.ne.0)then
       tsc = (amsq+amur+amdr)/3
       bsc = (amsq+amur+amdr)/3
       call sfermion_hdec(tsc,bsc,amsq,amur,amdr,amel,amer,al,au,ad,amu,
     .                amst,amsb,amsl,amsu,amsd,amse,amsn,amsn1,
     .                glee,gltt,glbb,ghee,ghtt,ghbb,
     .                gaee,gatt,gabb,gcen,gctb)
       call twohdmcp_hdec(tgbet)
      endif

c--decoupling the top quark from alphas
      amt0=3.d8

c--top quark decay width
c     gamt00= gf*amt**3/8/dsqrt(2d0)/pi*(1-amw**2/amt**2)**2
c    .                                 *(1+2*amw**2/amt**2)
c    .      * (1+wtop(amw**2/amt**2,alphas_hdec(amt,3),5))
      gamt0 = gf*amt**3/8/dsqrt(2d0)/pi*vtb**2
     .      * lamb_hdec(amb**2/amt**2,amw**2/amt**2)
     .      * ((1-amw**2/amt**2)*(1+2*amw**2/amt**2)
     .         -amb**2/amt**2*(2-amw**2/amt**2-amb**2/amt**2))
     .      * (1+wtop(amw**2/amt**2,alphas_hdec(amt,3),5))
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c--real!!
        loop  = 2
        qsusy = 1
        qsusy1 = qsusy
        qsusy2 = qsusy
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       do i = -10,10,1
c        qsusy = 10.d0**(i/10.d0)
c        qsusy1 = qsusy
c        qsusy2 = qsusy
c        qsusy = qsusy1
c        call botsusy_hdec(glb,ghb,gab,xglb,xghb,xgab,qsusy,loop)
c        call strsusy_hdec(glb,ghb,gab,xglb,xghb,xgab,qsusy,loop)
c       enddo
c       close(51)
c       close(52)
c       close(53)
c       close(54)
c       return
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      if(ihiggs.ne.0.and.amt.gt.amch+amb)then
       if(i2hdm.eq.0)then
        tsc = (amsq+amur+amdr)/3
        bsc = (amsq+amur+amdr)/3
       call sfermion_hdec(tsc,bsc,amsq,amur,amdr,amel,amer,al,au,ad,amu,
     .                 amst,amsb,amsl,amsu,amsd,amse,amsn,amsn1,
     .                 glee,gltt,glbb,ghee,ghtt,ghbb,
     .                 gaee,gatt,gabb,gcen,gctb)
c       loop  = 2
c       qsusy = 1
c       qsusy1 = qsusy
c       qsusy2 = qsusy
        qsusy = qsusy1
        call botsusy_hdec(glb,ghb,gab,xglb,xghb,xgab,qsusy,loop)
       else
        xglb = glb
        xghb = ghb
        xgab = gab
       endif
c      gamt10= gf*amt**3/8/dsqrt(2d0)/pi*vtb**2*(1-amch**2/amt**2)**2
c    .        *((amb/amt)**2*xgab**2 + gat**2)
c    .      * (1+alphas_hdec(amt,3)/pi*chtop0(amch**2/amt**2))
       ymb = runm_hdec(amt,5)
c      gamt10= gf*amt**3/8/dsqrt(2d0)/pi*vtb**2
c    .      * lamb_hdec(amb**2/amt**2,amch**2/amt**2)
c    .      * (((ymb/amt)**2*xgab**2 + gat**2)
c    .      * (1+amb**2/amt**2-amch**2/amt**2)+4*ymb**2/amt**2*xgab*gat)
c    .      * (1+alphas_hdec(amt,3)/pi*chtop0(amch**2/amt**2))
       gamt1 = gf*amt**3/8/dsqrt(2d0)/pi*vtb**2
     .      * lamb_hdec(amb**2/amt**2,amch**2/amt**2)
     .      * (gat**2*(1+amb**2/amt**2-amch**2/amt**2)
     .      * (1+alphas_hdec(amt,3)/pi*chtop(amch**2/amt**2))
     .        +(ymb/amt)**2*xgab**2*(1+amb**2/amt**2-amch**2/amt**2)
     .      * (1+alphas_hdec(amt,3)/pi*chtop1(amch**2/amt**2))
     .      + 4*ymb**2/amt**2*xgab*gat)
      else
       gamt1 = 0
      endif
      gamt1 = gamt0+gamt1
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)'topw: ',gamt0,wtop(amw**2/amt**2,alphas_hdec(amt,3),5),
c    .                   wtop0(amw**2/amt**2,alphas_hdec(amt,3),5),
c    .                   alphas_hdec(amt,3)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     gamt10= gamt00+gamt10
c     gamt0 = gamt00
c     gamt1 = gamt10
c     write(6,*)'topw: ',gamt0,gamt00,gamt00/gamt0
c     write(6,*)'toph: ',gamt1-gamt0,gamt10-gamt00,
c    .                  (gamt10-gamt00)/(gamt1-gamt0)
c     write(6,*)'top:  ',gamt0/gamt1,(gamt1-gamt0)/gamt1,gamt1
c     write(6,*)'top0: ',gamt00/gamt10,(gamt10-gamt00)/gamt10,gamt10
c     write(6,*)'toph: ',chtop(amch**2/amt**2),chtop0(amch**2/amt**2),
c    .                   chtop(amch**2/amt**2)/chtop0(amch**2/amt**2)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)'ckm: ',vus,vcb,vub/vcb
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      if(ihiggs.eq.0)then

c        =========================================================
c                              sm higgs decays
c        =========================================================
      amxx=amh
      amh=amsm
c     =============  running masses
      rms = runm_hdec(amh,3)
      rmc = runm_hdec(amh,4)
      rmb = runm_hdec(amh,5)
      rmt = runm_hdec(amh,6)
c     write(6,*) 'm_{s,c,b,t} (m_h) = ',amh,rms,rmc,rmb,rmt
c     write(6,*) 'm_{s,c,b,t} = ',runm_hdec(2.d0,3),runm_hdec(3.d0,4),
c    .                            runm_hdec(ambb,5),amt
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     xxx = runm_hdec(amb,5)
c     write(6,*) 'mb = ',xxx
c     do i=1,23
c      xxx = runm_hdec(xxx,5)
c      write(6,*) 'mb = ',xxx
c     enddo
c     write(6,*)
c     xxx = runm_hdec(amc,4)
c     write(6,*) 'mc = ',xxx
c     do i=1,43
c      xxx = runm_hdec(xxx,4)
c      write(6,*) 'mc = ',xxx
c     enddo
c     write(6,*)'mc(3): ',runm_hdec(3.d0,4)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)
c     write(6,*) 'alpha_s(mz) = ',alphas_hdec(amz,3)
c     write(6,*)
c     xxx = runm_hdec(ambb,5)
c     write(6,*) 'mb = ',amb
c     write(6,*) 'mb = ',xxx
c     write(6,*) 'mb = ',ambb
c     write(6,*)
c     xxx = runm_hdec(3.d0,4)
c     write(6,*) 'mc = ',amc
c     write(6,*) 'mc = ',xxx
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,"(f9.2,3(2x,g12.5))")amh,rmc,rmb,rmt
      if(ism4.ne.0)then
       rmbp= runm_hdec(amh,7)
       rmtp= runm_hdec(amh,8)
c      write(6,*)amtp,ambp
c      write(6,*)runm_hdec(amtp,8),runm_hdec(amtp,7)
c      write(6,*)rmtp,rmbp,amh
      endif
      ratcoup = 1
      higtop = amh**2/amt**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)'strange mass: ',runm_hdec(1.d0,3),runm_hdec(2.d0,3)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     x1 = runm_hdec(amb,5)
c     x2 = runm_hdec(amh,5)
c     do i=1,100
c      x1 = runm_hdec(x1,5)
c     enddo
c     write(6,*)'mb,mh      = ',amb,amh
c     write(6,*)'als(mz)    = ',alphas_hdec(amz,3)
c     write(6,*)'als(mb,mh) = ',alphas_hdec(amb,3),alphas_hdec(amh,3)
c     write(6,*)'mb(mb,mh)  = ',x1,x2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     topfac = 1
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     x2 = runm_hdec(amh/4,6)
c     x3 = runm_hdec(amh/2,6)
c     x4 = runm_hdec(amh,6)
c     x1 = amt
c     do i=1,100
c      x1 = runm_hdec(x1,6)
c     enddo
c     write(6,*)'mh           = ',amh
c     write(6,*)'mt(mt,k*mh)  = ',amt,x1,x2,x3,x4
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     xk = 16.11d0
c     xk = xk - 1.0414d0*(1.d0-amb/amt)
c     xk = xk - 1.0414d0*(1.d0-amc/amt)
c     xk = xk - 1.0414d0*(1.d0-ams/amt)
c     xk = xk - 1.0414d0*(2.d0)
c     xk2 = 0.65269d0*(5)**2 - 29.7010d0*(5) + 239.2966d0
c     x0 = x1*(1+4*alphas_hdec(amt,3)/3/pi
c    .              +xk*(alphas_hdec(amt,3)/pi)**2
c    .              +xk2*(alphas_hdec(amt,3)/pi)**3)
c    .   * (1-alphas_hdec(amt,3)/pi*dlog(amt**2/x1**2))
c     write(6,*)x0,x0/x1
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      x1 = runm_hdec(amh,4)
      x2 = runm_hdec(amh,5)
      x3 = runm_hdec(amh,6)
c     write(6,*)amh,x1,x2,x3
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      ash=alphas_hdec(amh,3)
c     write(66,('f4.0,3(g18.10)'))amh,ash,rmb,runm_hdec(amh/2,5)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      ash=alphas_hdec(amh,3)
c     write(66,('f4.0,3(g18.10)'))amh,ash,rmb,runm_hdec(amh/2,5)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      ash=alphas_hdec(amh,3)
c     write(66,('f4.0,3(g18.10)'))amh,ash,rmb,runm_hdec(amh/2,5)
      amc0=1.d8
      amb0=2.d8
      as3=alphas_hdec(amh,3)
      amc0=amc
      as4=alphas_hdec(amh,3)
      amb0=amb
c     amt0=amt
c     =============== partial widths
c  h ---> g g
c
c  mass dependent nlo qcd corrections
       call corrgg_hdec(amh,amt,amb,xhgg,xhqq)
c      write(6,*)amh,amt,amb,xhgg,xhqq
       eps=1.d-8
       nfext = 3
       asg = as3
       ctt = 4*amt**2/amh**2*dcmplx(1d0,-eps)
       ctb = 4*amb**2/amh**2*dcmplx(1d0,-eps)
       ctc = 4*amc**2/amh**2*dcmplx(1d0,-eps)
       cat0= 2*ctt*(1+(1-ctt)*cf(ctt))
       cab0= 2*ctb*(1+(1-ctb)*cf(ctb))
       cac0= 2*ctc*(1+(1-ctc)*cf(ctc))
       cat = cat0 * cpt
       cab = cab0 * cpb
       cac = cac0 * cpc
       catp = 0
       cabp = 0
       catp0= 0
       cabp0= 0
       if(ism4.ne.0)then
        cttp = 4*amtp**2/amh**2*dcmplx(1d0,-eps)
        ctbp = 4*ambp**2/amh**2*dcmplx(1d0,-eps)
        catp0= 2*cttp*(1+(1-cttp)*cf(cttp))
        cabp0= 2*ctbp*(1+(1-ctbp)*cf(ctbp))
        catp = catp0 * cptp
        cabp = cabp0 * cpbp
       endif
       fqcd=hggqcd(asg,nfext)
       dqcd=dhggqcd(asg,nfext)
       pqcd=phggqcd(asg,nfext)
       xfac = cdabs(cat+cab+cac+catp+cabp)**2*fqcd
     .      + 4*dreal(dconjg(cat+cab+cac+catp+cabp)*cpgg)*dqcd
     .      + (2*cpgg)**2*pqcd
c  mass dependent nlo qcd corrections
       xfac0 =(cdabs(cat)**2*(xhgg(1)+xhqq(1)*nfext)
     .       + cdabs(cab)**2*(xhgg(2)+xhqq(2)*nfext)
     .       + 2*dreal(dconjg(cat)*cab)*(xhgg(3)+xhqq(3)*nfext))*asg/pi
       xfac = xfac + xfac0
       hgg=hvv(amh,0.d0)*(asg/pi)**2*xfac/8

c  h ---> g g* ---> g cc   to be added to h ---> cc
       nfext = 4
       asg = as4
       fqcd=hggqcd(asg,nfext)
       dqcd=dhggqcd(asg,nfext)
       pqcd=phggqcd(asg,nfext)
       xfac = cdabs(cat+cab+cac+catp+cabp)**2*fqcd
     .      + 4*dreal(dconjg(cat+cab+cac+catp+cabp)*cpgg)*dqcd
     .      + (2*cpgg)**2*pqcd
c  mass dependent nlo qcd corrections
       xfac0 =(cdabs(cat)**2*(xhgg(1)+xhqq(1)*nfext)
     .       + cdabs(cab)**2*(xhgg(2)+xhqq(2)*nfext)
     .       + 2*dreal(dconjg(cat)*cab)*(xhgg(3)+xhqq(3)*nfext))*asg/pi
       xfac = xfac + xfac0
       dcc=hvv(amh,0.d0)*(asg/pi)**2*xfac/8 - hgg
c  h ---> g g* ---> g bb   to be added to h ---> bb
       nfext = 5
       asg = ash
       fqcd=hggqcd(asg,nfext)
       dqcd=dhggqcd(asg,nfext)
       pqcd=phggqcd(asg,nfext)
       xfac = cdabs(cat+cab+cac+catp+cabp)**2*fqcd
     .      + 4*dreal(dconjg(cat+cab+cac+catp+cabp)*cpgg)*dqcd
     .      + (2*cpgg)**2*pqcd
c  mass dependent nlo qcd corrections
       xfac0 =(cdabs(cat)**2*(xhgg(1)+xhqq(1)*nfext)
     .       + cdabs(cab)**2*(xhgg(2)+xhqq(2)*nfext)
     .       + 2*dreal(dconjg(cat)*cab)*(xhgg(3)+xhqq(3)*nfext))*asg/pi
       xfac = xfac + xfac0
       dbb=hvv(amh,0.d0)*(asg/pi)**2*xfac/8 - hgg - dcc

c  h ---> g g: full nnnlo corrections for nf=5
       if(ism4.eq.0)then
        fqcd0=hggqcd(asg,5)
        fqcd=hggqcd2(asg,5,amh,amt)
        dqcd=dhggqcd2(asg,5,amh,amt)
        pqcd=phggqcd2(asg,5,amh,amt)
        xfac = cdabs(cat+cab+cac+catp+cabp)**2*fqcd
     .      + 4*dreal(dconjg(cat+cab+cac+catp+cabp)*cpgg)*dqcd
     .      + (2*cpgg)**2*pqcd
c  mass dependent nlo qcd corrections
        xfac0 =(cdabs(cat)**2*(xhgg(1)+xhqq(1)*nfext)
     .        + cdabs(cab)**2*(xhgg(2)+xhqq(2)*nfext)
     .        + 2*dreal(dconjg(cat)*cab)*(xhgg(3)+xhqq(3)*nfext))*asg/pi
c       write(6,*)'xfac: ',xfac,xfac0
        xfac = xfac + xfac0
        hgg=hvv(amh,0.d0)*(asg/pi)**2*xfac/8
c  electroweak corrections
        if(icoupelw.eq.0)then
c        xfac00 = cdabs(cat0+cab0+cac0+catp0+cabp0)**2*fqcd
         xfac00= dreal(dconjg(cat+cab+cac+catp+cabp)
     .                     * (cat0+cab0+cac0+catp0+cabp0))*fqcd
        else
         xfac00 = cdabs(cat+cab+cac+catp+cabp)**2*fqcd
     .          + 4*dreal(dconjg(cat+cab+cac+catp+cabp)*cpgg)*dqcd
     .          + (2*cpgg)**2*pqcd
        endif
        hgg0 =hvv(amh,0.d0)*(asg/pi)**2*xfac00/8*ioelw*glgl_elw(amt,amh)
c       write(6,*)'hgg: ',hgg,hgg0,xfac00,glgl_elw(amt,amh)
        hgg=hgg+hgg0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      cdum0 = cat0+cab0
c      cdum  = cat +cab
c      ctot0= cat0/cdum0
c      cdt0 = cat0/cat0
c      cdb0 = cab0/cat0
c      ctot = cat/cdum
c      cdt  = cat/cat
c      cdb  = cab/cat
c      write(6,*)'h -> gg ',ctot,cdt,cdb
c      write(6,*)'h -> gg0',ctot0,cdt0,cdb0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)'h -> gg: ',1.d0,asg/pi*(95.d0/4.d0-5*7.d0/6.d0),
c    .  (asg/pi)**2*(149533/288.d0-363/8.d0*zeta2-495/8.d0*zeta3
c    .              +19/8.d0*dlog(amh**2/amt**2)
c    . +5*(-4157/72.d0+11/2.d0*zeta2+5/4.d0*zeta3
c    . +2/3.d0*dlog(amh**2/amt**2))
c    . +5**2*(127/108.d0-1/6.d0*zeta2)),
c    .  (asg/pi)**3*(467.683620788d0+122.440972222d0*dlog(amh**2/amt**2)
c    .  +10.9409722222d0*dlog(amh**2/amt**2)**2),
c    .  xfac0/cdabs(cat+cab+cac+catp+cabp)**2,
c    .  xfac/cdabs(cat+cab+cac+catp+cabp)**2,glgl_elw(amt,amh),
c    .  (hgg)/(hvv(amh,0.d0)*(asg/pi)**2/8*cdabs(cat+cab+cac)**2),
c    .  (xfac-xfac0)/cdabs(cat+cab+cac+catp+cabp)**2
c    . * (1+glgl_elw(amt,amh)) + xfac0/cdabs(cat+cab+cac+catp+cabp)**2,
c    .  (fqcd+xfac0/cdabs(cat+cab+cac)**2)*(1+glgl_elw(amt,amh)),
c    .  (fqcd+xfac0/cdabs(cat+cab+cac)**2)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       write(6,*)'hgg:',xfac/fqcd/(4/3.d0)**2-1
c    .                  ,cdabs(cat)**2/(4/3.d0)**2-1
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       write(6,*)cdabs(cat)**2,16/9.d0,16/9.d0/cdabs(cat)**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       write(46,*)amh,fqcd0,fqcd0+xfac0/cdabs(cat+cab)**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       yy0 = cdabs(cat+cac)**2*fqcd
c    .      + cdabs(cat)**2*(xhgg(1)+xhqq(1)*nfext)*asg/pi
c       ytt = hvv(amh,0.d0)*(asg/pi)**2*yy0/8/hgg*(1+glgl_elw(amt,amh))
c       yy0 = cdabs(cab)**2*(fqcd+(xhgg(2)+xhqq(2)*nfext)*asg/pi)
c       ybb = hvv(amh,0.d0)*(asg/pi)**2*yy0/8/hgg*(1+glgl_elw(amt,amh))
c       yy0 = 2*dreal(dconjg(cat+cac)*cab)*fqcd
c    .      + 2*dreal(dconjg(cat)*cab)*(xhgg(3)+xhqq(3)*nfext)*asg/pi
c       ytb = hvv(amh,0.d0)*(asg/pi)**2*yy0/8/hgg*(1+glgl_elw(amt,amh))
c       write(51,('f4.0,4(g18.10)'))amh,hgg,ytt,ybb,ytb
c       write(6,*)'gg: ',amh,ytt+ybb+ytb
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       xfac1 = cdabs(cat)**2*(xhgg(1)+xhqq(1)*nfext)*asg/pi
c       xfac2 = cdabs(cab)**2*(xhgg(2)+xhqq(2)*nfext)*asg/pi
c       xfac3 = 2*dreal(dconjg(cat)*cab)*(xhgg(3)+xhqq(3)*nfext)*asg/pi
c       write(50,*)amh,fqcd0,xfac1/cdabs(cat)**2/fqcd0
c       write(50,*)amh,fqcd0,xfac2/cdabs(cab)**2/fqcd0
c       write(50,*)amh,fqcd0,xfac3/(2*dreal(dconjg(cat)*cab))/fqcd0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       write(50,*)amh,fqcd0,fqcd0+xfac1/cdabs(cat)**2
c       write(51,*)amh,fqcd0,fqcd0+xfac2/cdabs(cab)**2
c       write(52,*)amh,fqcd0,fqcd0+xfac3/(2*dreal(dconjg(cat)*cab))
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       else
        im = iggelw
        fqcd0=hggqcd(asg,5)
        dqcd=dhggqcd2(asg,5,amh,amt)
        pqcd=phggqcd2(asg,5,amh,amt)
        fac4 = -asg**2/pi**2*(77/288.d0*2 + (2/3.d0*5+19/8.d0)
     .       * (cpbp*dlog(ambp**2/amt**2)+cptp*dlog(amtp**2/amt**2))
     .         / (cpt+cpbp+cptp))
        if(cpbp.eq.0.d0.and.cptp.eq.0.d0.and.cpt.eq.0.d0)
     .                            fac4 = -asg**2/pi**2*(77/288.d0*2)
        fqcd=hggqcd2(asg,5,amh,amt)+fac4
        dqcd=dqcd+fac4/2
        xfac = cdabs(cat+cab+cac+catp+cabp)**2*fqcd
        hgg=hvv(amh,0.d0)*(asg/pi)**2*xfac/8
        if(icoupelw.eq.0)then
         xfac00= dreal(dconjg(cat+cab+cac+catp+cabp)
     .                     * (cat0+cab0+cac0+catp0+cabp0))*fqcd
        else
         xfac00 = cdabs(cat+cab+cac+catp+cabp)**2*fqcd
     .          + 4*dreal(dconjg(cat+cab+cac+catp+cabp)*cpgg)*dqcd
     .          + (2*cpgg)**2*pqcd
        endif
        hgg0 = hvv(amh,0.d0)*(asg/pi)**2*xfac00/8 * glgl_elw4(im,amh)
        hgg=hgg+hgg0
c     write(6,*)'h -> gg: ',amh,hgg,glgl_elw4(im,amh)*100,fqcd,
c    .                      fac4/fqcd*100
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        if(amnup.eq.amep)then
         xxl = amep**2
        else
         xxl =amnup**2*amep**2/(amnup**2-amep**2)*dlog(amnup**2/amep**2)
        endif
        if(amtp.eq.ambp)then
         xxq = amtp**2
        else
         xxq = amtp**2*ambp**2/(amtp**2-ambp**2)*dlog(amtp**2/ambp**2)
        endif
        celw = gf/8/dsqrt(2.d0)/pi**2*(5*amt**2/2
     .       + 7*(amnup**2+amep**2)/6 - xxl
     .       + 3*(amtp**2+ambp**2)/2 - 3*xxq)
c      write(6,*)'h -> gg: ',amh,2*celw*100,glgl_elw4(im,amh)*100
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       endif
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)'h -> gg: ',hgg,dbb,dcc
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      if(nfgg.eq.3)then
       hgg = hgg - dbb - dcc
      elseif(nfgg.eq.4)then
       hgg = hgg - dbb
       dcc = 0
      else
       dcc = 0
       dbb = 0
      endif
      if(ifermphob.ne.0)then
c      write(6,*)'fermiophobic: h -> gg ---> 0'
       hgg = 0
       dbb = 0
       dcc = 0
      endif
c     write(6,*)'h -> gg: ',fqcd0,fqcd,fqcd-fqcd0
c     write(6,*)'h -> gg: ',(fqcd0-1)/asg*pi,(fqcd-fqcd0)/asg**2*pi**2
c     write(6,*)'h -> gg: ',glgl_elw(amt,amh)
c     write(6,*)'xfac(',amh,') = ',xfac
c     write(6,*)'br(h -> gg) = ',hgg
c     write(6,*)'ga(h -> gg) = ',hgg,hgg0,hvv(amh,0.d0),xfac,xfac0

      sm4facf=1
      sm4facw = 1
      sm4facz = 1
      if(ism4.ne.0)then
       sm4facf=1+elw4_hdec(amnup,amep,amtp,ambp)
       sm4facw=1+elw4v_hdec(1,amnup,amep,amtp,ambp)
       sm4facz=1+elw4v_hdec(2,amnup,amep,amtp,ambp)
c      write(6,*)'sm4 elw.: ff,ww,zz ',(sm4facf-1)*100,
c    .                 (sm4facw-1)*100,(sm4facz-1)*100
      endif

c  h ---> e e
c     cpel = cpmu
c     amelec = 0.510998910d-3
c     if(amh.le.2*amelec) then
c      hee = 0
c     else
c     hee=hff(amh,(amelec/amh)**2)
c    .    *(1+elw0(amh,amelec,-1.d0,7.d0))
c    .    *(1+elw(amh,amelec,0.d0,-1.d0,-1/2.d0))
c    .    *hffself(amh)
c    .    *sm4facf
c    .    * cpel**2
c     if(icoupelw.eq.0)then
c      hee=hff(amh,(amelec/amh)**2) * cpmu
c    .    * ((cpel-1)
c    .       +(1+elw0(amh,amelec,-1.d0,7.d0))
c    .       +(1+elw(amh,amelec,0.d0,-1.d0,-1/2.d0))
c    .       *hffself(amh) * sm4facf)
c     endif
c     endif
c  h ---> mu mu
      if(amh.le.2*ammuon) then
       hmm = 0
      else
      hmm=hff(amh,(ammuon/amh)**2)
c    .    *(1+elw0(amh,ammuon,-1.d0,7.d0))
     .    *(1+elw(amh,ammuon,0.d0,-1.d0,-1/2.d0))
     .    *hffself(amh)
     .    *sm4facf
     .    * cpmu**2
      if(icoupelw.eq.0)then
       hmm=hff(amh,(ammuon/amh)**2) * cpmu
     .    * ((cpmu-1)
c    .       +(1+elw0(amh,ammuon,-1.d0,7.d0))
     .       +(1+elw(amh,ammuon,0.d0,-1.d0,-1/2.d0))
     .       *hffself(amh) * sm4facf)
      endif
      endif
c     write(6,*)'ee/mumu: ',hee/hmm,hee/hmm * ammuon**2/amelec**2
c  h ---> tau tau
      if(amh.le.2*amtau) then
       hll = 0
      else
      hll=hff(amh,(amtau/amh)**2)
c    .    *(1+elw0(amh,amtau,-1.d0,7.d0))
     .    *(1+elw(amh,amtau,0.d0,-1.d0,-1/2.d0))
     .    *hffself(amh)
     .    *sm4facf
     .    * cptau**2
      if(icoupelw.eq.0)then
       hll=hff(amh,(amtau/amh)**2) * cptau
     .    * ((cptau-1)
c    .        +(1+elw0(amh,amtau,-1.d0,7.d0))
     .        +(1+elw(amh,amtau,0.d0,-1.d0,-1/2.d0))
     .        *hffself(amh) * sm4facf)
      endif
      endif

c      write(96,*)amh,elw0(amh,amtau,-1.d0,7.d0)
c    .               -alph/pi*3.d0/2*(3.d0/2-dlog(amh**2/amtau**2))
c    .               ,elw(amh,amtau,0.d0,-1.d0,-1/2.d0)
c    .               -alph/pi*hqcdm(beta_hdec(amtau**2/amh**2))
c    .               +gf*amh**2/16.d0/pi**2/dsqrt(2.d0)*2.117203d0

c      write(6,*)'tau tau: ',amh,elw0(amh,amtau,-1.d0,7.d0),hffself(amh)
c  h --> ss
      irat = 0
      ratcoup = 1
      xqcd0 = qcdh(rms**2/amh**2)
      if(cpt.eq.0.d0)then
       ratcoup = 0
      else
       if(cps.eq.0.d0)then
        cps = 1.d-20
        irat = 1
       endif
       ratcoup = cpt/cps
      endif
      xqcd1= qcdh(rms**2/amh**2)
      xqcd = (xqcd0-xqcd1)/2/xqcd1
      if(amh.le.2*ams) then
       hss = 0
      else
       hs2=3.d0*hff(amh,(rms/amh)**2)
     .    *qcdh(rms**2/amh**2)
c    .    *(1+elw0(amh,rms,-1.d0/3.d0,7.d0))
     .    *(1+elw(amh,ams,amc,-1/3.d0,-1/2.d0))
     .    *hffself(amh) * sm4facf
     .    * cps**2
      if(icoupelw.eq.0)then
       hs2=3.d0*hff(amh,(rms/amh)**2) * cps
     .    *qcdh(rms**2/amh**2)
     .    * ((cps-1)
c    .         +(1+elw0(amh,rms,-1.d0/3.d0,7.d0)*(1+xqcd))
     .         +(1+elw(amh,ams,amc,-1/3.d0,-1/2.d0)*(1+xqcd))
     .       *hffself(amh) * sm4facf)
      endif
c      if(hs2.lt.0.d0) hs2 = 0
       hs1=3.d0*hff(amh,(ams/amh)**2)
     .    *tqcdh(ams**2/amh**2)
c    .    *(1+elw0(amh,rms,-1.d0/3.d0,7.d0))
     .    *(1+elw(amh,ams,amc,-1/3.d0,-1/2.d0))
     .    *hffself(amh) * sm4facf
     .    *cps**2
      if(icoupelw.eq.0)then
       hs1=3.d0*hff(amh,(ams/amh)**2) * cps
     .    *tqcdh(ams**2/amh**2)
     .    * ((cps-1)
c    .         +(1+elw0(amh,rms,-1.d0/3.d0,7.d0))
     .         +(1+elw(amh,ams,amc,-1/3.d0,-1/2.d0))
     .       *hffself(amh) * sm4facf)
      endif
       rat = 2*ams/amh
       hss = qqint_hdec(rat,hs1,hs2)
      endif
c     hss = hss * sm4facf
      if(irat.eq.1)cps = 0
c  h --> cc
      irat = 0
      ratcoup = 1
      xqcd0 = qcdh(rmc**2/amh**2)
      if(cpt.eq.0.d0)then
       ratcoup = 0
      else
       if(cpc.eq.0.d0)then
        cpc = 1.d-20
        irat = 1
       endif
       ratcoup = cpt/cpc
      endif
      xqcd1= qcdh(rmc**2/amh**2)
      xqcd = (xqcd0-xqcd1)/2/xqcd1
      if(amh.le.2*amc) then
       hcc = 0
      else
       hc2=3.d0*hff(amh,(rmc/amh)**2)
     .    *qcdh(rmc**2/amh**2)
c    .    *(1+elw0(amh,rmc,2.d0/3.d0,7.d0))
     .    *(1+elw(amh,amc,ams,2/3.d0,1/2.d0))
     .    *hffself(amh) * sm4facf
     .    *cpc**2
     .   + dcc
      if(icoupelw.eq.0)then
       hc2=3.d0*hff(amh,(rmc/amh)**2) * cpc
     .    *qcdh(rmc**2/amh**2)
     .    * ((cpc-1)
c    .       +(1+elw0(amh,rmc,2.d0/3.d0,7.d0)*(1+xqcd))
     .       +(1+elw(amh,amc,ams,2/3.d0,1/2.d0)*(1+xqcd))
     .       *hffself(amh) * sm4facf)
     .   + dcc
      endif
c      if(hc2.lt.0.d0) hc2 = 0
       hc1=3.d0*hff(amh,(amc/amh)**2)
     .    *tqcdh(amc**2/amh**2)
c    .    *(1+elw0(amh,amc,2.d0/3.d0,7.d0))
     .    *(1+elw(amh,amc,ams,2/3.d0,1/2.d0))
     .    *hffself(amh) * sm4facf
     .    *cpc**2
     .   + dcc
      if(icoupelw.eq.0)then
       hc1=3.d0*hff(amh,(amc/amh)**2) * cpc
     .    *tqcdh(amc**2/amh**2)
     .    * ((cpc-1)
c    .       +(1+elw0(amh,amc,2.d0/3.d0,7.d0))
     .       +(1+elw(amh,amc,ams,2/3.d0,1/2.d0))
     .       *hffself(amh) * sm4facf)
     .   + dcc
      endif
       rat = 2*amc/amh
       hcc = qqint_hdec(rat,hc1,hc2)
c      write(6,*)'h -> cc: ',hcc,qcdh(rmc**2/amh**2)
c      write(6,*)'h -> cc: ',
c    .  rmc**2/amh**2*(-93.72459d0+12)*(ash/pi)**2/qcdh(rmc**2/amh**2)

c      eps00 = 1.d-15
c      xq1 = amb*(1-eps00)
c      xq2 = amb*(1+eps00)
c      xal1 = alphas_hdec(xq1,3)
c      xal2 = alphas_hdec(xq2,3)
c      xdel1= 7*alphas_hdec(xq1,3)**2/pi**2/24
c      xdel2= 7*alphas_hdec(xq2,3)**2/pi**2/24
c      write(6,*)xal1,xal2
c      write(6,*)(xal2-xal1)/xal1,(xal2-xal1)/xal2
c      write(6,*)xdel2,xdel1

c      als0 = alphas_hdec(amz,3)
c      wmc = runm_hdec(amc,4)
c      ymc = runm_hdec(3.d0,4)
c      zmc = 0.996d0-(als0-0.1189d0)/2.d0*9
c      do i =125,133
c       sc = i/100.d0
c       xmc = runm_hdec(sc,4)
c       write(6,*)
c       write(6,*)sc,xmc,amc,ymc,zmc,wmc,als0
c      enddo
      endif
c     hcc = hcc * sm4facf
      if(irat.eq.1)cpc = 0
c     write(6,*)amb,amc
c  h --> bb :
      irat = 0
      ratcoup = 1
      xqcd0 = qcdh(rmb**2/amh**2)
      if(cpt.eq.0.d0)then
       ratcoup = 0
      else
       if(cpb.eq.0.d0)then
        cpb = 1.d-20
        irat = 1
       endif
       ratcoup = cpt/cpb
      endif
      xqcd1= qcdh(rmb**2/amh**2)
      xqcd = (xqcd0-xqcd1)/2/xqcd1
      if(amh.le.2*amb) then
       hbb = 0
      else
       hb2=3.d0*hff(amh,(rmb/amh)**2)
     .    *qcdh(rmb**2/amh**2)
c    .    *(1+elw0(amh,rmb,-1.d0/3.d0,1.d0))
     .    *(1+elw(amh,amb,amt,-1/3.d0,-1/2.d0))
     .    *hffself(amh) * sm4facf
     .    * cpb**2
     .   + dbb
       if(icoupelw.eq.0)then
        hb2=3.d0*hff(amh,(rmb/amh)**2) * cpb
     .     *qcdh(rmb**2/amh**2)
     .     * ((cpb-1)
c    .        +(1+elw0(amh,rmb,-1.d0/3.d0,1.d0)*(1+xqcd))
     .        +(1+elw(amh,amb,amt,-1/3.d0,-1/2.d0)*(1+xqcd))
     .        *hffself(amh) * sm4facf)
     .    + dbb
       endif
c      if(hb2.lt.0.d0) hb2 = 0
       hb1=3.d0*hff(amh,(amb/amh)**2)
     .    *tqcdh(amb**2/amh**2)
c    .    *(1+elw0(amh,amb,-1.d0/3.d0,1.d0))
     .    *(1+elw(amh,amb,amt,-1/3.d0,-1/2.d0))
     .    *hffself(amh) * sm4facf
     .    * cpb**2
     .   + dbb
      if(icoupelw.eq.0)then
       hb1=3.d0*hff(amh,(amb/amh)**2) * cpb
     .    *tqcdh(amb**2/amh**2)
     .    * ((cpb-1)
c    .        +(1+elw0(amh,amb,-1.d0/3.d0,1.d0))
     .        +(1+elw(amh,amb,amt,-1/3.d0,-1/2.d0))
     .       *hffself(amh) * sm4facf)
     .   + dbb
      endif
       rat = 2*amb/amh
       hbb = qqint_hdec(rat,hb1,hb2)
c      write(6,*)'h -> bb = ',amh,hbb
c      write(6,*)'h -> bb: ',hbb,qcdh(rmb**2/amh**2)
c      write(6,*)'h -> bb: ',qcdh(rmb**2/amh**2)
c      write(6,*)'h -> bb: ',
c    .  rmb**2/amh**2*(-93.72459d0+12)*(ash/pi)**2/qcdh(rmb**2/amh**2)

c     write(6,*)amh,hbb,rmb,runm_hdec(amb,5),amb,hb1,hb2
c     write(6,*)amh,elw0(amh,amb,-1.d0/3.d0,1.d0),
c    .              elw(amh,amb,amt,-1/3.d0,-1/2.d0)
c     write(6,*)(1.570d0-2*dlog(higtop)/3+ dlog(rmb**2/amh**2)**2/9)
c    .          *(ash/pi)**2,
c    .          qcdh(rmb**2/amh**2)-1
c     write(6,*)'mu,tau,b: ',elw(amh,ammuon,0.d0,-1.d0,-1/2.d0)
c    .                      ,elw(amh,amtau,0.d0,-1.d0,-1/2.d0)
c    .                      ,elw(amh,amb,amt,-1/3.d0,-1/2.d0)

c      write(97,*)amh,elw0(amh,amb,-1/3.d0,1.d0)
c    .               -alph/pi*3.d0/2/9*(3.d0/2-dlog(amh**2/amb**2))
c    .               ,elw(amh,amb,amt,-1/3.d0,-1/2.d0)
c    .               -alph/pi/9*hqcdm(beta_hdec(amb**2/amh**2))
c    .               +gf*amh**2/16.d0/pi**2/dsqrt(2.d0)*2.117203d0

c      write(6,*)amh,
c    .        alph/pi*amh**2/32.d0/amw**2/(1-amw**2/amz**2)*2.117203d0
c      write(6,*)

c      write(6,*)rmc,runm_hdec(amc,4),rmc/runm_hdec(amc,4),
c    .           qcdh(rmc**2/amh**2),
c    .           rmb,runm_hdec(amb,5),rmb/runm_hdec(amb,5),
c    .           qcdh(rmb**2/amh**2)

c      write(6,*)'bb: ',amh,ratcoup*(1.570d0 - 2*dlog(higtop)/3
c    .    + dlog(amh**2/rmb**2)**2/9)*(ash/pi)**2,qcdh(rmb**2/amh**2)-1,
c    .        elw0(amh,amb,-1.d0/3.d0,1.d0),hffself(amh)

c      write(6,*)amh,hb1,hb2,hbb

c      write(61,*)amh,1.d0+gf*amh**2/16.d0/pi**2/dsqrt(2.d0)*2.117203d0,
c    .               hffself(amh)

c      als0 = alphas_hdec(amz,3)
c      wmb = runm_hdec(amb,5)
c      ymb = 4.163d0-(als0-0.1189d0)/2.d0*12
c      xmb = runm_hdec(ymb,5)
c      write(6,*)
c      write(6,*)amb,xmb,ymb,wmb,als0
c      do i =1,100
c       sc = i
c       xms = runm_hdec(sc,3)
c       xmc = runm_hdec(sc,4)
c       xmb = runm_hdec(sc,5)
c       write(66,*)sc,xms,xmc,xmb,als0
c      enddo
      endif
c     hbb = hbb * sm4facf
      if(irat.eq.1)cpb = 0

c     hb1x=3.d0*hff(amh,(amb/amh)**2)
c    .    *tqcdh(amb**2/amh**2)
c    .    /(beta_hdec(amb**2/amh**2))**3
c     hb2x=3.d0*hff(amh,(rmb/amh)**2)
c    .    *qcdh(rmb**2/amh**2)
c    .    /(beta_hdec(rmb**2/amh**2))**3
c     ratcoup = 0
c     deltaqcd = qcdh(rmb**2/amh**2)
c     ratcoup = 1
c     deltat = qcdh(rmb**2/amh**2) - deltaqcd
c     write(6,*)'sm: mh     = ',amh
c     write(6,*)'alphas(mz) = ',alphas_hdec(91.d0,3)
c     write(6,*)'alphas(mh) = ',alphas_hdec(amh,3)
c     write(6,*)'alphas(mb) = ',alphas_hdec(amb,3)
c     write(6,*)'mb,mb(mb)  = ',amb,runm_hdec(amb,5)
c     write(6,*)'mb(mh,100) = ',rmb,runm_hdec(100.d0,5)
c     write(6,*)'deltaqcd,t = ',deltaqcd,deltat
c     write(6,*)'gamma(0)   = ',hb2x,hb1x
c     write(6,*)'gamma(mb)  = ',hb2,hb1
c     write(6,*)'mb         = ',amb
c     write(6,*)'mb(mh/2)   = ',runm_hdec(amh/2,5)
c     write(6,*)'mc         = ',amc
c     write(6,*)'mc(mh/2)   = ',runm_hdec(amh/2,4)
c     write(6,*)'mb(mb),mb  = ',ambb,ambb*(1+4*alphas_hdec(ambb,3)/3/pi)

c     als0 = alphas_hdec(amz,3)
c     wmt = runm_hdec(amt,6)
c     do i =165440,165450
c      sc = i/1000.d0
c      xmt = runm_hdec(sc,6)
c      write(6,*)
c      write(6,*)sc,xmt,amt,wmt,als0
c     enddo

c  h ---> tt
      irat = 0
      ratcoup = 0
      if(ionsh.eq.0)then
       dld=3d0
       dlu=5d0
       xm1 = 2d0*amt-dld
       xm2 = 2d0*amt+dlu
       if (amh.le.amt+amw+amb) then
       htt=0.d0
       elseif (amh.le.xm1) then
        factt=6.d0*gf**2*amh**3*amt**2/2.d0/128.d0/pi**3
        call htotts_hdec(amh,amt,amb,amw,htts)
        htt=factt*htts
       elseif (amh.le.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        factt=6.d0*gf**2*xx(1)**3*amt**2/2.d0/128.d0/pi**3
        call htotts_hdec(xx(1),amt,amb,amw,htts)
        yy(1)=factt*htts
        factt=6.d0*gf**2*xx(2)**3*amt**2/2.d0/128.d0/pi**3
        call htotts_hdec(xx(2),amt,amb,amw,htts)
        yy(2)=factt*htts
        xmt = runm_hdec(xx(3),6)
        xy2=3.d0*hff(xx(3),(xmt/xx(3))**2)
     .    *qcdh(xmt**2/xx(3)**2)
     .    *(1+elw(xx(3),amt,amb,2/3.d0,1/2.d0))
     .    *hffself(xx(3)) * sm4facf
     .    * cpt**2
        if(icoupelw.eq.0)then
         xy2=3.d0*hff(xx(3),(xmt/xx(3))**2) * cpt
     .      *qcdh(xmt**2/xx(3)**2)
     .      * ((cpt-1)
     .        +(1+elw(xx(3),amt,amb,2/3.d0,1/2.d0))
     .         *hffself(xx(3)) * sm4facf)
        endif
        if(xy2.lt.0.d0) xy2 = 0
        xy1=3.d0*hff(xx(3),(amt/xx(3))**2)
     .    *tqcdh(amt**2/xx(3)**2)
     .    *(1+elw(xx(3),amt,amb,2/3.d0,1/2.d0))
     .    *hffself(xx(3)) * sm4facf
     .    * cpt**2
        if(icoupelw.eq.0)then
         xy1=3.d0*hff(xx(3),(amt/xx(3))**2) * cpt
     .     *tqcdh(amt**2/xx(3)**2)
     .      * ((cpt-1)
     .        +(1+elw(xx(3),amt,amb,2/3.d0,1/2.d0))
     .    *hffself(xx(3)) * sm4facf)
        endif
        rat = 2*amt/xx(3)
        yy(3) = qqint_hdec(rat,xy1,xy2)
        xmt = runm_hdec(xx(4),6)
        xy2=3.d0*hff(xx(4),(xmt/xx(4))**2)
     .    *qcdh(xmt**2/xx(4)**2)
     .    *(1+elw(xx(4),amt,amb,2/3.d0,1/2.d0))
     .    *hffself(xx(4)) * sm4facf
     .    * cpt**2
        if(icoupelw.eq.0)then
         xy2=3.d0*hff(xx(4),(xmt/xx(4))**2) * cpt
     .      *qcdh(xmt**2/xx(4)**2)
     .      * ((cpt-1)
     .         +(1+elw(xx(4),amt,amb,2/3.d0,1/2.d0))
     .         *hffself(xx(4)) * sm4facf)
        endif
        if(xy2.lt.0.d0) xy2 = 0
        xy1=3.d0*hff(xx(4),(amt/xx(4))**2)
     .    *tqcdh(amt**2/xx(4)**2)
     .    *(1+elw(xx(4),amt,amb,2/3.d0,1/2.d0))
     .    *hffself(xx(4)) * sm4facf
     .    * cpt**2
        if(icoupelw.eq.0)then
         xy1=3.d0*hff(xx(4),(amt/xx(4))**2) * cpt
     .      *tqcdh(amt**2/xx(4)**2)
     .      * ((cpt-1)
     .         +(1+elw(xx(4),amt,amb,2/3.d0,1/2.d0))
     .         *hffself(xx(4)) * sm4facf)
        endif
        rat = 2*amt/xx(4)
        yy(4) = qqint_hdec(rat,xy1,xy2)
        htt = fint_hdec(amh,xx,yy)
       else
        ht2=3.d0*hff(amh,(rmt/amh)**2)
     .    *qcdh(rmt**2/amh**2)
     .    *(1+elw(amh,amt,amb,2/3.d0,1/2.d0))
     .    *hffself(amh) * sm4facf
     .    * cpt**2
        if(icoupelw.eq.0)then
         ht2=3.d0*hff(amh,(rmt/amh)**2) * cpt
     .      *qcdh(rmt**2/amh**2)
     .      * ((cpt-1)
     .         +(1+elw(amh,amt,amb,2/3.d0,1/2.d0))
     .         *hffself(amh) * sm4facf)
        endif
        if(ht2.lt.0.d0) ht2 = 0
        ht1=3.d0*hff(amh,(amt/amh)**2)
     .    *tqcdh(amt**2/amh**2)
     .    *(1+elw(amh,amt,amb,2/3.d0,1/2.d0))
     .    *hffself(amh) * sm4facf
     .    * cpt**2
        if(icoupelw.eq.0)then
         ht1=3.d0*hff(amh,(amt/amh)**2) * cpt
     .      *tqcdh(amt**2/amh**2)
     .      * ((cpt-1)
     .         +(1+elw(amh,amt,amb,2/3.d0,1/2.d0))
     .      *hffself(amh) * sm4facf)
        endif
        rat = 2*amt/amh
        htt = qqint_hdec(rat,ht1,ht2)
c      write(6,*)'h -> tt: ',
c    .  rmt**2/amh**2*(-93.72459d0+12)*(ash/pi)**2/qcdh(rmt**2/amh**2)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       ht0=htt
c       topfac = 0.5d0
c       rmt = runm_hdec(topfac*amh,6)
c       ht2=3.d0*hff(amh,(rmt/amh)**2)
c    .    *qcdh(rmt**2/amh**2)
c    .    *hffself(amh)
c       htl = qqint_hdec(rat,ht1,ht2)
c       topfac = 2
c       rmt = runm_hdec(topfac*amh,6)
c       ht2=3.d0*hff(amh,(rmt/amh)**2)
c    .    *qcdh(rmt**2/amh**2)
c    .    *hffself(amh)
c       htu = qqint_hdec(rat,ht1,ht2)
c       write(6,*)'h -> tt: ',amh,ht0,htl/ht0,htu/ht0
c       topfac = 1
c       rmt = runm_hdec(amh,6)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       endif
      else
       if (amh.le.2.d0*amt) then
        htt=0.d0
       else
        ht2=3.d0*hff(amh,(rmt/amh)**2)
     .    *qcdh(rmt**2/amh**2)
     .    *(1+elw(amh,amt,amb,2/3.d0,1/2.d0))
     .    *hffself(amh) * sm4facf
     .    * cpt**2
        if(icoupelw.eq.0)then
         ht2=3.d0*hff(amh,(rmt/amh)**2) * cpt
     .      *qcdh(rmt**2/amh**2)
     .      * ((cpt-1)
     .         +(1+elw(amh,amt,amb,2/3.d0,1/2.d0))
     .         *hffself(amh) * sm4facf)
        endif
        if(ht2.lt.0.d0) ht2 = 0
        ht1=3.d0*hff(amh,(amt/amh)**2)
     .    *tqcdh(amt**2/amh**2)
     .    *(1+elw(amh,amt,amb,2/3.d0,1/2.d0))
     .    *hffself(amh) * sm4facf
     .    * cpt**2
        if(icoupelw.eq.0)then
         ht1=3.d0*hff(amh,(amt/amh)**2) * cpt
     .      *tqcdh(amt**2/amh**2)
     .      * ((cpt-1)
     .         +(1+elw(amh,amt,amb,2/3.d0,1/2.d0))
     .         *hffself(amh) * sm4facf)
        endif
        rat = 2*amt/amh
        htt = qqint_hdec(rat,ht1,ht2)
       endif
      endif
c     htt=htt*sm4facf
      if(ifermphob.ne.0)then
c      write(6,*)'fermiophobic: h -> mu mu,tau tau,ss,cc,bb,tt ---> 0'
       hmm = 0
       hll = 0
       hss = 0
       hcc = 0
       hbb = 0
       htt = 0
      endif
c  h ---> gamma gamma
       eps=1.d-8
       xrmc = runm_hdec(amh/2,4)*amc/runm_hdec(amc,4)
       xrmb = runm_hdec(amh/2,5)*amb/runm_hdec(amb,5)
       xrmt = runm_hdec(amh/2,6)*amt/runm_hdec(amt,6)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      xrmc = amc
c      xrmb = amb
c      xrmt = amt
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       ctt = 4*xrmt**2/amh**2*dcmplx(1d0,-eps)
       ctb = 4*xrmb**2/amh**2*dcmplx(1d0,-eps)
       ctc = 4*xrmc**2/amh**2*dcmplx(1d0,-eps)
       ctl = 4*amtau**2/amh**2*dcmplx(1d0,-eps)
       ctw = 4*amw**2/amh**2*dcmplx(1d0,-eps)
       caw = -(2+3*ctw+3*ctw*(2-ctw)*cf(ctw)) * cpw
       caw00= -(2+3*ctw+3*ctw*(2-ctw)*cf(ctw))
       cat00= 4/3d0 * 2*ctt*(1+(1-ctt)*cf(ctt))
       cab00= 1/3d0 * 2*ctb*(1+(1-ctb)*cf(ctb))
       cac00= 4/3d0 * 2*ctc*(1+(1-ctc)*cf(ctc))
       cal00 =         2*ctl*(1+(1-ctl)*cf(ctl))
       cat0= 4/3d0 * 2*ctt*(1+(1-ctt)*cf(ctt)) * cpt
       cab0= 1/3d0 * 2*ctb*(1+(1-ctb)*cf(ctb)) * cpb
       cac0= 4/3d0 * 2*ctc*(1+(1-ctc)*cf(ctc)) * cpc
       cat = 4/3d0 * 2*ctt*(1+(1-ctt)*cf(ctt)) * cpt
     .     * cfacq_hdec(0,amh,xrmt)
       cab = 1/3d0 * 2*ctb*(1+(1-ctb)*cf(ctb)) * cpb
     .     * cfacq_hdec(0,amh,xrmb)
       cac = 4/3d0 * 2*ctc*(1+(1-ctc)*cf(ctc)) * cpc
     .     * cfacq_hdec(0,amh,xrmc)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c--default
c     topscale0 = amh/2
c     xfac = cdabs(cat+cab+cac+cal+caw)**2
c     xxx0 = cdabs(cat00+cab00+cac00+cal+caw)**2
c--pole mass
c     dshift = dlog(amt**2/topscale0**2)
c     ctrun=-cdct(amh,amt)*dshift*alphas_hdec(amh,3)/pi
c     cctt0 = 4*amt**2/amh**2*dcmplx(1d0,-eps)
c     cat1 = 4/3d0 * 2*cctt0*(1+(1-cctt0)*cf(cctt0))
c    .     * (cfacq_hdec(0,amh,amt)+ctrun)
c     xfac0 = cdabs(cat0+cab+cac+cal+caw)**2
c--scale = m_h/4
c     topscale = amh/4
c     dshift = dlog(topscale**2/topscale0**2)
c     xrmt0 = runm_hdec(topscale,6)*amt/runm_hdec(amt,6)
c     ctrun=-cdct(amh,xrmt0)*dshift*alphas_hdec(amh,3)/pi
c     ctt1 = 4*xrmt0**2/amh**2*dcmplx(1d0,-eps)
c     cat1 = 4/3d0 * 2*ctt1*(1+(1-ctt1)*cf(ctt1))
c    .     * (cfacq_hdec(0,amh,xrmt0)+ctrun)
c     xfac1 = cdabs(cat1+cab+cac+cal+caw)**2
c     dshift = dlog(topscale**2/topscale0**2)+4/3.d0
c     xrmt0 = runm_hdec(topscale,6)
c     ctrun=-cdct(amh,xrmt0)*dshift*alphas_hdec(amh,3)/pi
c     ctt1 = 4*xrmt0**2/amh**2*dcmplx(1d0,-eps)
c     cat1 = 4/3d0 * 2*ctt1*(1+(1-ctt1)*cf(ctt1))
c    .     * (cfacq_hdec(0,amh,xrmt0)+ctrun)
c     xfac10= cdabs(cat1+cab+cac+cal+caw)**2
c--scale = m_h
c     topscale = amh
c     dshift = dlog(topscale0**2/topscale**2)
c     xrmt0 = runm_hdec(topscale,6)*amt/runm_hdec(amt,6)
c     ctrun=-cdct(amh,xrmt0)*dshift*alphas_hdec(amh,3)/pi
c     ctt2 = 4*xrmt0**2/amh**2*dcmplx(1d0,-eps)
c     cat2 = 4/3d0 * 2*ctt2*(1+(1-ctt2)*cf(ctt2))
c    .     * (cfacq_hdec(0,amh,xrmt0)+ctrun)
c     xfac2 = cdabs(cat2+cab+cac+cal+caw)**2
c     dshift = dlog(topscale**2/topscale0**2)+4/3.d0
c     xrmt0 = runm_hdec(topscale,6)
c     ctrun=-cdct(amh,xrmt0)*dshift*alphas_hdec(amh,3)/pi
c     ctt2 = 4*xrmt0**2/amh**2*dcmplx(1d0,-eps)
c     cat2 = 4/3d0 * 2*ctt2*(1+(1-ctt2)*cf(ctt2))
c    .     * (cfacq_hdec(0,amh,xrmt0)+ctrun)
c     xfac20= cdabs(cat2+cab+cac+cal+caw)**2
c     write(6,*)'h -> gamma gamma: ',xfac,cdabs(cat+caw)**2,
c    .          cdabs(caw)**2,cdabs(cat)**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)xfac0/xfac,xfac1/xfac,xfac2/xfac
c     write(6,*)xfac10/xfac,xfac20/xfac
c     write(6,*)xfac0/xxx0,xfac1/xxx0,xfac2/xxx0
c     write(6,*)xfac10/xxx0,xfac20/xxx0
c     xrho = 100.d0
c     amx = dsqrt(xrho)*amt
c     topscale0= amx/2
c     topscale = amt
c     dshift = dlog(topscale**2/topscale0**2)+0*4/3.d0
c     ctrun=-cdct(amx,amt)*dshift*alphas_hdec(amx,3)/pi
c     write(6,*)'cfactor: ',xrho,
c    .     dreal(cfacq_hdec(0,amx,amt)+ctrun-1)/alphas_hdec(amx,3)*pi
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       cal =         2*ctl*(1+(1-ctl)*cf(ctl)) * cptau
       catp = 0
       cabp = 0
       caep = 0
       catp0 = 0
       cabp0 = 0
       catp00 = 0
       cabp00 = 0
       caep00 = 0
       xfac0= cdabs(cat0+cab0+cac0+cal+caw+cpgaga)**2
     .      * ioelw*gaga_elw(amt,amh)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       cctt = 4*amt**2/amh**2*dcmplx(1d0,-eps)
       ccat = 4/3d0 * 2*ctt*(1+(1-ctt)*cf(ctt))
       cct  = cfacq_hdec(0,amh,amt) - 1
c      write(6,*)'h -> gamma gamma: ',caw,ccat,cct,alphas_hdec(amh,3)/pi
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      cdum0 = cat0+caw
c      cdum  = cat +caw
c      ctot0= caw/cdum0
c      cdt0 = cat0/caw
c      cdb0 = cab0/caw
c      ctot = caw/cdum
c      cdt  = cat/caw
c      cdb  = cab/caw
c      write(6,*)'h -> gamma gamma',ctot,cdt,cdb
c      write(6,*)'h -> gamma gamma',ctot0,cdt0,cdb0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       dum1 = cdabs(cat+cab+cac+cal+caw)**2
c       ckofq = ckofq_hdec(0,amh**2/xrmt**2)
c       cfacq = ckofq*alphas_hdec(amh,3)/pi
c       cfacq0 = -alphas_hdec(amh,3)/pi
c       catx = 4/3d0 * 2*ctt*(1+(1-ctt)*cf(ctt)) * cpt
c    .       * (1+cfacq0)
c       dum2 = cdabs(catx+cab+cac+cal+caw)**2
c       write(6,*)'h -> gamma gamma: ',ckofq,cfacq,dum1
c       write(6,*)'h -> gamma gamma: ',-1.d0,cfacq0,dum2,dum1/dum2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       if(icoupelw.eq.0)then
c       xfac0= cdabs(cat00+cab00+cac00+cal00+caw00)**2*gaga_elw(amt,amh)
        xfac0= dreal(dconjg(cat0+cab0+cac0+cal+caw+cpgaga)
     .       * (cat00+cab00+cac00+cal00+caw00))
     .       * ioelw*gaga_elw(amt,amh)
       endif
       if(ism4.ne.0)then
        xrmbp = runm_hdec(amh/2,7)*ambp/runm_hdec(ambp,7)
        xrmtp = runm_hdec(amh/2,8)*amtp/runm_hdec(amtp,8)
        cttp = 4*xrmtp**2/amh**2*dcmplx(1d0,-eps)
        ctbp = 4*xrmbp**2/amh**2*dcmplx(1d0,-eps)
        ctep = 4*amep**2/amh**2*dcmplx(1d0,-eps)
        catp00= 4/3d0 * 2*cttp*(1+(1-cttp)*cf(cttp))
        cabp00= 1/3d0 * 2*ctbp*(1+(1-ctbp)*cf(ctbp))
        caep00=         2*ctep*(1+(1-ctep)*cf(ctep))
        catp0= 4/3d0 * 2*cttp*(1+(1-cttp)*cf(cttp)) * cptp
        cabp0= 1/3d0 * 2*ctbp*(1+(1-ctbp)*cf(ctbp)) * cpbp
        catp = 4/3d0 * 2*cttp*(1+(1-cttp)*cf(cttp)) * cptp
     .       * cfacq_hdec(0,amh,xrmtp)
        cabp = 1/3d0 * 2*ctbp*(1+(1-ctbp)*cf(ctbp)) * cpbp
     .       * cfacq_hdec(0,amh,xrmbp)
        caep =         2*ctep*(1+(1-ctep)*cf(ctep)) * cpep
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       catp = 0
c       cabp = 0
c       caep = 0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        if(amnup.eq.amep)then
         xxl = amep**2
        else
         xxl =amnup**2*amep**2/(amnup**2-amep**2)*dlog(amnup**2/amep**2)
        endif
        if(amtp.eq.ambp)then
         xxq = amtp**2
        else
         xxq = amtp**2*ambp**2/(amtp**2-ambp**2)*dlog(amtp**2/ambp**2)
        endif
        fft = -6
        fft = 1
        celw = gf/8/dsqrt(2.d0)/pi**2*(-49*amt**2/2*fft
     .       + 7*amnup**2/6 - 65*amep**2/6 - xxl
     .       - 237*amtp**2/10 - 117*ambp**2/10 - 3*xxq)
        celw = celw*(1-0.614d0*amh**2/4/amw**2)
        xfac0= cdabs(cat0+cab0+cac0+cal+caw+catp0+cabp0+caep+cpgaga)**2
     .       * ((1+celw)**2 - 1)
       if(icoupelw.eq.0)then
        xfac0= dreal(dconjg(cat0+cab0+cac0+cal+caw+catp0+cabp0+caep
     .                     +cpgaga)
     .       * (cat00+cab00+cac00+cal00+caw00+catp00+cabp00+caep00))
     .       * ((1+celw)**2 - 1)
       endif
c       write(6,*)'h -> ga ga: ',amh,2*celw*100,((1+celw)**2-1)*100
c       write(6,*)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       xfac0= 0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       endif
       if(ifermphob.ne.0)then
c       write(6,*)'fermiophobic: h -> gamma gamma ---> w loop'
        cal = 0
        cac = 0
        cab = 0
        cat = 0
        xfac0= 0
c       write(6,*)cdabs(caw)**2,cdabs(cat+cab+cac+cal+caw)**2
       endif
       xfac = cdabs(cat+cab+cac+cal+caw+catp+cabp+caep+cpgaga)**2
       hga=hvv(amh,0.d0)*(alph/pi)**2/16.d0*(xfac+xfac0)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      xxx0 = cdabs(cat00+cab00+cac00+cal+caw)**2
c      xxx1 = cdabs(cat+cab+cac+cal+caw)**2
c      write(6,*)'h->gaga: ',amh,hga/(xfac+xfac0)*xxx0*1.d6,
c    .                           hga/(xfac+xfac0)*xxx1*1.d6
c      write(9,*)amh,hga/(xfac+xfac0)*xxx0*1.d6,
c    .               hga/(xfac+xfac0)*xxx1*1.d6
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)'h -> gamma gamma: nlo qcd, approx ',
c    .  cdabs(cat+cab+cac+cal+caw)**2/cdabs(cat0+cab0+cac0+cal+caw)**2,
c    .  cdabs(cat0*(1-alphas_hdec(amh/2)/pi)+cab0+cac0+cal+caw)**2
c    . /cdabs(cat0+cab0+cac0+cal+caw)**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)'h -> gamma gamma: ',xfac,xfac0,hga,ioelw
c      write(6,*)'h -> gamma gamma: ',cat0,cab0,cac0,cal,caw
c    .                               ,catp0,cabp0,caep,cgaga
c      write(6,*)'h -> gamma gamma: ',cat00,cab00,cac00,cal00,caw00
c    .                               ,catp00,cabp00,caep00
c      write(6,*)'h -> gamma gamma: ',amh,xrmt,xrmb,xrmc
c      write(6,*)'h -> gamma gamma: ',
c    .            cdabs(cat+cab+cac+cal+caw)**2/cdabs(cat+caw)**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)'h -> gamma gamma: ',
c    .           dsqrt(xfac)/8
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       write(6,*)'h gamma gamma: ',(16/9.d0-7)/8,(cat0+caw)/8,
c    .                              (cat0+cab0+cac0+cal+caw)/8
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      hga=hvv(amh,0.d0)*(alph/pi)**2/16.d0*xfac
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)'h -> gamma gamma: ',hga,xfac0/xfac
c      write(6,*)'gaga: ',amh,cat,caw,cat+caw
c      write(6,*)'gaga: ',amh,cat0,caw,cat0+caw
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      yy0 = cdabs(cat+cac)**2 + cdabs(cat0+cac0)**2*gaga_elw(amt,amh)
c      ytt = hvv(amh,0.d0)*(alph/pi)**2/16.d0*yy0 / hga
c      yy0 = cdabs(cab)**2 + cdabs(cab0)**2*gaga_elw(amt,amh)
c      ybb = hvv(amh,0.d0)*(alph/pi)**2/16.d0*yy0 / hga
c      yy0 = cdabs(caw)**2 + cdabs(caw)**2*gaga_elw(amt,amh)
c      yww = hvv(amh,0.d0)*(alph/pi)**2/16.d0*yy0 / hga
c      yy0 = cdabs(cal)**2 + cdabs(cal)**2*gaga_elw(amt,amh)
c      yll = hvv(amh,0.d0)*(alph/pi)**2/16.d0*yy0 / hga
c      yy0 = 2*dreal(dconjg(cat+cac)*cab)
c    .     + 2*dreal(dconjg(cat0+cac0)*cab0)*gaga_elw(amt,amh)
c      ytb = hvv(amh,0.d0)*(alph/pi)**2/16.d0*yy0 / hga
c      yy0 = 2*dreal(dconjg(cat+cac)*caw)
c    .     + 2*dreal(dconjg(cat0+cac0)*caw)*gaga_elw(amt,amh)
c      ytw = hvv(amh,0.d0)*(alph/pi)**2/16.d0*yy0 / hga
c      yy0 = 2*dreal(dconjg(cab)*caw)
c    .     + 2*dreal(dconjg(cab0)*caw)*gaga_elw(amt,amh)
c      ybw = hvv(amh,0.d0)*(alph/pi)**2/16.d0*yy0 / hga
c      yy0 = 2*dreal(dconjg(cat+cac)*cal)
c    .     + 2*dreal(dconjg(cat0+cac0)*cal)*gaga_elw(amt,amh)
c      ytl = hvv(amh,0.d0)*(alph/pi)**2/16.d0*yy0 / hga
c      yy0 = 2*dreal(dconjg(cab)*cal)
c    .     + 2*dreal(dconjg(cab0)*cal)*gaga_elw(amt,amh)
c      ybl = hvv(amh,0.d0)*(alph/pi)**2/16.d0*yy0 / hga
c      yy0 = 2*dreal(dconjg(cal)*caw)
c    .     + 2*dreal(dconjg(cal)*caw)*gaga_elw(amt,amh)
c      ylw = hvv(amh,0.d0)*(alph/pi)**2/16.d0*yy0 / hga
c      write(52,('f4.0,7(g13.5)'))amh,hga,ytt,ybb,yww,ytb,ytw,ybw
c    .                            ,yll,ytl,ybl,ylw
c      write(6,*)'gaga: ',amh,ytt+ybb+yww+ytb+ytw+ybw+yll+ytl+ybl+ylw
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)amh,(xfac/xfac0*gaga_elw(amt,amh)-1)*100,
c    .               gaga_elw(amt,amh)*100
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      cat0 = 4/3d0 * 2*ctt*(1+(1-ctt)*cf(ctt))
c      cab0 = 1/3d0 * 2*ctb*(1+(1-ctb)*cf(ctb))
c      cac0 = 4/3d0 * 2*ctc*(1+(1-ctc)*cf(ctc))
c      xfaclo = cdabs(cat0+cab0+cac0+cal+caw)**2
c      write(54,('4(1x,e12.6)'))amh,hga,hga*xfaclo/xfac,xfac/xfaclo-1
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)cdabs(cat0+caw)**2,(16/9.d0-7)**2,
c    .           (16/9.d0-7)**2/cdabs(cat0+caw)**2,16/9.d0,cat0,-7,caw
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c  h ---> z gamma
      xrmc = runm_hdec(amh/2,4)*amc/runm_hdec(amc,4)
      xrmb = runm_hdec(amh/2,5)*amb/runm_hdec(amb,5)
      xrmt = runm_hdec(amh/2,6)*amt/runm_hdec(amt,6)
      if(amh.le.amz)then
       hzga=0
      else
       eps=1.d-8
       ts = ss/cs
       ft = -3*2d0/3*(1-4*2d0/3*ss)/dsqrt(ss*cs) * cpt
       fb = 3*1d0/3*(-1+4*1d0/3*ss)/dsqrt(ss*cs) * cpb
       fc = -3*2d0/3*(1-4*2d0/3*ss)/dsqrt(ss*cs) * cpc
       fl = (-1+4*ss)/dsqrt(ss*cs) * cptau
c      ctt = 4*xrmt**2/amh**2*dcmplx(1d0,-eps)
c      ctb = 4*xrmb**2/amh**2*dcmplx(1d0,-eps)
c      ctc = 4*xrmc**2/amh**2*dcmplx(1d0,-eps)
       ctt = 4*amt**2/amh**2*dcmplx(1d0,-eps)
       ctb = 4*amb**2/amh**2*dcmplx(1d0,-eps)
       ctc = 4*amc**2/amh**2*dcmplx(1d0,-eps)
       ctl = 4*amtau**2/amh**2*dcmplx(1d0,-eps)
       ctw = 4*amw**2/amh**2*dcmplx(1d0,-eps)
c      clt = 4*xrmt**2/amz**2*dcmplx(1d0,-eps)
c      clb = 4*xrmb**2/amz**2*dcmplx(1d0,-eps)
c      clc = 4*xrmc**2/amz**2*dcmplx(1d0,-eps)
       clt = 4*amt**2/amz**2*dcmplx(1d0,-eps)
       clb = 4*amb**2/amz**2*dcmplx(1d0,-eps)
       clc = 4*amc**2/amz**2*dcmplx(1d0,-eps)
       cle = 4*amtau**2/amz**2*dcmplx(1d0,-eps)
       clw = 4*amw**2/amz**2*dcmplx(1d0,-eps)
       cat = ft*(ci1(ctt,clt) - ci2(ctt,clt))
       cab = fb*(ci1(ctb,clb) - ci2(ctb,clb))
       cac = fc*(ci1(ctc,clc) - ci2(ctc,clc))
       cal = fl*(ci1(ctl,cle) - ci2(ctl,cle))
       caw = -1/dsqrt(ts)*(4*(3-ts)*ci2(ctw,clw)
     .     + ((1+2/ctw)*ts - (5+2/ctw))*ci1(ctw,clw)) * cpw
       catp = 0
       cabp = 0
       caep = 0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)6*ci1(ctt,clt),2*ci2(ctt,clt)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      eps = 1.d-30
c      qq = 2*amt - 1
c      clt = 4*amt**2/qq**2*dcmplx(1d0,-eps)
c      cat = ft*(ci1(ctt,clt) - ci2(ctt,clt))
c      write(6,*)qq,cat
c      qq = 2*amt - 0.1d0
c      clt = 4*amt**2/qq**2*dcmplx(1d0,-eps)
c      cat = ft*(ci1(ctt,clt) - ci2(ctt,clt))
c      write(6,*)qq,cat
c      qq = 2*amt - 0.01d0
c      clt = 4*amt**2/qq**2*dcmplx(1d0,-eps)
c      cat = ft*(ci1(ctt,clt) - ci2(ctt,clt))
c      write(6,*)qq,cat
c      qq = 2*amt - 1.d-8
c      clt = 4*amt**2/qq**2*dcmplx(1d0,-eps)
c      cat = ft*(ci1(ctt,clt) - ci2(ctt,clt))
c      write(6,*)qq,cat
c      qq = 2*amt
c      clt = 4*amt**2/qq**2*dcmplx(1d0,-eps)
c      cat = ft*(ci1(ctt,clt) - ci2(ctt,clt))
c      write(6,*)qq,cat
c      qq = 2*amt + 1.d-8
c      clt = 4*amt**2/qq**2*dcmplx(1d0,-eps)
c      cat = ft*(ci1(ctt,clt) - ci2(ctt,clt))
c      write(6,*)qq,cat
c      qq = 2*amt + 0.01d0
c      clt = 4*amt**2/qq**2*dcmplx(1d0,-eps)
c      cat = ft*(ci1(ctt,clt) - ci2(ctt,clt))
c      write(6,*)qq,cat
c      qq = 2*amt + 0.1d0
c      clt = 4*amt**2/qq**2*dcmplx(1d0,-eps)
c      cat = ft*(ci1(ctt,clt) - ci2(ctt,clt))
c      write(6,*)qq,cat
c      qq = 2*amt+1
c      clt = 4*amt**2/qq**2*dcmplx(1d0,-eps)
c      cat = ft*(ci1(ctt,clt) - ci2(ctt,clt))
c      write(6,*)qq,cat
c      clt = 4*amt**2/amz**2*dcmplx(1d0,-eps)
c      cat = ft*(ci1(ctt,clt) - ci2(ctt,clt))
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       if(ism4.ne.0)then
        ftp = -3*2d0/3*(1-4*2d0/3*ss)/dsqrt(ss*cs) * cptp
        fbp = 3*1d0/3*(-1+4*1d0/3*ss)/dsqrt(ss*cs) * cpbp
        fep = (-1+4*ss)/dsqrt(ss*cs) * cpep
        cttp = 4*amtp**2/amh**2*dcmplx(1d0,-eps)
        ctbp = 4*ambp**2/amh**2*dcmplx(1d0,-eps)
        ctep = 4*amep**2/amh**2*dcmplx(1d0,-eps)
        cltp = 4*amtp**2/amz**2*dcmplx(1d0,-eps)
        clbp = 4*ambp**2/amz**2*dcmplx(1d0,-eps)
        clep = 4*amep**2/amz**2*dcmplx(1d0,-eps)
        catp = ftp*(ci1(cttp,cltp) - ci2(cttp,cltp))
        cabp = fbp*(ci1(ctbp,clbp) - ci2(ctbp,clbp))
        caep = fep*(ci1(ctep,clep) - ci2(ctep,clep))
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       catp = 0
c       cabp = 0
c       caep = 0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       endif
       if(ifermphob.ne.0)then
c       write(6,*)'fermiophobic: h -> z gamma ---> w loop'
        cab = 0
        cat = 0
        cal = 0
c       write(6,*)cdabs(caw)**2,cdabs(cat+cab+caw)**2
       endif
       fptlike = cpzga
       xfac = cdabs(cat+cab+cac+cal+caw+catp+cabp+caep+fptlike)**2
       acoup = dsqrt(2d0)*gf*amz**2*ss*cs/pi**2
       hzga = gf/(4.d0*pi*dsqrt(2.d0))*amh**3*(alph/pi)*acoup/16.d0
     .        *xfac*(1-amz**2/amh**2)**3
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)'h -> z gamma: ',
c    .           1-cdabs(cab+cac+cal+caw+catp+cabp+caep)**2/xfac
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      yy0 = cdabs(cat+cac)**2
c      ytt = gf/(4.d0*pi*dsqrt(2.d0))*amh**3*(alph/pi)*acoup/16.d0
c    .     * yy0*(1-amz**2/amh**2)**3 / hzga
c      yy0 = cdabs(cab)**2
c      ybb = gf/(4.d0*pi*dsqrt(2.d0))*amh**3*(alph/pi)*acoup/16.d0
c    .     * yy0*(1-amz**2/amh**2)**3 / hzga
c      yy0 = cdabs(caw)**2
c      yww = gf/(4.d0*pi*dsqrt(2.d0))*amh**3*(alph/pi)*acoup/16.d0
c    .     * yy0*(1-amz**2/amh**2)**3 / hzga
c      yy0 = cdabs(cal)**2
c      yll = gf/(4.d0*pi*dsqrt(2.d0))*amh**3*(alph/pi)*acoup/16.d0
c    .     * yy0*(1-amz**2/amh**2)**3 / hzga
c      yy0 = 2*dreal(dconjg(cat+cac)*cab)
c      ytb = gf/(4.d0*pi*dsqrt(2.d0))*amh**3*(alph/pi)*acoup/16.d0
c    .     * yy0*(1-amz**2/amh**2)**3 / hzga
c      yy0 = 2*dreal(dconjg(cat+cac)*caw)
c      ytw = gf/(4.d0*pi*dsqrt(2.d0))*amh**3*(alph/pi)*acoup/16.d0
c    .     * yy0*(1-amz**2/amh**2)**3 / hzga
c      yy0 = 2*dreal(dconjg(cab)*caw)
c      ybw = gf/(4.d0*pi*dsqrt(2.d0))*amh**3*(alph/pi)*acoup/16.d0
c    .     * yy0*(1-amz**2/amh**2)**3 / hzga
c      yy0 = 2*dreal(dconjg(cat+cac)*cal)
c      ytl = gf/(4.d0*pi*dsqrt(2.d0))*amh**3*(alph/pi)*acoup/16.d0
c    .     * yy0*(1-amz**2/amh**2)**3 / hzga
c      yy0 = 2*dreal(dconjg(cab)*cal)
c      ybl = gf/(4.d0*pi*dsqrt(2.d0))*amh**3*(alph/pi)*acoup/16.d0
c    .     * yy0*(1-amz**2/amh**2)**3 / hzga
c      yy0 = 2*dreal(dconjg(cal)*caw)
c      ylw = gf/(4.d0*pi*dsqrt(2.d0))*amh**3*(alph/pi)*acoup/16.d0
c    .     * yy0*(1-amz**2/amh**2)**3 / hzga
c      write(53,('f4.0,11(g13.5)'))amh,hzga,ytt,ybb,yww,ytb,ytw,ybw
c    .                            ,yll,ytl,ybl,ylw
c      write(6,*)'zga: ',amh,ytt+ybb+yww+ytb+ytw+ybw+yll+ytl+ybl+ylw
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      xfac0 = cdabs(cat+cab+cal+caw+catp+cabp+caep)**2
c      write(6,*)'h -> z gamma: ',xfac/xfac0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      endif
c  h ---> w w
      if(ionwz.eq.0)then
       if(ifermphob.ne.0)then
        call htovv_hdec(0,amh,amw,gamw,htww)
        htww = htww*hvvself(amh)
       else
        call htovv_hdec(1,amh,amw,gamw,htww)
        call htovv_hdec(0,amh,amw,gamw,htww0)
        if(ioelw.eq.0) htww = htww0
        htww = htww * sm4facw
       endif
       hww = 3d0/2d0*gf*amw**4/dsqrt(2d0)/pi/amh**3*htww * cpw**2
       if(icoupelw.eq.0)then
        hww = 3d0/2d0*gf*amw**4/dsqrt(2d0)/pi/amh**3
     .      * cpw * (htww0*(cpw-1)+htww)
       endif
      elseif(ionwz.eq.-1)then
       dld=2d0
       dlu=2d0
       xm1 = 2d0*amw-dld
       xm2 = 2d0*amw+dlu
      if (amh.le.amw) then
       hww=0
      else if (amh.le.xm1) then
       cww=3.d0*gf**2*amw**4/16.d0/pi**3
       hww=hv(amw**2/amh**2)*cww*amh * cpw**2
     .      * sm4facw
       if(icoupelw.eq.0)then
        hww=hv(amw**2/amh**2)*cww*amh
     .      * cpw * ((cpw-1)+sm4facw)
       endif
      else if (amh.lt.xm2) then
       cww=3.d0*gf**2*amw**4/16.d0/pi**3
       xx(1) = xm1-1d0
       xx(2) = xm1
       xx(3) = xm2
       xx(4) = xm2+1d0
       yy(1)=hv(amw**2/xx(1)**2)*cww*xx(1) * sm4facw
       yy(2)=hv(amw**2/xx(2)**2)*cww*xx(2) * sm4facw
       yy(3)=hvv(xx(3),amw**2/xx(3)**2)
     .      *hvvself(xx(3)) * sm4facw
       yy(4)=hvv(xx(4),amw**2/xx(4)**2)
     .      *hvvself(xx(4)) * sm4facw
       if(icoupelw.eq.0)then
        yy(1)=hv(amw**2/xx(1)**2)*cww*xx(1)
     .       * cpw * ((cpw-1)+sm4facw)
        yy(2)=hv(amw**2/xx(2)**2)*cww*xx(2)
     .       * cpw * ((cpw-1)+sm4facw)
        yy(3)=hvv(xx(3),amw**2/xx(3)**2)
     .       * cpw * ((cpw-1)+hvvself(xx(3)) * sm4facw)
        yy(4)=hvv(xx(4),amw**2/xx(4)**2)
     .       * cpw * ((cpw-1)+hvvself(xx(4)) * sm4facw)
        endif
       hww = fint_hdec(amh,xx,yy)
      else
       hww=hvv(amh,amw**2/amh**2)
     .     *hvvself(amh) * sm4facw * cpw**2
       if(icoupelw.eq.0)then
        hww=hvv(amh,amw**2/amh**2)
     .      * cpw * ((cpw-1)+hvvself(amh) * sm4facw)
       endif
      endif
      else
       dld=2d0
       dlu=2d0
       xm1 = 2d0*amw-dld
       xm2 = 2d0*amw+dlu
       if (amh.le.xm1) then
        call htovv_hdec(0,amh,amw,gamw,htww)
        hww = 3d0/2d0*gf*amw**4/dsqrt(2d0)/pi/amh**3*htww * cpw**2
     .      * sm4facw
        if(icoupelw.eq.0)then
         hww = 3d0/2d0*gf*amw**4/dsqrt(2d0)/pi/amh**3*htww
     .       * cpw * ((cpw-1)+sm4facw)
        endif
       elseif (amh.le.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        call htovv_hdec(0,xx(1),amw,gamw,htww)
        yy(1)=3d0/2d0*gf*amw**4/dsqrt(2d0)/pi/xx(1)**3*htww * cpw**2
     .      * sm4facw
        if(icoupelw.eq.0)then
         yy(1)=3d0/2d0*gf*amw**4/dsqrt(2d0)/pi/xx(1)**3*htww
     .        * cpw * ((cpw-1) + sm4facw)
        endif
        call htovv_hdec(0,xx(2),amw,gamw,htww)
        yy(2)=3d0/2d0*gf*amw**4/dsqrt(2d0)/pi/xx(2)**3*htww * cpw**2
     .      * sm4facw
        if(icoupelw.eq.0)then
         yy(2)=3d0/2d0*gf*amw**4/dsqrt(2d0)/pi/xx(2)**3*htww * cpw**2
     .        * cpw * ((cpw-1) + sm4facw)
        endif
        yy(3)=hvv(xx(3),amw**2/xx(3)**2)
     .       *hvvself(xx(3)) * sm4facw * cpw**2
        yy(4)=hvv(xx(4),amw**2/xx(4)**2)
     .       *hvvself(xx(4)) * sm4facw * cpw**2
       if(icoupelw.eq.0)then
        yy(3)=hvv(xx(3),amw**2/xx(3)**2)
     .      * cpw * ((cpw-1) + hvvself(xx(3)) * sm4facw)
        yy(4)=hvv(xx(4),amw**2/xx(4)**2)
     .      * cpw * ((cpw-1) + hvvself(xx(4)) * sm4facw)
       endif
        hww = fint_hdec(amh,xx,yy)
       else
        hww=hvv(amh,amw**2/amh**2)
     .     *hvvself(amh) * sm4facw * cpw**2
        if(icoupelw.eq.0)then
         hww=hvv(amh,amw**2/amh**2)
     .      * cpw * ((cpw-1)+hvvself(amh) * sm4facw)
        endif
       endif
      endif
c     hww = hww * sm4facw
c  h ---> z z
      if(ionwz.eq.0)then
       if(ifermphob.ne.0)then
        call htovv_hdec(0,amh,amz,gamz,htzz)
        htzz = htzz*hvvself(amh)
       else
        call htovv_hdec(2,amh,amz,gamz,htzz)
        call htovv_hdec(0,amh,amz,gamz,htzz0)
        if(ioelw.eq.0) htzz = htzz0
        htzz = htzz * sm4facz
       endif
       hzz = 3d0/4d0*gf*amz**4/dsqrt(2d0)/pi/amh**3*htzz * cpz**2
       if(icoupelw.eq.0)then
        hzz = 3d0/4d0*gf*amz**4/dsqrt(2d0)/pi/amh**3
     .      * cpz * (htzz0*(cpz-1)+htzz)
       endif
      elseif(ionwz.eq.-1)then
       dld=2d0
       dlu=2d0
       xm1 = 2d0*amz-dld
       xm2 = 2d0*amz+dlu
       if (amh.le.amz) then
        hzz=0
       elseif (amh.le.xm1) then
        czz=3.d0*gf**2*amz**4/192.d0/pi**3*(7-40/3.d0*ss+160/9.d0*ss**2)
        hzz=hv(amz**2/amh**2)*czz*amh * cpz**2
     .      * sm4facz
        if(icoupelw.eq.0)then
         hzz=hv(amz**2/amh**2)*czz*amh
     .      * cpz * ((cpz-1)+sm4facz)
        endif
       elseif (amh.lt.xm2) then
        czz=3.d0*gf**2*amz**4/192.d0/pi**3*(7-40/3.d0*ss+160/9.d0*ss**2)
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        yy(1)=hv(amz**2/xx(1)**2)*czz*xx(1) * cpz**2
     .      * sm4facz
        yy(2)=hv(amz**2/xx(2)**2)*czz*xx(2) * cpz**2
     .      * sm4facz
        yy(3)=hvv(xx(3),amz**2/xx(3)**2)/2
     .       *hvvself(xx(3)) * sm4facz * cpz**2
        yy(4)=hvv(xx(4),amz**2/xx(4)**2)/2
     .       *hvvself(xx(4)) * sm4facz * cpz**2
        if(icoupelw.eq.0)then
         yy(1)=hv(amz**2/xx(1)**2)*czz*xx(1)
     .        * cpz * ((cpz-1)+sm4facz)
         yy(2)=hv(amz**2/xx(2)**2)*czz*xx(2)
     .        * cpz * ((cpz-1)+sm4facz)
         yy(3)=hvv(xx(3),amz**2/xx(3)**2)/2
     .        * cpz * ((cpz-1)+hvvself(xx(3)) * sm4facz)
         yy(4)=hvv(xx(4),amz**2/xx(4)**2)/2
     .        * cpz * ((cpz-1)+hvvself(xx(4)) * sm4facz)
        endif
        hzz = fint_hdec(amh,xx,yy)
       else
        hzz=hvv(amh,amz**2/amh**2)/2.d0
     .     *hvvself(amh) * sm4facz * cpz**2
        if(icoupelw.eq.0)then
         hzz=hvv(amh,amz**2/amh**2)/2.d0
     .      * cpz * ((cpz-1)+hvvself(amh) * sm4facz)
        endif
       endif
      else
       dld=2d0
       dlu=2d0
       xm1 = 2d0*amz-dld
       xm2 = 2d0*amz+dlu
       if (amh.le.xm1) then
        call htovv_hdec(0,amh,amz,gamz,htzz)
        hzz = 3d0/4d0*gf*amz**4/dsqrt(2d0)/pi/amh**3*htzz
     .      * sm4facz
        if(icoupelw.eq.0)then
         hzz = 3d0/4d0*gf*amz**4/dsqrt(2d0)/pi/amh**3*htzz
     .       * cpz * ((cpz-1)+sm4facz)
        endif
       elseif (amh.le.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        call htovv_hdec(0,xx(1),amz,gamz,htzz)
        yy(1)=3d0/4d0*gf*amz**4/dsqrt(2d0)/pi/xx(1)**3*htzz * cpz**2
     .       * sm4facz
        if(icoupelw.eq.0)then
         yy(1)=3d0/4d0*gf*amz**4/dsqrt(2d0)/pi/xx(1)**3*htzz
     .        * cpz * ((cpz-1) + sm4facz)
        endif
        call htovv_hdec(0,xx(2),amz,gamz,htzz)
        yy(2)=3d0/4d0*gf*amz**4/dsqrt(2d0)/pi/xx(2)**3*htzz * cpz**2
     .       * sm4facz
        if(icoupelw.eq.0)then
         yy(2)=3d0/4d0*gf*amz**4/dsqrt(2d0)/pi/xx(2)**3*htzz
     .        * cpz * ((cpz-1) + sm4facz)
        endif
        yy(3)=hvv(xx(3),amz**2/xx(3)**2)/2
     .       *hvvself(xx(3)) * sm4facz * cpz**2
        yy(4)=hvv(xx(4),amz**2/xx(4)**2)/2
     .       *hvvself(xx(4)) * sm4facz * cpz**2
        if(icoupelw.eq.0)then
         yy(3)=hvv(xx(3),amz**2/xx(3)**2)/2
     .        * cpz * ((cpz-1)+hvvself(xx(3)))
         yy(4)=hvv(xx(4),amz**2/xx(4)**2)/2
     .        * cpz * ((cpz-1)+hvvself(xx(4)))
        endif
        hzz = fint_hdec(amh,xx,yy)
       else
        hzz=hvv(amh,amz**2/amh**2)/2.d0
     .      *hvvself(amh) * sm4facz * cpz**2
       if(icoupelw.eq.0)then
        hzz=hvv(amh,amz**2/amh**2)/2.d0
     .     * cpz * ((cpz-1)+hvvself(amh) * sm4facz)
       endif
       endif
      endif
c     hzz = hzz * sm4facz
c     write(62,*)amh,1.d0+gf*amh**2/16.d0/pi**2/dsqrt(2.d0)*2.800952d0,
c    .               hvvself(amh)
c     write(91,*)amh,hww,hzz

c     call htovv_hdec(0,amh,amw,gamw,htww0)
c     call htovv_hdec(1,amh,amw,gamw,htww1)
c     call htovv_hdec(0,amh,amz,gamz,htzz0)
c     call htovv_hdec(2,amh,amz,gamz,htzz1)
c     write(6,*)amh,htww1/htww0,htzz1/htzz0
c
      hnupnup = 0
      hepep = 0
      hbpbp = 0
      htptp = 0
      if(ism4.ne.0)then
c  h ---> nup nup
       if(amh.le.2*amnup) then
        hnupnup = 0
       else
        hnupnup=hff(amh,(amnup/amh)**2) * cpnup**2
     .         *hffself(amh)
       endif
c  h ---> ep ep
       if(amh.le.2*amep) then
        hepep = 0
       else
        hepep=hff(amh,(amep/amh)**2) * cpep**2
     .       *hffself(amh)
       endif
c  h --> bp bp :
       if(amh.le.2*ambp) then
        hbpbp = 0
       else
        hbp2=3.d0*hff(amh,(rmbp/amh)**2) * cpbp**2
     .     *qcdh(rmbp**2/amh**2)
     .     *hffself(amh)
        if(hbp2.lt.0.d0) hbp2 = 0
        hbp1=3.d0*hff(amh,(ambp/amh)**2) * cpbp**2
     .     *tqcdh(ambp**2/amh**2)
     .     *hffself(amh)
        rat = 2*ambp/amh
        hbpbp = qqint_hdec(rat,hbp1,hbp2)
       endif
c  h --> tp tp :
       if(amh.le.2*amtp) then
        htptp = 0
       else
        htp2=3.d0*hff(amh,(rmtp/amh)**2) * cptp**2
     .     *qcdh(rmtp**2/amh**2)
     .     *hffself(amh)
        if(htp2.lt.0.d0) htp2 = 0
        htp1=3.d0*hff(amh,(amtp/amh)**2) * cptp**2
     .     *tqcdh(amtp**2/amh**2)
     .     *hffself(amh)
        rat = 2*amtp/amh
        htptp = qqint_hdec(rat,htp1,htp2)
       endif
      endif
c    ==========  total width and branching ratios
c
      wtot=hll+hmm+hss+hcc+hbb+htt+hgg+hga+hzga+hww+hzz
     .    +hnupnup+hepep+hbpbp+htptp
c    .    +hee
c     write(6,*)'br(h -> ee) = ',amh,hee/wtot
c     write(6,*)'part: ',hll,hmm,hss,hcc,hbb,htt,hgg,hga,hzga,hww,hzz
c     write(6,*)hbb,hww,hzz
      if(wtot.ne.0.d0)then
       smbrt=htt/wtot
       smbrb=hbb/wtot
       smbrl=hll/wtot
       smbrm=hmm/wtot
       smbrc=hcc/wtot
       smbrs=hss/wtot
       smbrg=hgg/wtot
       smbrga=hga/wtot
       smbrzga=hzga/wtot
       smbrw=hww/wtot
       smbrz=hzz/wtot
       smbrnup=hnupnup/wtot
       smbrep=hepep/wtot
       smbrbp=hbpbp/wtot
       smbrtp=htptp/wtot
      else
       smbrt=0
       smbrb=0
       smbrl=0
       smbrm=0
       smbrc=0
       smbrs=0
       smbrg=0
       smbrga=0
       smbrzga=0
       smbrw=0
       smbrz=0
       smbrnup=0
       smbrep=0
       smbrbp=0
       smbrtp=0
      endif
      smwdth=wtot

c     write(6,*)hll,hmm
c     write(6,*)hss,hcc,hbb,htt
c     write(6,*)hgg,hga,hzga,hww,hzz
c     write(6,*)hnupnup,hepep,hbpbp,htptp
c     write(6,*)wtot
c     write(6,*)smbrt+smbrb+smbrl+smbrm+smbrc+smbrs+smbrg+smbrga
c    .         +smbrzga+smbrw+smbrz+smbrnup+smbrep+smbrbp+smbrtp

      amh=amxx

      endif

      if(ihiggs.gt.0)then

c +++++++++++++++++++++++  susy higgsses +++++++++++++++++++++++
c
      call gaugino_hdec(amu,am2,b,a,amchar,amneut,xmneut,ac1,ac2,ac3,
     .             an1,an2,an3,acnl,acnr,agdl,agda,agdh,agdc)
c
      tsc = (amsq+amur+amdr)/3
      bsc = (amsq+amur+amdr)/3
      call sfermion_hdec(tsc,bsc,amsq,amur,amdr,amel,amer,al,au,ad,amu,
     .               amst,amsb,amsl,amsu,amsd,amse,amsn,amsn1,
     .               glee,gltt,glbb,ghee,ghtt,ghbb,
     .               gaee,gatt,gabb,gcen,gctb)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     qsusy = 1.d0/3
c     qsusy = 1
c     qsusy = 3
c     loop = 1
c     qsusy1 = qsusy
c     qsusy2 = qsusy
c     loop = 2
c     qsusy1 = qsusy
c     qsusy2 = 1/qsusy
c     write(6,*)'loop, factor = ?'
c     read(5,*)loop,qsusy
c     qsusy = dmin1(amsb(1),amsb(2),amglu)*qsusy
c     qsusy = (amsb(1)+amsb(2)+amglu)/3*qsusy
c     qsusy = +0.8204315362167340d3
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     qsusy = qsusy1
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c--real!!!
      qsusy = 1
      loop = 2
      qsusy1 = qsusy
      qsusy2 = qsusy
c

c     write(6,*)'m_a, m_h, m_h, sin(alpha): ',ama,aml,amh,dsin(a)

      endif

c     write(6,*)'glt, glb = ',glt,glb
c     write(6,*)'ght, ghb = ',ght,ghb
c     write(6,*)'gat, gab = ',gat,gab

      if(ihiggs.eq.1.or.ihiggs.eq.5)then
c        =========================================================
c                           light cp even higgs decays
c        =========================================================
c     =============  running masses
      rms = runm_hdec(aml,3)
      rmc = runm_hdec(aml,4)
      rmb = runm_hdec(aml,5)
      rmt = runm_hdec(aml,6)
      ratcoup = glt/glb
      higtop = aml**2/amt**2

      ash=alphas_hdec(aml,3)
      amc0=1.d8
      amb0=2.d8
c     amt0=3.d8
      as3=alphas_hdec(aml,3)
      amc0=amc
      as4=alphas_hdec(aml,3)
      amb0=amb
c     amt0=amt

c     =============== partial widths
c  h ---> g g
       eps=1.d-8
       nfext = 3
       asg = as3
       ctt = 4*amt**2/aml**2*dcmplx(1d0,-eps)
       ctb = 4*amb**2/aml**2*dcmplx(1d0,-eps)
       cat = 2*ctt*(1+(1-ctt)*cf(ctt))*glt
       cab = 2*ctb*(1+(1-ctb)*cf(ctb))*glb
       ctc = 4*amc**2/aml**2*dcmplx(1d0,-eps)
       cac = 2*ctc*(1+(1-ctc)*cf(ctc))*glt
c

c this is for a check
       bb = datan(tgbet2hdm)
c       print*,'1,glt/b',dcos(alph2hdm)/dsin(bb),glt,glb

c       print*,'2,glt',dcos(alph2hdm)/dsin(bb),glt
c       print*,'2,glb',-sin(alph2hdm)/dcos(bb),glb

c       print*,'v1',amt,amb,amc,cat,cab,cac
c end check

       if(iofsusy.eq.0) then
       csb1= 4*amsb(1)**2/aml**2*dcmplx(1d0,-eps)
       csb2= 4*amsb(2)**2/aml**2*dcmplx(1d0,-eps)
       cst1= 4*amst(1)**2/aml**2*dcmplx(1d0,-eps)
       cst2= 4*amst(2)**2/aml**2*dcmplx(1d0,-eps)
       cxb1=-amz**2/amsb(1)**2*csb1*(1-csb1*cf(csb1))*glbb(1,1)
       cxb2=-amz**2/amsb(2)**2*csb2*(1-csb2*cf(csb2))*glbb(2,2)
       cxt1=-amz**2/amst(1)**2*cst1*(1-cst1*cf(cst1))*gltt(1,1)
       cxt2=-amz**2/amst(2)**2*cst2*(1-cst2*cf(cst2))*gltt(2,2)

       csul = 4*amsu(1)**2/aml**2*dcmplx(1d0,-eps)
       csur = 4*amsu(2)**2/aml**2*dcmplx(1d0,-eps)
       csdl = 4*amsd(1)**2/aml**2*dcmplx(1d0,-eps)
       csdr = 4*amsd(2)**2/aml**2*dcmplx(1d0,-eps)
       cxul=2*(1.d0/2.d0-2.d0/3.d0*ss)*amz**2/amsu(1)**2*dsin(a+b)
     .      *csul*(1-csul*cf(csul))
       cxur=2*(2.d0/3.d0*ss)*amz**2/amsu(2)**2*dsin(a+b)
     .      *csur*(1-csur*cf(csur))
       cxdl=2*(-1.d0/2.d0+1.d0/3.d0*ss)*amz**2/amsd(1)**2*dsin(a+b)
     .      *csdl*(1-csdl*cf(csdl))
       cxdr=2*(-1.d0/3.d0*ss)*amz**2/amsd(2)**2*dsin(a+b)
     .      *csdr*(1-csdr*cf(csdr))

       else
       cxb1=0.d0
       cxb2=0.d0
       cxt1=0.d0
       cxt2=0.d0

       cxul=0.d0
       cxur=0.d0
       cxdl=0.d0
       cxdr=0.d0
       endif

       fqcd=hggqcd(asg,nfext)
       sqcd=sggqcd(asg)
       xfac = cdabs(cat+cab+cac+cxb1+cxb2+cxt1+cxt2
     .             +cxul+cxur+cxdl+cxdr)**2*fqcd
     .      + dreal(dconjg(cat+cab+cac+cxb1+cxb2+cxt1+cxt2
     .                    +cxul+cxur+cxdl+cxdr)
     .             *(cxb1+cxb2+cxt1+cxt2+cxul+cxur+cxdl+cxdr))*sqcd

c this is for a check
c       print*,'v2',xfac,cdabs(cat+cab+cac)**2*fqcd
c maggie question // mass dependent nlo qcd corrections?
c end check

       hgg=hvv(aml,0.d0)*(asg/pi)**2*xfac/8
c      write(6,*)'gg: ',cat,cab,cac,cxb1+cxb2,cxt1+cxt2,
c    .                  cxul+cxur+cxdl+cxdr

c      write(6,*)'amhl, glb, glt: ',aml,glb,glt

c      print*,''
c      print*,'h decay widths'
c      print*,'hgg_nlo',hgg

c  h ---> g g* ---> g cc   to be added to h ---> cc
       nfext = 4
       asg = as4
       fqcd=hggqcd(asg,nfext)
       sqcd=sggqcd(asg)
       xfac = cdabs(cat+cab+cac+cxb1+cxb2+cxt1+cxt2
     .             +cxul+cxur+cxdl+cxdr)**2*fqcd
     .      + dreal(dconjg(cat+cab+cac+cxb1+cxb2+cxt1+cxt2
     .                    +cxul+cxur+cxdl+cxdr)
     .             *(cxb1+cxb2+cxt1+cxt2+cxul+cxur+cxdl+cxdr))*sqcd
       dcc=hvv(aml,0.d0)*(asg/pi)**2*xfac/8 - hgg

c  h ---> g g* ---> g bb   to be added to h ---> bb
       nfext = 5
       asg = ash
       fqcd=hggqcd(asg,nfext)
       sqcd=sggqcd(asg)
       xfac = cdabs(cat+cab+cac+cxb1+cxb2+cxt1+cxt2
     .             +cxul+cxur+cxdl+cxdr)**2*fqcd
     .      + dreal(dconjg(cat+cab+cac+cxb1+cxb2+cxt1+cxt2
     .                    +cxul+cxur+cxdl+cxdr)
     .             *(cxb1+cxb2+cxt1+cxt2+cxul+cxur+cxdl+cxdr))*sqcd
       dbb=hvv(aml,0.d0)*(asg/pi)**2*xfac/8 - hgg - dcc
       hgg=hvv(aml,0.d0)*(asg/pi)**2*xfac/8

c  h ---> g g: full nnnlo corrections to top loops for nf=5
       fqcd0=hggqcd(asg,5)
       fqcd=hggqcd2(asg,5,aml,amt)
       xfac = cdabs(cat+cab+cac)**2*(fqcd-fqcd0)
       hgg=hgg+hvv(aml,0.d0)*(asg/pi)**2*xfac/8

      if(nfgg.eq.3)then
       hgg = hgg - dbb - dcc
      elseif(nfgg.eq.4)then
       hgg = hgg - dbb
       dcc = 0
      else
       dcc = 0
       dbb = 0
      endif

c alex
c     cat0 = cat/glt
c     cab0 = cab/glb
c     cac0 = cac/glt
c     xfac0 = cdabs(cat0+cab0+cac0)**2*fqcd
c     hgg0=hvv(aml,0.d0)*(asg/pi)**2*xfac0/8
c     write(1,*)'c_gg= ',hgg/hgg0
c end alex

c     print*,'hgg_nnlo',hgg

c  h ---> mu mu
      xglm = glb
      xghm = ghb
      xgam = gab
      if(i2hdm.eq.1) then
         xglm = gllep
      endif
      if(iofsusy.eq.0) then
       call staususy_hdec(glb,ghb,gab,xglm,xghm,xgam,qsusy,0)
      endif
      if(aml.le.2*ammuon) then
       hmm = 0
      else
      hmm=hff(aml,(ammuon/aml)**2)*xglm**2
      endif

c      print*,'h -> mumu',hmm
c  h ---> tau tau
      xglt = glb
      xght = ghb
      xgat = gab
      if(i2hdm.eq.1) then
         xglt = gllep
      endif
      if(iofsusy.eq.0) then
       call staususy_hdec(glb,ghb,gab,xglt,xght,xgat,qsusy,1)
      endif
      if(aml.le.2*amtau) then
       hll = 0
      else
      hll=hff(aml,(amtau/aml)**2)*xglt**2
      endif

c alex
c     write(1,*)'c_tau= ',xglt
c     write(1,*)'c0_tau= ',glt
c     write(1,*)'deltatau= ',(gat/xgat-1)/(1+gat/xgat/tgbet**2)
c end alex

c     write(6,*)'h: tau/mu: ',hll/hmm*ammuon**2/amtau**2,xglt**2/xglm**2
c      print*,'h -> tautau',hll
c  h --> ss

      xgls = glb
      xghs = ghb
      xgas = gab
      ssusy = (amsd(1)+amsd(2)+amglu)/3*qsusy
      if(iofsusy.eq.0) then
       call strsusy_hdec(glb,ghb,gab,xgls,xghs,xgas,ssusy,loop)
      endif
      if(aml.le.2*ams) then
       hss = 0
      else
       hs1=3.d0*hff(aml,(ams/aml)**2)
     .    *xgls**2
     .    *tqcdh(ams**2/aml**2)
       hs2=3.d0*hff(aml,(rms/aml)**2)*xgls**2
     .    *qcdh(rms**2/aml**2)
       if(hs2.lt.0.d0) hs2 = 0
       rat = 2*ams/aml
       hss = qqint_hdec(rat,hs1,hs2)
      endif
c alex
c     write(1,*)'c_s= ',xgls
c     write(1,*)'c0_s= ',glb
c end alex

c      print*,'h -> ss',hss
c  h --> cc

      ratcoup = 1
      if(aml.le.2*amc) then
       hcc = 0
      else
       hc1=3.d0*hff(aml,(amc/aml)**2)
     .    *glt**2
     .    *tqcdh(amc**2/aml**2)
       hc2=3.d0*hff(aml,(rmc/aml)**2)*glt**2
     .    *qcdh(rmc**2/aml**2)
     .   + dcc
       if(hc2.lt.0.d0) hc2 = 0
       rat = 2*amc/aml
       hcc = qqint_hdec(rat,hc1,hc2)
      endif

c      print*,'h -> cc',hcc
c  h --> bb :

      xglb = glb
      xghb = ghb
      xgab = gab
      qq = amb
      susy = 0
      xglb = glb
      ssusy = (amsb(1)+amsb(2)+amglu)/3*qsusy
      fsusy = susyscale
      as0 = alphas_hdec(fsusy,3)
      if(iofsusy.eq.0) then
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       i0 = 0
       call dmbapp_hdec(i0,dglb,dghb,dgab,fsusy,loop)
       delb1 = -dgab/(1+1/tgbet**2)
       delb0 = delb1/(1-delb1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       i0 = 1
       call dmbapp_hdec(i0,dglb,dghb,dgab,fsusy,loop)
       i0 = 1
       bsc = (amsq+amur+amdr)/3
       delb2 = -dgab/(1+1/tgbet**2)
c      write(6,*)'delta_b = ',delb0
       xmb = runm_hdec(fsusy,5)/(1+delb0)
c      xmb = amb
c1357
       if(islhai.ne.0) xmb = amb
       susy = cofsusy_hdec(i0,amb,xmb,qq)*as0/pi - 2*dglb
c      susy = cofsusy_hdec(i0,xmb,xmb,qq)*as0/pi - 2*dglb
       call botsusy_hdec(glb,ghb,gab,xglb,xghb,xgab,ssusy,loop)
      endif
      ratcoup = glt/xglb
      if(aml.le.2*amb) then
       hbb = 0
      else
       hb1=3.d0*hff(aml,(amb/aml)**2)
     .    *(xglb**2+xglb*glb*susy)
     .    *tqcdh(amb**2/aml**2)
       hb2=3.d0*hff(aml,(rmb/aml)**2)
     .    *(xglb**2+xglb*glb*susy)
     .    *qcdh(rmb**2/aml**2)
     .   + dbb
       if(hb2.lt.0.d0) hb2 = 0
       rat = 2*amb/aml
       hbb = qqint_hdec(rat,hb1,hb2)

c alex
c      xb1=3.d0*hff(aml,(amb/aml)**2)
c    .    *tqcdh(amb**2/aml**2)
c      xb2=3.d0*hff(aml,(rmb/aml)**2)
c    .    *qcdh(rmb**2/aml**2)
c    .   + dbb
c      if(xb2.lt.0.d0) hb2 = 0
c      rat = 2*amb/aml
c      xbb = qqint_hdec(rat,xb1,xb2)
c      write(6,*)'h -> bb = ',aml,hbb,xbb
c      write(6,*)'rem/tot = ',xglb*glb*susy/xglb**2,
c    .                        xglb**2+xglb*glb*susy,xglb**2
c      write(1,*)'c_b= ',xglb
c      write(1,*)'c0_b= ',glb
c      write(1,*)'deltab= ',delb0
c      write(1,*)'deltab= ',delb2
c      tana = dtan(a)
c      tanb = tgbet
c      delb1 = delb0/(1-delb0)
c      delb1 = delb0
c      test = -delb1/(1+delb1)*(1+1/tana/tanb)
c      yglb = glb*(1+test)
c      write(1,*)'deltab= ',delb1
c      write(1,*)'tgbet = ',tgbet
c      write(1,*)'tgalp = ',tana
c      write(1,*)'c_b= ',yglb
c      write(1,*)'a_b= ',ad
c end alex

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      xxx = cofsusy_hdec(i0,amb,xmb,qq)
c      write(1,*)'h -> bb: ',xxx
c      write(1,*)'h -> bb: ',xxx*as0/pi
c      write(1,*)'h -> bb: ',2*dglb
c      write(1,*)'h -> bb: ',glb,xglb,susy
c      write(1,*)'h -> bb: ',hbb

c alex
c      write(1,*)'c_b= ',xglb
c      write(1,*)'c0_b= ',glb
c      write(1,*)'deltab= ',delb0,-dgab,tgbet
c      write(1,*)-dsin(a),dcos(b),-dsin(a)/dcos(b)
c      write(6,*)'h -> bb: ',xglb**2,xglb*glb*susy/xglb**2,
c    .                       (xglb**2+xglb*glb*susy)/xglb**2
c      write(6,*)'approx:  ',susy+2*dglb,2*dglb,susy
c      fac = as0/pi
c      write(6,*)'approx2: ',(susy+2*dglb)/fac,2*dglb/fac,susy/fac
c end alex

c      print*,'h -> bb',hbb,rmb
c      print*
c      print*,'h -> bb:',hbb,glb,xglb,glb*susy,2*dglb,susy,2*dglb+susy

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c--hmssm?
c      sb0 = dsin(b)
c      cb0 = dcos(b)
c      c2b0 = cb0**2-sb0**2
c      amh0 =dsqrt(((ama**2+amz**2-aml**2)*(amz**2*cb0**2+ama**2*sb0**2)
c    .     -ama**2*amz**2*c2b0**2)/(amz**2*cb0**2+ama**2*sb0**2-aml**2))
c      amch0 = dsqrt(ama**2+amw**2)
c      a0 = -datan((amz**2+ama**2)*sb0*cb0
c    .            /(amz**2*cb0**2+ama**2*sb0**2-aml**2))
c      sa0 = dsin(a0)
c      ca0 = dcos(a0)
c      s2a0 = 2*sa0*ca0
c      c2a0 = ca0**2-sa0**2
c      sbpa0 = sb0*ca0+cb0*sa0
c      cbpa0 = cb0*ca0-sb0*sa0
c      deps = (aml**2*(ama**2+amz**2-aml**2)-ama**2*amz**2*c2b0**2)
c    .      / (amz**2*cb0**2+ama**2*sb0**2-aml**2)
c      ghhh0 = 3*c2a0*cbpa0              + 3*deps/amz**2*sa0**3/sb0
c      glll0 = 3*c2a0*sbpa0              + 3*deps/amz**2*ca0**3/sb0
c      ghll0 = 2*s2a0*sbpa0 - c2a0*cbpa0 + 3*deps/amz**2*sa0*ca0**2/sb0
c      glhh0 =-2*s2a0*cbpa0 - c2a0*sbpa0 + 3*deps/amz**2*sa0**2*ca0/sb0
c      ghaa0 =-c2b0*cbpa0                 + deps/amz**2*sa0*cb0**2/sb0
c      glaa0 = c2b0*sbpa0                 + deps/amz**2*ca0*cb0**2/sb0
c      write(6,*)'0: ',aml,amh,ama
c      write(6,*)'1: ',aml,amh0,ama
c      write(6,*)'0: ',amch,b,a
c      write(6,*)'1: ',amch0,b,a0
c      write(6,*)'   ',glb,xglb
c      write(6,*)'   ',ghb,xghb
c      write(6,*)'   ',gab,xgab
c      write(6,*)
c      write(6,*)
c      write(6,*)'hhh: ',glll,ghll,glhh,ghhh
c      write(6,*)'     ',glll0,ghll0,glhh0,ghhh0
c      write(6,*)
c      write(6,*)'haa: ',glaa,ghaa
c      write(6,*)'     ',glaa0,ghaa0
c      write(6,*)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,('a3,4(1x,g15.8)'))'h: ',ama,aml,susy+2*dglb,
c    .                             susy/(susy+2*dglb)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      hb2=3.d0*hff(aml,(rmb/aml)**2)
c    .    *qcdh(rmb**2/aml**2)
c    .    *(1+elw0(aml,rmb,-1.d0/3.d0,1.d0))
c    .    *hffself(aml)
c    .    + dbb
c      if(hb2.lt.0.d0) hb2 = 0
c      hb1=3.d0*hff(aml,(amb/aml)**2)
c    .    *tqcdh(amb**2/aml**2)
c    .    *hffself(aml)
c      rat = 2*amb/aml
c      hbb0 = qqint_hdec(rat,hb1,hb2)
c      write(6,*)aml,xglb,glb,xglb/glb-1,susy,hbb/hbb0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)aml,hb1,hb2,hbb,glb,xglb,susy+2*dglb,2*dglb
c    .          ,xglb**2+xglb*glb*susy
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      xb0=3.d0*hff(aml,(amb/aml)**2)
c    .    *glb**2
c      xb1=3.d0*hff(aml,(rmb/aml)**2)
c    .    *glb**2
c    .    *qcdh(rmb**2/aml**2)
c    .   + dbb
c      xb2=3.d0*hff(aml,(rmb/aml)**2)
c    .    *(xglb**2+xglb*glb*susy)
c    .    *qcdh(rmb**2/aml**2)
c    .   + dbb
c      write(51,('5(1x,g15.8)'))ama,aml,xb0,xb1,xb2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(51,('4(1x,g15.8)'))ama,aml,susy+2*dglb,susy/(susy+2*dglb)
c      write(51,('4(1x,g15.8)'))ama,aml,hbb,2*dglb,xglb,susy-1+2*dlgb,
c    .                          dsin(a),dcos(a)
c      write(51,*)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c      x1 = (qcdh(rmb**2/aml**2)*hff(aml,(rmb/aml)**2)/
c    .       hff(aml,(amb/aml)**2)-1)
c      x2 = (susy-1)

c     ratcoup = glt/xglb
c      hb1x=3.d0*hff(aml,(amb/aml)**2)
c    .    *xglb**2
c    .    *tqcdh(amb**2/aml**2)
c    .    /(beta_hdec(amb**2/aml**2))**3
c    .    *susy
c      hb2x=3.d0*hff(aml,(rmb/aml)**2)*xglb**2
c    .    *qcdh(rmb**2/aml**2)
c    .    /(beta_hdec(rmb**2/aml**2))**3
c    .    *susy
c      hb1x=3.d0*hff(aml,(rmb/aml)**2)*glb**2
c    .    *qcdh(rmb**2/aml**2)
c    .    /(beta_hdec(rmb**2/aml**2))**3
c    .    *(susy+2*dglb)

c     ratcoup = 0
c     deltaqcd = qcdh(rmb**2/aml**2)
c     ratcoup = glt/xglb
c     deltat = qcdh(rmb**2/aml**2) - deltaqcd

c      write(6,*)
c      write(6,*)'h:'
c      write(6,*)'mb,runmb,alpha_s: ',amb,rmb,ash
c      write(6,*)'mh =              ',aml
c      write(6,*)'ma =              ',ama
c      write(6,*)'delta(mb) = ',-dgab
c      write(6,*)'qcd           susy          approx',
c    .           '        approx/full  gbh(qcd)    gbh(sqcd):'
c      write(6,*)x1,x2+2*dglb,2*dglb,2*dglb/(x2+2*dglb),glb,xglb
c      write(6,*)'resummation: ',(xglb/glb)**2-1
c      write(6,*)'rest:        ',susy-1
c      write(6,*)'rest:        ',susy-1,dtan(a),tgbet
c      write(6,*)amsq,amur,amdr,(susy-1)/(x2+2*dglb)
c      write(6,*)'total susy:  ',(xglb/glb)**2*susy-1
c      write(6,*)'deltaqcd,t = ',deltaqcd,deltat
c      write(6,*)'gamma(0)   = ',ama,hb2x,hb1x
c      write(6,*)'gamma(mb)  = ',hb2,hb1
c      write(6,*)
c      write(9,*)ama,aml,hb2x,hb2x/susy,glb,xglb
c      write(6,*)'rest: h      ',ama,aml,(susy-1)/(x2+2*dglb)
c      write(51,*)ama,aml,(susy-1)/(x2+2*dglb)
      endif
c  h ---> tt
      ratcoup = 0
      call topsusy_hdec(glt,ght,gat,xgltop,xghtop,xgatop,scale,1)
c     write(6,*)xgltop,xghtop,xgatop
      if (aml.le.2*amt) then
       htt=0.d0
      else
       ht1=3.d0*hff(aml,(amt/aml)**2)*glt**2
     .    *tqcdh(amt**2/aml**2)
       ht2=3.d0*hff(aml,(rmt/aml)**2)*glt**2
     .    *qcdh(rmt**2/aml**2)
       if(ht2.lt.0.d0) ht2 = 0
       rat = 2*amt/aml
       htt = qqint_hdec(rat,ht1,ht2)
      endif

c mmm changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0)then
            dld=5.d0
            dlu=3.d0
            xm1 = 2d0*amt-dld
            xm2 = 2d0*amt+dlu
            if (aml.le.amt+amw+amb) then
               htt=0.d0
            elseif (aml.le.xm1) then
               factt=6.d0*gf**2*aml**3*amt**2/2.d0/128.d0/pi**3
               call htott_hdec(aml,amt,amb,amw,amch,glt,glb,gat,gab,
     .              glvv,gzah,htt0)
               htt=factt*htt0
            elseif (aml.le.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0

               factt=6.d0*gf**2*xx(1)**3*amt**2/2.d0/128.d0/pi**3
               call htott_hdec(xx(1),amt,amb,amw,amch,glt,glb,gat,gab,
     .              glvv,gzah,htt0)
               yy(1)=factt*htt0

               factt=6.d0*gf**2*xx(2)**3*amt**2/2.d0/128.d0/pi**3
               call htott_hdec(xx(2),amt,amb,amw,amch,glt,glb,gat,gab,
     .              glvv,gzah,htt0)
               yy(2)=factt*htt0

               xmt = runm_hdec(xx(3),6)
               ht1=3.d0*hff(xx(3),(amt/xx(3))**2)*glt**2
     .              *tqcdh(amt**2/xx(3)**2)
               ht2=3.d0*hff(xx(3),(xmt/xx(3))**2)*glt**2
     .              *qcdh(xmt**2/xx(3)**2)
               if(ht2.lt.0.d0) ht2 = 0
               rat = 2*amt/xx(3)
               yy(3) = qqint_hdec(rat,ht1,ht2)

               xmt = runm_hdec(xx(4),6)
               ht1=3.d0*hff(xx(4),(amt/xx(4))**2)*glt**2
     .              *tqcdh(amt**2/xx(4)**2)
               ht2=3.d0*hff(xx(4),(xmt/xx(4))**2)*glt**2
     .              *qcdh(xmt**2/xx(4)**2)
               if(ht2.lt.0.d0) ht2 = 0
               rat = 2*amt/xx(4)
               yy(4) = qqint_hdec(rat,ht1,ht2)

               htt=fint_hdec(aml,xx,yy)
            else
               ht1=3.d0*hff(aml,(amt/aml)**2)*glt**2
     .              *tqcdh(amt**2/aml**2)
               ht2=3.d0*hff(aml,(rmt/aml)**2)*glt**2
     .              *qcdh(rmt**2/aml**2)
               if(ht2.lt.0.d0) ht2 = 0
               rat = 2.d0*amt/aml
               htt = qqint_hdec(rat,ht1,ht2)
            endif
         else
            if (aml.le.2.d0*amt) then
               htt=0.d0
            else
               ht1=3.d0*hff(aml,(amt/aml)**2)*glt**2
     .              *tqcdh(amt**2/aml**2)
               ht2=3.d0*hff(aml,(rmt/aml)**2)*glt**2
     .              *qcdh(rmt**2/aml**2)
               if(ht2.lt.0.d0) ht2 = 0
               rat = 2.d0*amt/aml
               htt = qqint_hdec(rat,ht1,ht2)
            endif
         endif
      endif
c end mmm changed 22/8/2013

c alex
c     write(1,*)'c_t= ',xgltop
c     write(1,*)'c0_t= ',glt
c end alex

c      print*,'h -> tt',htt
c  h ---> gamma gamma
       eps=1.d-8
       xrmc = runm_hdec(aml/2,4)*amc/runm_hdec(amc,4)
       xrmb = runm_hdec(aml/2,5)*amb/runm_hdec(amb,5)
       xrmt = runm_hdec(aml/2,6)*amt/runm_hdec(amt,6)

       ctt = 4*xrmt**2/aml**2*dcmplx(1d0,-eps)
       ctb = 4*xrmb**2/aml**2*dcmplx(1d0,-eps)
       ctc = 4*xrmc**2/aml**2*dcmplx(1d0,-eps)
       ctl = 4*amtau**2/aml**2*dcmplx(1d0,-eps)
       ctw = 4*amw**2/aml**2*dcmplx(1d0,-eps)
       cth = 4*amch**2/aml**2*dcmplx(1d0,-eps)
       cat = 4/3d0 * 2*ctt*(1+(1-ctt)*cf(ctt))*glt
     .     * cfacq_hdec(0,aml,xrmt)

c       print*,'cat_lo',4/3d0 * 2*ctt*(1+(1-ctt)*cf(ctt))*glt
c       print*,'cat_nlo',cat
c       print*,'h-top-top coupling',glt
c       print*,'4*rmt**2/aml**2',ctt
c       print*,'rmt',rmt
c       print*,'aml',aml

       cab = 1/3d0 * 2*ctb*(1+(1-ctb)*cf(ctb))*glb
     .     * cfacq_hdec(0,aml,xrmb)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      call botsusy_hdec(glb,ghb,gab,xglb,xghb,xgab,ssusy,loop)
c      cab = 1/3d0 * 2*ctb*(1+(1-ctb)*cf(ctb))*xglb
c    .     * cfacq_hdec(0,aml,xrmb)
c      write(6,*)ctb,xglb,cfacq_hdec(0,aml,xrmb)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       cac = 4/3d0 * 2*ctc*(1+(1-ctc)*cf(ctc))*glt
     .     * cfacq_hdec(0,aml,xrmc)
       cal = 1.d0  * 2*ctl*(1+(1-ctl)*cf(ctl))*glb
       if(i2hdm.eq.1) then
          cal = 1.d0  * 2*ctl*(1+(1-ctl)*cf(ctl))*gllep
       endif
       caw = -(2+3*ctw+3*ctw*(2-ctw)*cf(ctw))*glvv
       cah = -amz**2/2/amch**2*cth*(1-cth*cf(cth))*glpm

c      write(6,*)'gamma gamma: ',aml,amch,-3*cth*(1-cth*cf(cth))

c       print*,'amz,amch,ghlh+h-',amz,amch,glpm,cth
c       print*,'fac1',-amz**2/2/amch**2*glpm
c       print*,'fac2',cth*(1-cth*cf(cth))
c       write(6,*)'cah,amz,amch,cth,cf(cth),glpm: ',
c    .             cah,amz,amch,cth,cf(cth),glpm
       if(iofsusy.eq.0) then
        rmsu1 = runms_hdec(aml/2,amsu(1))
        rmsu2 = runms_hdec(aml/2,amsu(2))
        rmsd1 = runms_hdec(aml/2,amsd(1))
        rmsd2 = runms_hdec(aml/2,amsd(2))
        rmsb1 = runms_hdec(aml/2,amsb(1))
        rmsb2 = runms_hdec(aml/2,amsb(2))
        rmst1 = runms_hdec(aml/2,amst(1))
        rmst2 = runms_hdec(aml/2,amst(2))
        cx1 = 4*amchar(1)**2/aml**2*dcmplx(1d0,-eps)
        cx2 = 4*amchar(2)**2/aml**2*dcmplx(1d0,-eps)
        csb1= 4*rmsb1**2/aml**2*dcmplx(1d0,-eps)
        csb2= 4*rmsb2**2/aml**2*dcmplx(1d0,-eps)
        cst1= 4*rmst1**2/aml**2*dcmplx(1d0,-eps)
        cst2= 4*rmst2**2/aml**2*dcmplx(1d0,-eps)
        csl1= 4*amsl(1)**2/aml**2*dcmplx(1d0,-eps)
        csl2= 4*amsl(2)**2/aml**2*dcmplx(1d0,-eps)
        cax1= amw/xmchar(1) * 2*cx1*(1+(1-cx1)*cf(cx1))*2*ac2(1,1)
        cax2= amw/xmchar(2) * 2*cx2*(1+(1-cx2)*cf(cx2))*2*ac2(2,2)

        csel = 4*amse(1)**2/aml**2*dcmplx(1d0,-eps)
        cser = 4*amse(2)**2/aml**2*dcmplx(1d0,-eps)
        csul = 4*rmsu1**2/aml**2*dcmplx(1d0,-eps)
        csur = 4*rmsu2**2/aml**2*dcmplx(1d0,-eps)
        csdl = 4*rmsd1**2/aml**2*dcmplx(1d0,-eps)
        csdr = 4*rmsd2**2/aml**2*dcmplx(1d0,-eps)
        cxel=2*(-1/2d0+ss)*amz**2/amse(1)**2*dsin(a+b)
     .       *csel*(1-csel*cf(csel))
        cxer=-2*(ss)*amz**2/amse(2)**2*dsin(a+b)
     .       *cser*(1-cser*cf(cser))
        cxul=2*4.d0/3.d0*(1.d0/2.d0-2.d0/3.d0*ss)
     .       *amz**2/amsu(1)**2*dsin(a+b)*csul*(1-csul*cf(csul))
     .      * cfacsq_hdec(aml,rmsu1)
        cxur=2*4.d0/3.d0*(2.d0/3.d0*ss)
     .       *amz**2/amsu(2)**2*dsin(a+b)*csur*(1-csur*cf(csur))
     .      * cfacsq_hdec(aml,rmsu2)
        cxdl=2/3.d0*(-1.d0/2.d0+1.d0/3.d0*ss)
     .       *amz**2/amsd(1)**2*dsin(a+b)*csdl*(1-csdl*cf(csdl))
     .      * cfacsq_hdec(aml,rmsd1)
        cxdr=2/3.d0*(-1.d0/3.d0*ss)
     .       *amz**2/amsd(2)**2*dsin(a+b)*csdr*(1-csdr*cf(csdr))
     .      * cfacsq_hdec(aml,rmsd2)

        cxb1=-1/3d0*amz**2/amsb(1)**2*csb1*(1-csb1*cf(csb1))*glbb(1,1)
     .      * cfacsq_hdec(aml,rmsb1)
        cxb2=-1/3d0*amz**2/amsb(2)**2*csb2*(1-csb2*cf(csb2))*glbb(2,2)
     .      * cfacsq_hdec(aml,rmsb2)
        cxt1=-4/3d0*amz**2/amst(1)**2*cst1*(1-cst1*cf(cst1))*gltt(1,1)
     .      * cfacsq_hdec(aml,rmst1)
        cxt2=-4/3d0*amz**2/amst(2)**2*cst2*(1-cst2*cf(cst2))*gltt(2,2)
     .      * cfacsq_hdec(aml,rmst2)
        csl1= 4*amsl(1)**2/aml**2*dcmplx(1d0,-eps)
        csl2= 4*amsl(2)**2/aml**2*dcmplx(1d0,-eps)
        cxl1=      -amz**2/amsl(1)**2*csl1*(1-csl1*cf(csl1))*glee(1,1)
        cxl2=      -amz**2/amsl(2)**2*csl2*(1-csl2*cf(csl2))*glee(2,2)
        xfac = cdabs(cat+cab+cac+cal+caw+cah+cax1+cax2
     .      +  cxel+cxer+cxul+cxur+cxdl+cxdr
     .      +  cxb1+cxb2+cxt1+cxt2+cxl1+cxl2)**2
c      write(6,*)'hga: ',cah,glpm
       else
        xfac = cdabs(cat+cab+cac+cal+caw+cah)**2
       endif
       hga=hvv(aml,0.d0)*(alph/pi)**2/16.d0*xfac
c      write(6,*)'h -> ga ga: ',xfac,cat,cab,cac,cal,caw,cah
c      write(6,*)'h -> ga ga: ',glpm

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       cat0 = cat/glt
       cab0 = cab/glb
       cac0 = cac/glt
       cal0 = cal/glb
       caw0 = caw/glvv
       xfac0 = cdabs(cat0+cab0+cac0+cal0+caw0)**2
       xclgaga = xfac/xfac0
c      write(6,*)'hgaga: ',aml,xclgaga
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c       print*,'h -> gamgam',hga,aml,amch
c       print*,'cat,cab',cat,cab
c       print*,'cac,cal',cac,cal
c       print*,'caw,cah',caw,cah
c       print*,'charged higgs loop',cah
c       print*,'charged higgs loop',cth,glpm,cf(cth)
c      cah = -amz**2/2/amch**2*cth*(1-cth*cf(cth))*glpm
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       xfacq = cdabs(cat+cab+cac+cal+caw+cah)**2
       xfacs = cdabs(cat+cab+cac+cal+caw+cah+cax1+cax2
     .      +  cxl1+cxl2)**2
       xfacsq = cdabs(cat+cab+cac+cal+caw+cah+cax1+cax2
     .      +  cxb1+cxb2+cxt1+cxt2+cxl1+cxl2)**2
       hga0 = hga*xfacsq/xfac
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      ctt = 4*amt**2/aml**2*dcmplx(1d0,-eps)
c      ctb = 4*amb**2/aml**2*dcmplx(1d0,-eps)
c      ctc = 4*amc**2/aml**2*dcmplx(1d0,-eps)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       cac0 = 4/3d0 * 2*ctc*(1+(1-ctc)*cf(ctc))*glt
       cat0 = 4/3d0 * 2*ctt*(1+(1-ctt)*cf(ctt))*glt
       cab0 = 1/3d0 * 2*ctb*(1+(1-ctb)*cf(ctb))*glb
       cxb10= -1/3d0*amz**2/amsb(1)**2*csb1*(1-csb1*cf(csb1))*glbb(1,1)
       cxb20= -1/3d0*amz**2/amsb(2)**2*csb2*(1-csb2*cf(csb2))*glbb(2,2)
       cxt10= -4/3d0*amz**2/amst(1)**2*cst1*(1-cst1*cf(cst1))*gltt(1,1)
       cxt20= -4/3d0*amz**2/amst(2)**2*cst2*(1-cst2*cf(cst2))*gltt(2,2)
       xfacloq = cdabs(cat0+cab0+cac0+cal+caw+cah)**2
       cxul0=2*4.d0/3.d0*(1.d0/2.d0-2.d0/3.d0*ss)
     .      *amz**2/amsu(1)**2*dsin(a+b)*csul*(1-csul*cf(csul))
       cxur0=2*4.d0/3.d0*(2.d0/3.d0*ss)
     .      *amz**2/amsu(2)**2*dsin(a+b)*csur*(1-csur*cf(csur))
       cxdl0=2/3.d0*(-1.d0/2.d0+1.d0/3.d0*ss)
     .      *amz**2/amsd(1)**2*dsin(a+b)*csdl*(1-csdl*cf(csdl))
       cxdr0=2/3.d0*(-1.d0/3.d0*ss)
     .      *amz**2/amsd(2)**2*dsin(a+b)*csdr*(1-csdr*cf(csdr))
       xfaclo = cdabs(cat0+cab0+cac0+cal+caw+cah+cax1+cax2
     .      +  cxel+cxer+cxul0+cxur0+cxdl0+cxdr0
     .      +  cxb10+cxb20+cxt10+cxt20+cxl1+cxl2)**2
       csq = 1+3*alphas_hdec(aml,3)
       xfacsql = cdabs(cat+cab+cac+cal+caw+cah+cax1+cax2
     .      +  cxel+cxer+(cxul0+cxur0+cxdl0+cxdr0
     .      +  cxb10+cxb20+cxt10+cxt20)*csq+cxl1+cxl2)**2
      xfacsm = cdabs(cat/glt+cab/glb+cac/glt+cal/glb+caw/glvv)**2
      xfac0  = cdabs(cat+cab+cac+cal+caw+cxl1)**2

c alex
c     write(1,*)'c_gaga= ',xclgaga
c     write(1,*)'h -> gaga: ',xfac,cdabs(cat+cab+cac+cal+caw)**2
c    .                       ,xfac/cdabs(cat+cab+cac+cal+caw)**2
c     write(1,*)'h_sm -> gaga: ',xfac0
c     write(1,*)'h -> gaga:    ',cdabs(cat+cab+cac+cal+caw+cah+cax1+cax2
c    .      +  cxel+cxer+cxul+cxur+cxdl+cxdr
c    .      +  cxb1+cxb2+cxt1+cxt2+cxl1+cxl2)**2
c     write(1,*)'h -> gaga:    ',cdabs(cat+cab+cac+cal+caw)**2
c     write(1,*)'h -> gaga:    ',cdabs(cat+cab+cac+cal+caw+cah+cax1+cax2
c    .      +  0*(cxel+cxer+cxul+cxur+cxdl+cxdr
c    .      +  cxb1+cxb2+cxt1+cxt2+cxl1+cxl2))**2
c     write(1,*)'h -> gaga:    ',cdabs(cat+cab+cac+cal+caw+0*cah
c    .      +  0*cax1+0*cax2
c    .      +  1*cxel+0*cxer+0*cxul+0*cxur+0*cxdl+0*cxdr
c    .      +  0*cxb1+0*cxb2+0*cxt1+0*cxt2+0*cxl1+0*cxl2)**2
c     write(1,*)'h_sm -> gaga: ',xfac0
c     write(1,*)
c     write(1,*)'h -> gaga:    ',cdabs(cat+cab+cac+cal+caw+cah+cax1+cax2
c    .      +  cxel+cxer+cxul+cxur+cxdl+cxdr
c    .      +  cxb1+cxb2+cxt1+cxt2+cxl1+cxl2)**2
c end alex

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c  h ---> z gamma
      xrmc = runm_hdec(aml/2,4)*amc/runm_hdec(amc,4)
      xrmb = runm_hdec(aml/2,5)*amb/runm_hdec(amb,5)
      xrmt = runm_hdec(aml/2,6)*amt/runm_hdec(amt,6)
c     print*,'xrmc,xrmb,xrmt ',xrmc,xrmb,xrmt
      if(aml.le.amz)then
       hzga=0
      else
       ts = ss/cs
       ft = -3*2d0/3*(1-4*2d0/3*ss)/dsqrt(ss*cs)*glt
       fb = 3*1d0/3*(-1+4*1d0/3*ss)/dsqrt(ss*cs)*glb
       fc = -3*2d0/3*(1-4*2d0/3*ss)/dsqrt(ss*cs)*glt
       fl = (-1+4*ss)/dsqrt(ss*cs)*glb
       if(i2hdm.eq.1) then
          fl = (-1+4*ss)/dsqrt(ss*cs)*gllep
       endif
       eps=1.d-8
c      ctt = 4*xrmt**2/aml**2*dcmplx(1d0,-eps)
c      ctb = 4*xrmb**2/aml**2*dcmplx(1d0,-eps)
c      ctc = 4*xrmc**2/aml**2*dcmplx(1d0,-eps)
       ctt = 4*amt**2/aml**2*dcmplx(1d0,-eps)
       ctb = 4*amb**2/aml**2*dcmplx(1d0,-eps)
       ctc = 4*amc**2/aml**2*dcmplx(1d0,-eps)
       ctl = 4*amtau**2/aml**2*dcmplx(1d0,-eps)
       ctw = 4*amw**2/aml**2*dcmplx(1d0,-eps)
       cth = 4*amch**2/aml**2*dcmplx(1d0,-eps)
c      clt = 4*xrmt**2/amz**2*dcmplx(1d0,-eps)
c      clb = 4*xrmb**2/amz**2*dcmplx(1d0,-eps)
c      clc = 4*xrmc**2/amz**2*dcmplx(1d0,-eps)
       clt = 4*amt**2/amz**2*dcmplx(1d0,-eps)
       clb = 4*amb**2/amz**2*dcmplx(1d0,-eps)
       clc = 4*amc**2/amz**2*dcmplx(1d0,-eps)
       cle = 4*amtau**2/amz**2*dcmplx(1d0,-eps)
       clw = 4*amw**2/amz**2*dcmplx(1d0,-eps)
       clh = 4*amch**2/amz**2*dcmplx(1d0,-eps)
       cat = ft*(ci1(ctt,clt) - ci2(ctt,clt))
       cab = fb*(ci1(ctb,clb) - ci2(ctb,clb))
       cac = fc*(ci1(ctc,clc) - ci2(ctc,clc))
       cal = fl*(ci1(ctl,cle) - ci2(ctl,cle))
       caw = -1/dsqrt(ts)*(4*(3-ts)*ci2(ctw,clw)
     .     + ((1+2/ctw)*ts - (5+2/ctw))*ci1(ctw,clw))*glvv
       cah = (1-2*ss)/dsqrt(ss*cs)*amz**2/2/amch**2*ci1(cth,clh)*glpm
       xfac = cdabs(cat+cab+cac+cal+caw+cah)**2
       acoup = dsqrt(2d0)*gf*amz**2*ss*cs/pi**2
       hzga = gf/(4.d0*pi*dsqrt(2.d0))*aml**3*(alph/pi)*acoup/16.d0
     .        *xfac*(1-amz**2/aml**2)**3
c      write(6,*)'z gamma:     ',aml,amch,6*ci1(cth,clh)

      endif

c      print*,'h -> zgam',hzga
c  h ---> w w
      if(ionwz.eq.0)then
       call htovv_hdec(0,aml,amw,gamw,htww)
       hww = 3d0/2d0*gf*amw**4/dsqrt(2d0)/pi/aml**3*htww*glvv**2
      elseif(ionwz.eq.-1)then
       dld=2d0
       dlu=2d0
       xm1 = 2d0*amw-dld
       xm2 = 2d0*amw+dlu
       if (aml.le.xm1) then
        call htovv_hdec(0,aml,amw,gamw,htww)
        hww = 3d0/2d0*gf*amw**4/dsqrt(2d0)/pi/aml**3*htww*glvv**2
       elseif (aml.le.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        call htovv_hdec(0,xx(1),amw,gamw,htww)
        yy(1)=3d0/2d0*gf*amw**4/dsqrt(2d0)/pi/xx(1)**3*htww
        call htovv_hdec(0,xx(2),amw,gamw,htww)
        yy(2)=3d0/2d0*gf*amw**4/dsqrt(2d0)/pi/xx(2)**3*htww
        yy(3)=hvv(xx(3),amw**2/xx(3)**2)
        yy(4)=hvv(xx(4),amw**2/xx(4)**2)
        hww = fint_hdec(aml,xx,yy)*glvv**2
       else
        hww=hvv(aml,amw**2/aml**2)*glvv**2
       endif
      else
      dld=2d0
      dlu=2d0
      xm1 = 2d0*amw-dld
      xm2 = 2d0*amw+dlu
      if (aml.le.amw) then
       hww=0
      else if (aml.le.xm1) then
       cww=3.d0*gf**2*amw**4/16.d0/pi**3
       hww=hv(amw**2/aml**2)*cww*aml*glvv**2
      else if (aml.lt.xm2) then
       cww=3.d0*gf**2*amw**4/16.d0/pi**3
       xx(1) = xm1-1d0
       xx(2) = xm1
       xx(3) = xm2
       xx(4) = xm2+1d0
       yy(1)=hv(amw**2/xx(1)**2)*cww*xx(1)
       yy(2)=hv(amw**2/xx(2)**2)*cww*xx(2)
       yy(3)=hvv(xx(3),amw**2/xx(3)**2)
       yy(4)=hvv(xx(4),amw**2/xx(4)**2)
       hww = fint_hdec(aml,xx,yy)*glvv**2
      else
       hww=hvv(aml,amw**2/aml**2)*glvv**2
      endif
      endif

c      print*,'h -> ww',hww
c  h ---> z z
      if(ionwz.eq.0)then
       call htovv_hdec(0,aml,amz,gamz,htzz)
       hzz = 3d0/4d0*gf*amz**4/dsqrt(2d0)/pi/aml**3*htzz*glvv**2
      elseif(ionwz.eq.-1)then
       dld=2d0
       dlu=2d0
       xm1 = 2d0*amz-dld
       xm2 = 2d0*amz+dlu
       if (aml.le.xm1) then
        call htovv_hdec(0,aml,amz,gamz,htzz)
        hzz = 3d0/4d0*gf*amz**4/dsqrt(2d0)/pi/aml**3*htzz*glvv**2
       elseif (aml.le.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        call htovv_hdec(0,xx(1),amz,gamz,htzz)
        yy(1)=3d0/4d0*gf*amz**4/dsqrt(2d0)/pi/xx(1)**3*htzz
        call htovv_hdec(0,xx(2),amz,gamz,htzz)
        yy(2)=3d0/4d0*gf*amz**4/dsqrt(2d0)/pi/xx(2)**3*htzz
        yy(3)=hvv(xx(3),amz**2/xx(3)**2)/2
        yy(4)=hvv(xx(4),amz**2/xx(4)**2)/2
        hzz = fint_hdec(aml,xx,yy)*glvv**2
       else
        hzz=hvv(aml,amz**2/aml**2)/2.d0*glvv**2
       endif
      else
      dld=2d0
      dlu=2d0
      xm1 = 2d0*amz-dld
      xm2 = 2d0*amz+dlu
      if (aml.le.amz) then
       hzz=0
      else if (aml.le.xm1) then
       czz=3.d0*gf**2*amz**4/192.d0/pi**3*(7-40/3.d0*ss+160/9.d0*ss**2)
       hzz=hv(amz**2/aml**2)*czz*aml*glvv**2
      else if (aml.lt.xm2) then
       czz=3.d0*gf**2*amz**4/192.d0/pi**3*(7-40/3.d0*ss+160/9.d0*ss**2)
       xx(1) = xm1-1d0
       xx(2) = xm1
       xx(3) = xm2
       xx(4) = xm2+1d0
       yy(1)=hv(amz**2/xx(1)**2)*czz*xx(1)
       yy(2)=hv(amz**2/xx(2)**2)*czz*xx(2)
       yy(3)=hvv(xx(3),amz**2/xx(3)**2)/2d0
       yy(4)=hvv(xx(4),amz**2/xx(4)**2)/2d0
       hzz = fint_hdec(aml,xx,yy)*glvv**2
      else
       hzz=hvv(aml,amz**2/aml**2)/2.d0*glvv**2
      endif
      endif

c alex
c     write(1,*)'c_v= ',glvv
c end alex

c      print*,'h -> zz',hzz
c  h ---> a a
      if (aml.le.2.d0*ama) then
      haa=0
      else
      haa=gf/16.d0/dsqrt(2d0)/pi*amz**4/aml
     .   *beta_hdec(ama**2/aml**2)*glaa**2
      endif

c mmm changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0) then
            dld=0.1d0
            dlu=0.1d0
            xm1 = 2d0*ama-dld
            xm2 = 2d0*ama+dlu
            if (aml.le.ama) then
               haa = 0d0
            elseif (aml.le.xm1) then
               xa=ama**2/aml**2
               xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .              *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xa-1.d0)))
     .              /dsqrt(4.d0*xa-1.d0)
               xa2=3*gf**2/32.d0/pi**3*amz**4/aml*glaa**2*gab**2*amb**2
               haa=xa1*xa2
            elseif (aml.le.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               xa=ama**2/xx(1)**2
               xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .              *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xa-1.d0)))
     .              /dsqrt(4.d0*xa-1.d0)
               xa2=3*gf**2/32.d0/pi**3*amz**4/xx(1)*gab**2*amb**2
               yy(1)=xa1*xa2
               xa=ama**2/xx(2)**2
               xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .              *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xa-1.d0)))
     .              /dsqrt(4.d0*xa-1.d0)
               xa2=3*gf**2/32.d0/pi**3*amz**4/xx(2)*gab**2*amb**2
               yy(2)=xa1*xa2
               yy(3)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(3)
     .              *beta_hdec(ama**2/xx(3)**2)
               yy(4)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(4)
     .              *beta_hdec(ama**2/xx(4)**2)
               haa = fint_hdec(aml,xx,yy)*glaa**2
            else
               haa=gf/16d0/dsqrt(2d0)/pi*amz**4/aml*
     .              beta_hdec(ama**2/aml**2)*glaa**2
            endif
         else
            if (aml.le.2*ama) then
               haa=0
            else
               haa=gf/16d0/dsqrt(2d0)/pi*amz**4/aml*
     .              beta_hdec(ama**2/aml**2)*glaa**2
            endif
         endif
      endif
c end mmm changed 22/8/2013

c      print*,'h -> aa',haa

c  h ---> h+ h-

      if(i2hdm.eq.1) then
         if (aml.le.2*amch) then
            hlchch=0.d0
         else
            hlchch=gf/8d0/dsqrt(2d0)/pi*amz**4/aml*
     .           beta_hdec(amch**2/aml**2)*glpm**2
         endif
      elseif(i2hdm.eq.0) then
         hlchch=0.d0
      endif

c     print*,'h -> h+h-',hlchch

c  h ---> a z
      if (aml.le.amz+ama) then
      haz=0
      else
      caz=lamb_hdec(ama**2/aml**2,amz**2/aml**2)
     .   *lamb_hdec(aml**2/amz**2,ama**2/amz**2)**2
      haz=gf/8.d0/dsqrt(2d0)/pi*amz**4/aml*caz*gzal**2
      endif

c mmm changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0) then
            dld=1d0
            dlu=8d0
            xm1 = ama+amz-dld
            xm2 = ama+amz+dlu
            if (aml.lt.ama) then
               haz=0
            elseif (aml.lt.xm1) then
               if(aml.le.dabs(amz-ama))then
                  haz=0
               else
                  haz=9.d0*gf**2/8.d0/pi**3*amz**4*aml*gzal**2*
     .                 (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .                 *hvh((ama/aml)**2,(amz/aml)**2)
               endif
            elseif (aml.lt.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               yy(1)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(1)*
     .              (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .              *hvh((ama/xx(1))**2,(amz/xx(1))**2)
               yy(2)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(2)*
     .              (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .              *hvh((ama/xx(2))**2,(amz/xx(2))**2)
               caz=lamb_hdec(ama**2/xx(3)**2,amz**2/xx(3)**2)
     .              *lamb_hdec(xx(3)**2/amz**2,ama**2/amz**2)**2
               yy(3)=gf/8.d0/dsqrt(2d0)/pi*amz**4/xx(3)*caz
               caz=lamb_hdec(ama**2/xx(4)**2,amz**2/xx(4)**2)
     .              *lamb_hdec(xx(4)**2/amz**2,ama**2/amz**2)**2
               yy(4)=gf/8.d0/dsqrt(2d0)/pi*amz**4/xx(4)*caz
               haz = fint_hdec(aml,xx,yy)*gzal**2
            else
               caz=lamb_hdec(ama**2/aml**2,amz**2/aml**2)
     .              *lamb_hdec(aml**2/amz**2,ama**2/amz**2)**2
               haz=gf/8.d0/dsqrt(2d0)/pi*amz**4/aml*caz*gzal**2
            endif
         else
            if (aml.lt.amz+ama) then
               haz=0
            else
               caz=lamb_hdec(ama**2/aml**2,amz**2/aml**2)
     .              *lamb_hdec(aml**2/amz**2,ama**2/amz**2)**2
               haz=gf/8.d0/dsqrt(2d0)/pi*amz**4/aml*caz*gzal**2
            endif
         endif
      endif
c end mmm changed 22/8/2013

c     print*,'h -> az',haz

c  h ---> h+ w+

      if (aml.le.amw+amch) then
      hhw=0
      else
      chw=lamb_hdec(amch**2/aml**2,amw**2/aml**2)
     .   *lamb_hdec(aml**2/amw**2,amch**2/amw**2)**2
      hhw=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/aml*chw*ghvv**2
      endif

c mmm changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0) then
            dld=3d0
            dlu=9d0
            xm1 = amch+amw-dld
            xm2 = amch+amw+dlu
            if (aml.lt.amch) then
               hhw=0.d0
            elseif (aml.lt.xm1) then
               if(aml.le.dabs(amw-amch))then
                  hhw=0
               else
c ---- this is for a numeric check ----
                  iprint = 10

                  ivegas(1) = 30000
                  ivegas(2) = 5
                  ivegas(3) = 120000
                  ivegas(4) = 10

                  amhi = aml
                  amhj = amch
                  amhk = amw
                  gamtoti = 1.d0
                  gamtotj = 1.d0
                  gamtotk = gamw

c ---- initialization of vegas ----

c                  call rstart(12,34,56,78)

c                  call integ(hvhinteg,2,iprint,ivegas,result,relative)

c                  print*,'result',result
c ---- end numeric check

                  hhw=9.d0*gf**2/16.d0/pi**3*amw**4*aml*ghvv**2*2
     .                 *hvh((amch/aml)**2,(amw/aml)**2)
               endif
            elseif (aml.lt.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               yy(1)=9.d0*gf**2/16.d0/pi**3*amw**4*xx(1)*2
     .              *hvh((amch/xx(1))**2,(amw/xx(1))**2)
               yy(2)=9.d0*gf**2/16.d0/pi**3*amw**4*xx(2)*2
     .              *hvh((amch/xx(2))**2,(amw/xx(2))**2)
               chw=lamb_hdec(amch**2/xx(3)**2,amw**2/xx(3)**2)
     .              *lamb_hdec(xx(3)**2/amw**2,amch**2/amw**2)**2
               yy(3)=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(3)*chw
               chw=lamb_hdec(amch**2/xx(4)**2,amw**2/xx(4)**2)
     .              *lamb_hdec(xx(4)**2/amw**2,amch**2/amw**2)**2
               yy(4)=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(4)*chw
               hhw=fint_hdec(aml,xx,yy)*ghvv**2
            else
               chw=lamb_hdec(amch**2/aml**2,amw**2/aml**2)
     .              *lamb_hdec(aml**2/amw**2,amch**2/amw**2)**2
               hhw=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/aml*chw*ghvv**2
            endif
         else
            if (aml.lt.amw+amch) then
               hhw=0.d0
            else
               chw=lamb_hdec(amch**2/aml**2,amw**2/aml**2)
     .              *lamb_hdec(aml**2/amw**2,amch**2/amw**2)**2
               hhw=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/aml*chw*ghvv**2
            endif
         endif
      endif
c end mmm changed 22/8/2013

c     print*,'h -> h+w- + h-w+',hhw,hhw/2.d0

c  ============================ susy decays
      if(iofsusy.eq.0) then
c
c  hl ----> charginos
c
      do 711 i=1,2
      do 711 j=1,2
      if (aml.gt.amchar(i)+amchar(j)) then
      whlch(i,j)=gf*amw**2/(2*pi*dsqrt(2.d0))/aml
     .     *lamb_hdec(amchar(i)**2/aml**2,amchar(j)**2/aml**2)
     .     *( (ac2(i,j)**2+ac2(j,i)**2)*(aml**2-amchar(i)
     .         **2-amchar(j)**2)-4.d0*ac2(i,j)*ac2(j,i)*
     .         xmchar(i)*xmchar(j) )
      else
      whlch(i,j)=0.d0
      endif
      whlcht=whlch(1,1)+whlch(1,2)+whlch(2,1)+whlch(2,2)
 711  continue
c
c  hl ----> neutralinos
c
      do 712 i=1,4
      do 712 j=1,4
      if (aml.gt.amneut(i)+amneut(j)) then
      whlne(i,j)=gf*amw**2/(2*pi*dsqrt(2.d0))/aml
     .         *an2(i,j)**2*(aml**2-(xmneut(i)+xmneut(j))**2)
     .         *lamb_hdec(amneut(i)**2/aml**2,amneut(j)**2/aml**2)
      else
      whlne(i,j)=0.d0
      endif
 712  continue
      whlnet= whlne(1,1)+whlne(1,2)+whlne(1,3)+whlne(1,4)
     .       +whlne(2,1)+whlne(2,2)+whlne(2,3)+whlne(2,4)
     .       +whlne(3,1)+whlne(3,2)+whlne(3,3)+whlne(3,4)
     .       +whlne(4,1)+whlne(4,2)+whlne(4,3)+whlne(4,4)
ccc
c  hl ----> sleptons
c
      if (aml.gt.2.d0*amse(1)) then
      whlslel=2*gf/2.d0/dsqrt(2d0)/pi*amz**4/aml*dsin(b+a)**2
     .      *beta_hdec(amse(1)**2/aml**2)*(-0.5d0+ss)**2
      else
      whlslel=0.d0
      endif

      if (aml.gt.2.d0*amse(2)) then
      whlsler=2*gf/2.d0/dsqrt(2d0)/pi*amz**4/aml*dsin(b+a)**2
     .      *beta_hdec(amse(2)**2/aml**2)*ss**2
      else
      whlsler=0.d0
      endif

      whlslnl=0.d0
      if (aml.gt.2.d0*amsn1(1)) then
      whlslnl=2*gf/2.d0/dsqrt(2d0)/pi*amz**4/aml*dsin(b+a)**2
     .      *beta_hdec(amsn1(1)**2/aml**2)*0.5d0**2
      endif
      if (aml.gt.2.d0*amsn(1)) then
      whlslnl=whlslnl + gf/2.d0/dsqrt(2d0)/pi*amz**4/aml*dsin(b+a)**2
     .      *beta_hdec(amsn(1)**2/aml**2)*0.5d0**2
      endif

      do 718 i=1,2
      do 718 j=1,2
      if(aml.gt.amsl(i)+amsl(j)) then
      whlstau(i,j)=gf*amz**4/2.d0/dsqrt(2.d0)/pi*glee(i,j)**2*
     .      lamb_hdec(amsl(i)**2/aml**2,amsl(j)**2/aml**2)/aml
      else
      whlstau(i,j)=0.d0
      endif
 718  continue

      whlslt=whlstau(1,1)+whlstau(2,1)+whlstau(1,2)+whlstau(2,2)
     .       +whlslel+whlsler+whlslnl
c
c  hl ----> squarks
c
      if (aml.gt.2.d0*amsu(1)) then
      whlsqul=6*gf/2.d0/dsqrt(2d0)/pi*amz**4/aml*dsin(b+a)**2
     .      *beta_hdec(amsu(1)**2/aml**2)*(0.5d0-2.d0/3.d0*ss)**2
      else
      whlsqul=0.d0
      endif

      if (aml.gt.2.d0*amsu(2)) then
      whlsqur=6*gf/2.d0/dsqrt(2d0)/pi*amz**4/aml*dsin(b+a)**2
     .      *beta_hdec(amsu(2)**2/aml**2)*(-2.d0/3.d0*ss)**2
      else
      whlsqur=0.d0
      endif

      if (aml.gt.2.d0*amsd(1)) then
      whlsqdl=6*gf/2.d0/dsqrt(2d0)/pi*amz**4/aml*dsin(b+a)**2
     .      *beta_hdec(amsd(1)**2/aml**2)*(-0.5d0+1.d0/3.d0*ss)**2
      else
      whlsqdl=0.d0
      endif

      if (aml.gt.2.d0*amsd(2)) then
      whlsqdr=6*gf/2.d0/dsqrt(2d0)/pi*amz**4/aml*dsin(b+a)**2
     .      *beta_hdec(amsd(2)**2/aml**2)*(+1.d0/3.d0*ss)**2
      else
      whlsqdr=0.d0
      endif

      whlsq=whlsqul+whlsqur+whlsqdl+whlsqdr

c
c  hl ----> stops
      susy = 1
      do 713 i=1,2
      do 713 j=1,2
c     qsq = (ymst(i)+ymst(j))/2
      qsq = aml
      susy = 1
      if(aml.gt.ymst(i)+ymst(j)) then
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       call sqmbapp_hdec(qsq)
       susy = 1+sqsusy_hdec(1,1,i,j,qsq,0,1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       whlst(i,j)=3*gf*amz**4/2.d0/dsqrt(2.d0)/pi*yltt(i,j)**2*
     .      lamb_hdec(ymst(i)**2/aml**2,ymst(j)**2/aml**2)/aml
     .          *susy
c      write(6,*)'h -> stop: ',i,j,aml,ymst(i),ymst(j),susy-1,
c    .           whlst(i,j)/susy,whlst(i,j)
c      write(6,*)'h -> stop: ',i,j,aml,ymst(i),ymst(j),susy-1
      else
      whlst(i,j)=0.d0
      endif
 713  continue
c
c  hl ----> sbottoms
      susy = 1
      do 714 i=1,2
      do 714 j=1,2
c     qsq = (ymsb(i)+ymsb(j))/2
      qsq = aml
      if(aml.gt.ymsb(i)+ymsb(j)) then
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       call sqmbapp_hdec(qsq)
       susy = 1+sqsusy_hdec(1,2,i,j,qsq,0,1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       whlsb(i,j)=3*gf*amz**4/2.d0/dsqrt(2.d0)/pi*ylbb(i,j)**2*
     .       lamb_hdec(ymsb(i)**2/aml**2,ymsb(j)**2/aml**2)/aml
     .      *susy
c      write(6,*)'h -> sbot: ',i,j,aml,ymsb(i),ymsb(j),susy-1,
c    .           whlsb(i,j)/susy,whlsb(i,j)
c      write(6,*)'h -> sbot: ',i,j,aml,ymsb(i),ymsb(j),susy-1
      else
      whlsb(i,j)=0.d0
      endif
 714  continue
c
      whlstt=whlst(1,1)+whlst(1,2)+whlst(2,1)+whlst(2,2)
      whlsbb=whlsb(1,1)+whlsb(1,2)+whlsb(2,1)+whlsb(2,2)
      whlsqt=whlstt+whlsbb+whlsq

      else
      whlcht=0.d0
      whlnet=0.d0
      whlslt=0.d0
      whlsqt=0.d0
c--change thanks to elzbieta richter-was
      do i=1,2
       do j=1,2
        whlch(i,j)=0.d0
        whlst(i,j)=0.d0
        whlsb(i,j)=0.d0
        whlstau(i,j)=0.d0
       enddo
      enddo
      do i=1,4
       do j=1,4
        whlne(i,j)=0.d0
       enddo
      enddo
      endif

      if(igold.ne.0)then
c   hl ---> goldstinos
       do 710 i=1,4
       if (aml.gt.amneut(i)) then
        whlgd(i)=aml**5/axmpl**2/axmgd**2/48.d0/pi*
     .           (1.d0-amneut(i)**2/aml**2)**4*agdl(i)**2
       else
        whlgd(i)=0.d0
       endif
 710   continue
       whlgdt=whlgd(1)+whlgd(2)+whlgd(3)+whlgd(4)
      else
       whlgdt=0
      endif

c    ==========  total width and branching ratios
      wtot=hll+hmm+hss+hcc+hbb+htt+hgg+hga+hzga+hww+hzz+haa+haz+hhw
     .    +whlcht+whlnet+whlslt+whlsqt + whlgdt

c     write(6,*)'h:',wtot,hll,hmm,hss,hcc,hbb,htt,hgg,hga,hzga,hww,hzz
c    .   ,haa,haz,hhw,whlcht,whlnet,whlslt,whlsqt , whlgdt
c     write(6,*)'h:',hga

      wtot = wtot + hlchch
      hlbrchch=hlchch/wtot

c     print*,'wtot',wtot

      hlbrt=htt/wtot
      hlbrb=hbb/wtot
      hlbrl=hll/wtot
      hlbrm=hmm/wtot
      hlbrs=hss/wtot
      hlbrc=hcc/wtot
      hlbrg=hgg/wtot
      hlbrga=hga/wtot
      hlbrzga=hzga/wtot
      hlbrw=hww/wtot
      hlbrz=hzz/wtot
      hlbra=haa/wtot
      hlbraz=haz/wtot
      hlbrhw=hhw/wtot
      do 811 i=1,2
      do 811 j=1,2
      hlbrsc(i,j)=whlch(i,j)/wtot
811   continue
      do 812 i=1,4
      do 812 j=1,4
      hlbrsn(i,j)=whlne(i,j)/wtot
812   continue
      hlbrcht=whlcht/wtot
      hlbrnet=whlnet/wtot
      hlbrsl=whlslt/wtot
      hlbrsq=whlsq/wtot
      hlbrsqt=whlsqt/wtot
      hlbrgd =whlgdt/wtot
      hlwdth=wtot

      bhlslnl = whlslnl/wtot
      bhlslel = whlslel/wtot
      bhlsler = whlsler/wtot
      bhlsqul = whlsqul/wtot
      bhlsqur = whlsqur/wtot
      bhlsqdl = whlsqdl/wtot
      bhlsqdr = whlsqdr/wtot
      do i = 1,2
       do j = 1,2
        bhlst(i,j) = whlst(i,j)/wtot
        bhlsb(i,j) = whlsb(i,j)/wtot
        bhlstau(i,j) = whlstau( i,j)/wtot
       enddo
      enddo

      endif

      if(ihiggs.gt.1)then


c        =========================================================
c                       charged higgs decays
c        =========================================================
      tb=tgbet
c     =============  running masses
      rms = runm_hdec(amch,3)
      rmc = runm_hdec(amch,4)
      rmb = runm_hdec(amch,5)
      rmt = runm_hdec(amch,6)
      ash=alphas_hdec(amch,3)
c     =============== partial widths
c  h+ ---> mu nmu
      xgam = gab
      if(i2hdm.eq.1) then
         xgam = galep
      endif
      if(iofsusy.eq.0) then
       call staususy_hdec(glb,ghb,gab,xglm,xghm,xgam,qsusy,0)
      endif
      if(amch.le.ammuon) then
       hmn = 0
      else
      hmn=cff(amch,xgam,(ammuon/amch)**2,0.d0)
      endif

c     print*,''
c     print*,'h+ decay widths'
c     print*,'h+ -> nu mu',hmn

c  h+ ---> tau ntau
      xgat = gab
      if(i2hdm.eq.1) then
         xgat = galep
      endif
      if(iofsusy.eq.0) then
       call staususy_hdec(glb,ghb,gab,xglt,xght,xgat,qsusy,1)
      endif
      if(amch.le.amtau) then
       hln = 0
      else
      hln=cff(amch,xgat,(amtau/amch)**2,0.d0)
      endif

c     print*,'h+ -> tau ntau',hln

c  h+ --> su
      eps = 1.d-12
      ratx = 1
      ssusy = (amsd(1)+amsd(2)+amglu)/3*qsusy
c     ssusy = (amsd(1)+amsd(2))/2*qsusy
      if(iofsusy.eq.0) then
       call strsusy_hdec(glb,ghb,gab,xgls,xghs,xgas,ssusy,loop)
       ratx = xgas/gab
      endif
      if(amch.le.ams+eps) then
       hsu = 0
      else
       hsu1=3.d0*vus**2*cqcdm(amch,tb,(ams/amch)**2,eps,ratx)
       hsu2=3.d0*vus**2*cqcd(amch,tb,(rms/amch)**2,eps,ratx)
c mmm changed 21/8/13
       if(i2hdm.eq.1) then
          hsu1=3.d0*vus**2*
     .         cqcdm2hdm(amch,gab,gat,(ams/amch)**2,eps,ratx)
          hsu2=3.d0*vus**2*cqcd2hdm(amch,gab,gat,(rms/amch)**2,eps,ratx)
       endif
c end mmm changed 21/8/13
       if(hsu2.lt.0.d0) hsu2 = 0
       rat = ams/amch
       hsu = qqint_hdec(rat,hsu1,hsu2)
      endif

c     print*,'h+ -> su',hsu,3.d0*vus**2*
c    .         cqcdm2hdm(amch,gab,gat,(ams/amch)**2,eps,ratx)

c  h+ --> cs
      ratx = rms/ams
      raty = 1
      ssusy = (amsd(1)+amsd(2)+amglu)/3*qsusy
c     ssusy = (amsd(1)+amsd(2))/2*qsusy
      if(iofsusy.eq.0) then
       call strsusy_hdec(glb,ghb,gab,xgls,xghs,xgas,ssusy,loop)
       ratx = rms/ams*xgas/gab
       raty = xgas/gab
      endif
      if(amch.le.ams+amc) then
       hsc = 0
      else
       hsc1=3.d0*cqcdm(amch,tb,(ams/amch)**2,(amc/amch)**2,ratx)*vcs**2
       hsc2=3.d0*cqcd(amch,tb,(rms/amch)**2,(rmc/amch)**2,raty)*vcs**2
c mmm changed 21/8/13
       if(i2hdm.eq.1) then
          hsc1=3.d0*vcs**2*
     .         cqcdm2hdm(amch,gab,gat,(ams/amch)**2,(amc/amch)**2,ratx)
          hsc2=3.d0*vcs**2*
     .         cqcd2hdm(amch,gab,gat,(rms/amch)**2,(rmc/amch)**2,raty)
c         print*,'rms,rmc',hsc1,hsc2,rms,rmc
       endif
c end mmm changed 21/8/13
       if(hsc2.lt.0.d0) hsc2 = 0
       rat = (ams+amc)/amch
       hsc = qqint_hdec(rat,hsc1,hsc2)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      hsc1=3.d0*cqcdm(amch,tb,(ams/amch)**2,(amc/amch)**2,1.d0)
c      hsc2=3.d0*cqcd(amch,tb,(rms/amch)**2,(rmc/amch)**2,1.d0)
c      if(hsc2.lt.0.d0) hsc2 = 0
c      rat = (ams+amc)/amch
c      hsc0 = qqint_hdec(rat,hsc1,hsc2)
c      write(6,*)'h+- --> cs: ',amch,hsc,hsc0,hsc/hsc0,ratx**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      endif
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(53,*)amch,hsc
c     write(6,*)'h+ -> cs: ',amch,hsc,qsusy,loop,ratx,raty,ssusy
c     write(6,*)amch,hsc,gab,xgas
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     print*,'h+ -> cs',hsc

c maggie added 16/10/2013
c  h+ --> cd
      ratx = 1
      eps = 1d-12
      if(amch.le.amc) then
       hcd = 0
      else
       hcd1=3.d0*cqcdm(amch,tb,eps,(amc/amch)**2,ratx)*vcd**2
       hcd2=3.d0*cqcd(amch,tb,eps,(rmc/amch)**2,ratx)*vcd**2
       if(i2hdm.eq.1) then
          hcd1=3.d0*vcd**2*
     .         cqcdm2hdm(amch,gab,gat,eps,(amc/amch)**2,ratx)
          hcd2=3.d0*vcd**2*
     .         cqcd2hdm(amch,gab,gat,eps,(rmc/amch)**2,ratx)
       endif
       if(hcd2.lt.0.d0) hcd2 = 0
       rat = (eps+amc)/amch
       hcd = qqint_hdec(rat,hcd1,hcd2)
      endif

c     print*,'h+ -> cd',hcd
c end maggie added 16/10/2013

c  h+ --> cb
      ratx = 1
      qq = amb
      susy = 0
      xgab = gab
c     ssusy = amch
      ssusy = (amsb(1)+amsb(2)+amglu)/3*qsusy
c     ssusy = (amsb(1)+amsb(2))/2*qsusy
      as0 = alphas_hdec(ssusy,3)
      if(iofsusy.eq.0) then
       i0 = 1
c      write(6,*)
c      write(6,*)'h+ -> cb: ',amch
c      write(6,*)
       call dmbapp_hdec(i0,dglb,dghb,dgab,ssusy,loop)
       i0 = 1
       bsc = (amsq+amur+amdr)/3
c      xmb = runm_hdec(bsc,5)
       xmb = amb
c      susy = cofsusy_hdec(i0,amb,xmb,qq)*as0/pi - 2*dglb
c      write(6,*)
c      write(6,*)'h+ -> cb: ',amch
c      write(6,*)
       call botsusy_hdec(glb,ghb,gab,xglb,xghb,xgab,ssusy,loop)
      endif
      ratx = xgab/gab
c     write(6,*)'ratio = ',ratx
      if(amch.le.amb+amc) then
       hbc = 0
      else
       hbc1=3.d0*vcb**2*cqcdm(amch,tb,(amb/amch)**2,(amc/amch)**2,ratx)
       hbc2=3.d0*vcb**2*cqcd(amch,tb,(rmb/amch)**2,(rmc/amch)**2,ratx)
c mmm changed 21/8/13
       if(i2hdm.eq.1) then
          hbc1=3.d0*vcb**2*
     .         cqcdm2hdm(amch,gab,gat,(amb/amch)**2,(amc/amch)**2,ratx)
          hbc2=3.d0*vcb**2*
     .         cqcd2hdm(amch,gab,gat,(rmb/amch)**2,(rmc/amch)**2,ratx)
       endif
c end mmm changed 21/8/13
       if(hbc2.lt.0.d0) hbc2 = 0
       rat = (amb+amc)/amch
       hbc = qqint_hdec(rat,hbc1,hbc2)
      endif

c     print*,'h+ -> cb',hbc

c  h+ --> bu
      eps = 1.d-12
      if(amch.le.amb+eps) then
       hbu = 0
      else
       hbu1=3.d0*vub**2*cqcdm(amch,tb,(amb/amch)**2,eps,ratx)
       hbu2=3.d0*vub**2*cqcd(amch,tb,(rmb/amch)**2,eps,ratx)
c mmm changed 21/8/13
       if(i2hdm.eq.1) then
          hbu1=3.d0*vub**2*
     .         cqcdm2hdm(amch,gab,gat,(amb/amch)**2,eps,ratx)
          hbu2=3.d0*vub**2*
     .         cqcd2hdm(amch,gab,gat,(rmb/amch)**2,eps,ratx)
       endif
c end mmm changed 21/8/13
       if(hbu2.lt.0.d0) hbu2 = 0
       rat = amb/amch
       hbu = qqint_hdec(rat,hbu1,hbu2)
      endif

c     print*,'h+ -> ub',hbu

c  h+ --> td :
      eps = 1.d-12
      if(ionsh.eq.0)then
       dld=2d0
       dlu=2d0
       xm1 = amt-dld
       xm2 = amt+dlu
       if (amch.le.amw) then
        hdt=0.d0
       elseif (amch.le.xm1) then
        factb=3.d0*gf**2*amch*amt**4/32.d0/pi**3*gat**2
        call ctott_hdec(amch,amt,eps,amw,i2hdm,gat,gab,ctt0)
        hdt=vtd**2*factb*ctt0
       elseif (amch.le.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        factb=3.d0*gf**2*xx(1)*amt**4/32.d0/pi**3*gat**2
        call ctott_hdec(xx(1),amt,eps,amw,i2hdm,gat,gab,ctt0)
        yy(1)=vtd**2*factb*ctt0
        factb=3.d0*gf**2*xx(2)*amt**4/32.d0/pi**3*gat**2
        call ctott_hdec(xx(2),amt,eps,amw,i2hdm,gat,gab,ctt0)
        yy(2)=vtd**2*factb*ctt0
        xmb = runm_hdec(xx(3),5)
        xmt = runm_hdec(xx(3),6)
        xyz2 = 3.d0*cqcd(xx(3),tb,(eps/xx(3))**2,(xmt/xx(3))**2,ratx)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           xyz2 = 3.d0*
     .       cqcd2hdm(xx(3),gab,gat,(eps/xx(3))**2,(xmt/xx(3))**2,ratx)
        endif
c end mmm changed 21/8/13
        if(xyz2.lt.0.d0) xyz2 = 0
        xyz1 = 3.d0*cqcdm(xx(3),tb,(eps/xx(3))**2,(amt/xx(3))**2,ratx)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
        xyz1 = 3.d0*
     .      cqcdm2hdm(xx(3),gab,gat,(eps/xx(3))**2,(amt/xx(3))**2,ratx)
      endif
c end mmm changed 21/8/13
        rat = (amt)/xx(3)
        yy(3) = vtd**2*qqint_hdec(rat,xyz1,xyz2)
        xmb = runm_hdec(xx(4),5)
        xmt = runm_hdec(xx(4),6)
        xyz2 = 3.d0*cqcd(xx(4),tb,(eps/xx(4))**2,(xmt/xx(4))**2,ratx)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           xyz2 = 3.d0*
     .       cqcd2hdm(xx(4),gab,gat,(eps/xx(4))**2,(xmt/xx(4))**2,ratx)
        endif
c end mmm changed 21/8/13
        if(xyz2.lt.0.d0) xyz2 = 0
        xyz1 = 3.d0*cqcdm(xx(4),tb,(eps/xx(4))**2,(amt/xx(4))**2,ratx)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
        xyz1 = 3.d0*
     .      cqcdm2hdm(xx(4),gab,gat,(eps/xx(4))**2,(amt/xx(4))**2,ratx)
      endif
c end mmm changed 21/8/13
        rat = (amt)/xx(4)
        yy(4) = vtd**2*qqint_hdec(rat,xyz1,xyz2)
        hdt = fint_hdec(amch,xx,yy)
       else
        hdt2=3.d0*vtd**2*cqcd(amch,tb,(eps/amch)**2,(rmt/amch)**2,ratx)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           hdt2=3.d0*vtd**2*
     .          cqcd2hdm(amch,gab,gat,(eps/amch)**2,(rmt/amch)**2,ratx)
        endif
c end mmm changed 21/8/13
        if(hdt2.lt.0.d0) hdt2 = 0
        hdt1=3.d0*vtd**2*cqcdm(amch,tb,(eps/amch)**2,(amt/amch)**2,ratx)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           hdt1=3.d0*vtd**2*
     .          cqcdm2hdm(amch,gab,gat,(eps/amch)**2,(amt/amch)**2,ratx)
      endif
c end mmm changed 21/8/13
        rat = (amt)/amch
        hdt = qqint_hdec(rat,hdt1,hdt2)
       endif
      else
       if (amch.le.amt) then
        hdt=0.d0
       else
        hdt2=3.d0*vtd**2*cqcd(amch,tb,eps,(rmt/amch)**2,ratx)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           hdt2=3.d0*vtd**2*
     .          cqcd2hdm(amch,gab,gat,eps,(rmt/amch)**2,ratx)
        endif
c end mmm changed 21/8/13
        if(hdt2.lt.0.d0) hdt2 = 0
        hdt1=3.d0*vtd**2*cqcdm(amch,tb,eps,(amt/amch)**2,ratx)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           hdt1=3.d0*vtd**2*
     .          cqcdm2hdm(amch,gab,gat,eps,(amt/amch)**2,ratx)
      endif
c end mmm changed 21/8/13
        rat = (amt)/amch
        hdt = qqint_hdec(rat,hdt1,hdt2)
       endif
      endif

c     print*,'h+ -> td',hdt

c  h+ --> ts :
      eps = 1.d-12
      ratx = rms/ams
      raty = 1
      xgas = gab
      ssusy = (amsd(1)+amsd(2)+amglu)/3*qsusy
c     ssusy = (amsd(1)+amsd(2))/2*qsusy
      if(iofsusy.eq.0) then
       call strsusy_hdec(glb,ghb,gab,xgls,xghs,xgas,ssusy,loop)
       ratx = rms/ams*xgas/gab
       raty = xgas/gab
      endif
      if(ionsh.eq.0)then
       dld=2d0
       dlu=2d0
       xm1 = amt+ams-dld
       xm2 = amt+ams+dlu
       if (amch.le.amw+2*ams) then
        hst=0.d0
       elseif (amch.le.xm1) then
        factb=3.d0*gf**2*amch*amt**4/32.d0/pi**3*gat**2
        call ctott_hdec(amch,amt,eps,amw,i2hdm,gat,xgas,ctt0)
c       call ctott_hdec(amch,amt,eps,amw,i2hdm,gat,gab,ctt1)
c       write(6,*)'h+ -> ts: ',amch,ctt0/ctt1
        hst=vts**2*factb*ctt0
       elseif (amch.le.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        factb=3.d0*gf**2*xx(1)*amt**4/32.d0/pi**3*gat**2
        call ctott_hdec(xx(1),amt,eps,amw,i2hdm,gat,xgas,ctt0)
        yy(1)=vts**2*factb*ctt0
        factb=3.d0*gf**2*xx(2)*amt**4/32.d0/pi**3*gat**2
        call ctott_hdec(xx(2),amt,eps,amw,i2hdm,gat,xgas,ctt0)
        yy(2)=vts**2*factb*ctt0
        xms = runm_hdec(xx(3),3)
        xmt = runm_hdec(xx(3),6)
        xyz2 = 3.d0*cqcd(xx(3),tb,(xms/xx(3))**2,(xmt/xx(3))**2,raty)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           xyz2 = 3.d0*
     .       cqcd2hdm(xx(3),gab,gat,(xms/xx(3))**2,(xmt/xx(3))**2,raty)
        endif
c end mmm changed 21/8/13
        if(xyz2.lt.0.d0) xyz2 = 0
        xyz1 = 3.d0*cqcdm(xx(3),tb,(ams/xx(3))**2,(amt/xx(3))**2,ratx)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
        xyz1 = 3.d0*
     .      cqcdm2hdm(xx(3),gab,gat,(ams/xx(3))**2,(amt/xx(3))**2,ratx)
      endif
c end mmm changed 21/8/13
        rat = (ams+amt)/xx(3)
        yy(3) = vts**2*qqint_hdec(rat,xyz1,xyz2)
        xms = runm_hdec(xx(4),3)
        xmt = runm_hdec(xx(4),6)
        xyz2 = 3.d0*cqcd(xx(4),tb,(xms/xx(4))**2,(xmt/xx(4))**2,raty)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           xyz2 = 3.d0*
     .       cqcd2hdm(xx(4),gab,gat,(xms/xx(4))**2,(xmt/xx(4))**2,raty)
        endif
c end mmm changed 21/8/13
        if(xyz2.lt.0.d0) xyz2 = 0
        xyz1 = 3.d0*cqcdm(xx(4),tb,(ams/xx(4))**2,(amt/xx(4))**2,ratx)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
        xyz1 = 3.d0*
     .      cqcdm2hdm(xx(4),gab,gat,(ams/xx(4))**2,(amt/xx(4))**2,ratx)
      endif
c end mmm changed 21/8/13
        rat = (ams+amt)/xx(4)
        yy(4) = vts**2*qqint_hdec(rat,xyz1,xyz2)
        hst = fint_hdec(amch,xx,yy)
       else
        hst2=3.d0*vts**2*cqcd(amch,tb,(rms/amch)**2,(rmt/amch)**2,raty)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           hst2=3.d0*vts**2*
     .          cqcd2hdm(amch,gab,gat,(rms/amch)**2,(rmt/amch)**2,raty)
c          print*,'rms,rmt',rms,rmt
        endif
c end mmm changed 21/8/13
        if(hst2.lt.0.d0) hst2 = 0
        hst1=3.d0*vts**2*cqcdm(amch,tb,(ams/amch)**2,(amt/amch)**2,ratx)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           hst1=3.d0*vts**2*
     .          cqcdm2hdm(amch,gab,gat,(ams/amch)**2,(amt/amch)**2,ratx)
      endif
c end mmm changed 21/8/13
        rat = (ams+amt)/amch
        hst = qqint_hdec(rat,hst1,hst2)
       endif
      else
       if (amch.le.amt+ams) then
        hst=0.d0
       else
        hst2=3.d0*vts**2*cqcd(amch,tb,(rms/amch)**2,(rmt/amch)**2,raty)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           hst2=3.d0*vts**2*
     .          cqcd2hdm(amch,gab,gat,(rms/amch)**2,(rmt/amch)**2,raty)
        endif
c end mmm changed 21/8/13
        if(hst2.lt.0.d0) hst2 = 0
        hst1=3.d0*vts**2*cqcdm(amch,tb,(ams/amch)**2,(amt/amch)**2,ratx)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           hst1=3.d0*vts**2*
     .          cqcdm2hdm(amch,gab,gat,(ams/amch)**2,(amt/amch)**2,ratx)
      endif
c end mmm changed 21/8/13
        rat = (ams+amt)/amch
        hst = qqint_hdec(rat,hst1,hst2)
       endif
      endif

c     print*,'h+ -> ts',hst

c  h+ --> tb :
      ratx = rmb/amb
      raty = 1
      xgab = gab
      ssusy = (amsb(1)+amsb(2)+amglu)/3*qsusy
c     ssusy = (amsb(1)+amsb(2))/3*qsusy
      if(iofsusy.eq.0) then
       call botsusy_hdec(glb,ghb,gab,xglb,xghb,xgab,ssusy,loop)
       ratx = rmb/amb*xgab/gab
       raty = xgab/gab
      endif
      if(ionsh.eq.0)then
       dld=2d0
       dlu=2d0
       xm1 = amt+amb-dld
       xm2 = amt+amb+dlu
       if (amch.le.amw+2*amb) then
        hbt=0.d0
       elseif (amch.le.xm1) then
        factb=3.d0*gf**2*amch*amt**4/32.d0/pi**3*gat**2
        call ctott_hdec(amch,amt,amb,amw,i2hdm,gat,xgab,ctt0)
c       call ctott_hdec(amch,amt,amb,amw,i2hdm,gat,gab,ctt1)
c       write(6,*)'h+ -> tb: ',amch,ctt0/ctt1
        hbt=vtb**2*factb*ctt0
       elseif (amch.le.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        factb=3.d0*gf**2*xx(1)*amt**4/32.d0/pi**3*gat**2
        call ctott_hdec(xx(1),amt,amb,amw,i2hdm,gat,xgab,ctt0)
        yy(1)=vtb**2*factb*ctt0
        factb=3.d0*gf**2*xx(2)*amt**4/32.d0/pi**3*gat**2
        call ctott_hdec(xx(2),amt,amb,amw,i2hdm,gat,xgab,ctt0)
        yy(2)=vtb**2*factb*ctt0
        xmb = runm_hdec(xx(3),5)
        xmt = runm_hdec(xx(3),6)
        xyz2 = 3.d0*cqcd(xx(3),tb,(xmb/xx(3))**2,(xmt/xx(3))**2,raty)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           xyz2 = 3.d0*
     .       cqcd2hdm(xx(3),gab,gat,(xmb/xx(3))**2,(xmt/xx(3))**2,raty)
        endif
c end mmm changed 21/8/13
        if(xyz2.lt.0.d0) xyz2 = 0
        xyz1 = 3.d0*cqcdm(xx(3),tb,(amb/xx(3))**2,(amt/xx(3))**2,ratx)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
        xyz1 = 3.d0*
     .      cqcdm2hdm(xx(3),gab,gat,(amb/xx(3))**2,(amt/xx(3))**2,ratx)
      endif
c end mmm changed 21/8/13
        rat = (amb+amt)/xx(3)
        yy(3) = vtb**2*qqint_hdec(rat,xyz1,xyz2)
        xmb = runm_hdec(xx(4),5)
        xmt = runm_hdec(xx(4),6)
        xyz2 = 3.d0*cqcd(xx(4),tb,(xmb/xx(4))**2,(xmt/xx(4))**2,raty)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           xyz2 = 3.d0*
     .       cqcd2hdm(xx(4),gab,gat,(xmb/xx(4))**2,(xmt/xx(4))**2,raty)
        endif
c end mmm changed 21/8/13
        if(xyz2.lt.0.d0) xyz2 = 0
        xyz1 = 3.d0*cqcdm(xx(4),tb,(amb/xx(4))**2,(amt/xx(4))**2,ratx)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
        xyz1 = 3.d0*
     .      cqcdm2hdm(xx(4),gab,gat,(amb/xx(4))**2,(amt/xx(4))**2,ratx)
      endif
c end mmm changed 21/8/13
        rat = (amb+amt)/xx(4)
        yy(4) = vtb**2*qqint_hdec(rat,xyz1,xyz2)
        hbt = fint_hdec(amch,xx,yy)
       else
        hbt2=3.d0*vtb**2*cqcd(amch,tb,(rmb/amch)**2,(rmt/amch)**2,raty)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           hbt2=3.d0*vtb**2*
     .          cqcd2hdm(amch,gab,gat,(rmb/amch)**2,(rmt/amch)**2,raty)
c          print*,'rmb,rmt',rmb,rmt
        endif
c end mmm changed 21/8/13
        if(hbt2.lt.0.d0) hbt2 = 0
        hbt1=3.d0*vtb**2*cqcdm(amch,tb,(amb/amch)**2,(amt/amch)**2,ratx)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           hbt1=3.d0*vtb**2*
     .          cqcdm2hdm(amch,gab,gat,(amb/amch)**2,(amt/amch)**2,ratx)
      endif
c end mmm changed 21/8/13
        rat = (amb+amt)/amch
        hbt = qqint_hdec(rat,hbt1,hbt2)
       endif
      else
       if (amch.le.amt+amb) then
        hbt=0.d0
       else
        hbt2=3.d0*vtb**2*cqcd(amch,tb,(rmb/amch)**2,(rmt/amch)**2,raty)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           hbt2=3.d0*vtb**2*
     .          cqcd2hdm(amch,gab,gat,(rmb/amch)**2,(rmt/amch)**2,raty)
        endif
c end mmm changed 21/8/13
        if(hbt2.lt.0.d0) hbt2 = 0
        hbt1=3.d0*vtb**2*cqcdm(amch,tb,(amb/amch)**2,(amt/amch)**2,ratx)
c mmm changed 21/8/13
        if(i2hdm.eq.1) then
           hbt1=3.d0*vtb**2*
     .          cqcdm2hdm(amch,gab,gat,(amb/amch)**2,(amt/amch)**2,ratx)
      endif
c end mmm changed 21/8/13
        rat = (amb+amt)/amch
        hbt = qqint_hdec(rat,hbt1,hbt2)
       endif
      endif

c     print*,'h+ -> tb',hbt

c  h+ ---> w h
      if(ionsh.eq.0)then
       dld=3d0
       dlu=5d0
       xm1 = amw+aml-dld
       xm2 = amw+aml+dlu
       if (amch.lt.aml) then
        hwh=0
       elseif (amch.le.xm1) then
        if(amch.le.dabs(amw-aml))then
         hwh=0
        else
         hwh=9.d0*gf**2/16.d0/pi**3*amw**4*amch*ghvv**2
     .      *hvh((aml/amch)**2,(amw/amch)**2)
        endif
       elseif (amch.lt.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        yy(1) = 9.d0*gf**2/16.d0/pi**3*amw**4*xx(1)
     .         *hvh((aml/xx(1))**2,(amw/xx(1))**2)
        yy(2) = 9.d0*gf**2/16.d0/pi**3*amw**4*xx(2)
     .         *hvh((aml/xx(2))**2,(amw/xx(2))**2)
        cwh=lamb_hdec(aml**2/xx(3)**2,amw**2/xx(3)**2)
     .     *lamb_hdec(xx(3)**2/amw**2,aml**2/amw**2)**2
        yy(3)=gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(3)*cwh
        cwh=lamb_hdec(aml**2/xx(4)**2,amw**2/xx(4)**2)
     .     *lamb_hdec(xx(4)**2/amw**2,aml**2/amw**2)**2
        yy(4)=gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(4)*cwh
        hwh = fint_hdec(amch,xx,yy)*ghvv**2
       else
        cwh=lamb_hdec(aml**2/amch**2,amw**2/amch**2)
     .     *lamb_hdec(amch**2/amw**2,aml**2/amw**2)**2
        hwh=gf/8.d0/dsqrt(2d0)/pi*amw**4/amch*ghvv**2*cwh
       endif
      else
       if (amch.lt.amw+aml) then
        hwh=0
       else
        cwh=lamb_hdec(aml**2/amch**2,amw**2/amch**2)
     .     *lamb_hdec(amch**2/amw**2,aml**2/amw**2)**2
        hwh=gf/8.d0/dsqrt(2d0)/pi*amw**4/amch*ghvv**2*cwh
       endif
      endif

c     print*,'h+ -> w+ h',hwh

c mmm changed 21/8/2013
c  h+ ---> w h
      if(i2hdm.eq.1) then
      if(ionsh.eq.0)then
       dld=3d0
       dlu=5d0
       xm1 = amw+amh-dld
       xm2 = amw+amh+dlu
       if (amch.lt.amh) then
        hwhh=0
       elseif (amch.le.xm1) then
        if(amch.le.dabs(amw-amh))then
         hwhh=0
        else
         hwhh=9.d0*gf**2/16.d0/pi**3*amw**4*amch*glvv**2
     .      *hvh((amh/amch)**2,(amw/amch)**2)
        endif
       elseif (amch.lt.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        yy(1) = 9.d0*gf**2/16.d0/pi**3*amw**4*xx(1)
     .         *hvh((amh/xx(1))**2,(amw/xx(1))**2)
        yy(2) = 9.d0*gf**2/16.d0/pi**3*amw**4*xx(2)
     .         *hvh((amh/xx(2))**2,(amw/xx(2))**2)
        cwh=lamb_hdec(amh**2/xx(3)**2,amw**2/xx(3)**2)
     .     *lamb_hdec(xx(3)**2/amw**2,amh**2/amw**2)**2
        yy(3)=gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(3)*cwh
        cwh=lamb_hdec(amh**2/xx(4)**2,amw**2/xx(4)**2)
     .     *lamb_hdec(xx(4)**2/amw**2,amh**2/amw**2)**2
        yy(4)=gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(4)*cwh
        hwhh = fint_hdec(amch,xx,yy)*glvv**2
       else
        cwh=lamb_hdec(amh**2/amch**2,amw**2/amch**2)
     .     *lamb_hdec(amch**2/amw**2,amh**2/amw**2)**2
        hwhh=gf/8.d0/dsqrt(2d0)/pi*amw**4/amch*glvv**2*cwh
       endif
      else
       if (amch.lt.amw+amh) then
        hwhh=0
       else
        cwh=lamb_hdec(amh**2/amch**2,amw**2/amch**2)
     .     *lamb_hdec(amch**2/amw**2,amh**2/amw**2)**2
        hwhh=gf/8.d0/dsqrt(2d0)/pi*amw**4/amch*glvv**2*cwh
       endif
      endif
      endif

      if(i2hdm.eq.0) then
         hwhh=0.d0
      endif

c     print*,'h+ -> w+ h',hwhh
c end mmm changed 21/8/2013

c  h+ ---> w a
      if(ionsh.eq.0)then
       if (amch.lt.ama) then
        hwa=0
       elseif (amch.lt.amw+ama) then
        if(amch.le.dabs(amw-ama))then
         hwa=0
        else
         hwa=9.d0*gf**2/16.d0/pi**3*amw**4*amch
     .      *hvh((ama/amch)**2,(amw/amch)**2)
        endif
       else
        hwa=0.d0
       endif
      else
       if (amch.lt.amw+ama) then
        hwa=0
       else
        hwa=0.d0
       endif
      endif

c mmm changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0)then
            dld=3d0
            dlu=5d0
            xm1 = amw+ama-dld
            xm2 = amw+ama+dlu
            if (amch.lt.ama) then
               hwa=0
            elseif (amch.le.xm1) then
               if(amch.le.dabs(amw-ama))then
                  hwa=0
               else
                  hwa=9.d0*gf**2/16.d0/pi**3*amw**4*amch
     .                 *hvh((ama/amch)**2,(amw/amch)**2)

                  ivegas(1) = 30000
                  ivegas(2) = 5
                  ivegas(3) = 120000
                  ivegas(4) = 10

                  iprint = 0

                  amhi = amch
                  amhj = amw
                  amhk = ama
                  gamtoti = 1.d0
                  gamtotj = 1.d0
                  gamtotk = gamw

c ---- initialization of vegas ----

c                  call rstart(12,34,56,78)

c                  call integ(hvhinteg,2,iprint,ivegas,result,relative)

c                  hwa=9.d0*gf**2/16.d0/pi**3*amw**4*amch*result
               endif
            elseif (amch.lt.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               yy(1) = 9.d0*gf**2/16.d0/pi**3*amw**4*xx(1)
     .              *hvh((ama/xx(1))**2,(amw/xx(1))**2)
               yy(2) = 9.d0*gf**2/16.d0/pi**3*amw**4*xx(2)
     .              *hvh((ama/xx(2))**2,(amw/xx(2))**2)
               cwh=lamb_hdec(ama**2/xx(3)**2,amw**2/xx(3)**2)
     .              *lamb_hdec(xx(3)**2/amw**2,ama**2/amw**2)**2
               yy(3)=gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(3)*cwh
               cwh=lamb_hdec(ama**2/xx(4)**2,amw**2/xx(4)**2)
     .              *lamb_hdec(xx(4)**2/amw**2,ama**2/amw**2)**2
               yy(4)=gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(4)*cwh
               hwa = fint_hdec(amch,xx,yy)
            else
               cwh=lamb_hdec(ama**2/amch**2,amw**2/amch**2)
     .              *lamb_hdec(amch**2/amw**2,ama**2/amw**2)**2
               hwa=gf/8.d0/dsqrt(2d0)/pi*amw**4/amch*cwh
            endif
         else
            if (amch.lt.amw+ama) then
               hwa=0
            else
               cwh=lamb_hdec(ama**2/amch**2,amw**2/amch**2)
     .              *lamb_hdec(amch**2/amw**2,ama**2/amw**2)**2
               hwa=gf/8.d0/dsqrt(2d0)/pi*amw**4/amch*cwh
            endif
         endif
      endif
c end mmm changed 22/8/2013

c     print*,'h+ -> w+ a',hwa

c  ======================= susy decays
      if(iofsusy.eq.0) then
c
c  h+ ----> charginos+neutralinos
c
      do 751 i=1,2
      do 751 j=1,4
      if (amch.gt.amchar(i)+amneut(j)) then
      whccn(i,j)=gf*amw**2/(2*pi*dsqrt(2.d0))/amch
     .   *lamb_hdec(amchar(i)**2/amch**2,amneut(j)**2/amch**2)*(
     .   (acnl(i,j)**2+acnr(i,j)**2)*(amch**2-amchar(i)**2-xmneut(j)
     .   **2)-4.d0*acnl(i,j)*acnr(i,j)*xmchar(i)*xmneut(j) )
      else
      whccn(i,j)=0.d0
      endif
 751  continue

      whccnt=whccn(1,1)+whccn(1,2)+whccn(1,3)+whccn(1,4)
     .      +whccn(2,1)+whccn(2,2)+whccn(2,3)+whccn(2,4)
c
c  h+ ----> sleptons
c
      if (amch.gt.amse(1)+amsn1(1)) then
      whcsl00=2*gf/4.d0/dsqrt(2d0)/pi*amw**4/amch*dsin(2.d0*b)**2
     .     *lamb_hdec(amse(1)**2/amch**2,amsn1(1)**2/amch**2)
      else
      whcsl00=0.d0
      endif

      if (amch.gt.amsl(1)+amsn(1)) then
      whcsl11=gf/2.d0/dsqrt(2d0)/pi*amw**4/amch*gcen(1,1)**2
     .     *lamb_hdec(amsl(1)**2/amch**2,amsn(1)**2/amch**2)
      else
      whcsl11=0.d0
      endif

      if (amch.gt.amsl(2)+amsn(1)) then
      whcsl21=gf/2.d0/dsqrt(2d0)/pi*amw**4/amch*gcen(1,2)**2
     .     *lamb_hdec(amsl(2)**2/amch**2,amsn(1)**2/amch**2)
      else
      whcsl21=0.d0
      endif

      whcslt=whcsl00+whcsl11+whcsl21

c
c  h+ ----> squarks
c
      if (amch.gt.amsu(1)+amsd(1)) then
      whcsq=6*gf/4.d0/dsqrt(2d0)/pi*amw**4/amch*dsin(2.d0*b)**2
     .     *lamb_hdec(amsu(1)**2/amch**2,amsd(1)**2/amch**2)
      else
      whcsq=0.d0
      endif
c
      do 753 i=1,2
      do 753 j=1,2
      if(amch.gt.amst(i)+amsb(j)) then
      whcstb(i,j)=3*gf*amw**4/2.d0/dsqrt(2.d0)/pi*gctb(i,j)**2
     .      *lamb_hdec(amst(i)**2/amch**2,amsb(j)**2/amch**2)/amch
      else
      whcstb(i,j)=0.d0
      endif

 753  continue
c
      whcsqt=whcsq+whcstb(1,1)+whcstb(1,2)+whcstb(2,1)+whcstb(2,2)

      else
      whccnt=0.d0
      whcslt=0.d0
      whcsqt=0.d0
      whcgdt=0.d0
c--change thanks to elzbieta richter-was
      do i=1,2
       do j=1,2
        whcstb(i,j)=0.d0
       enddo
      enddo
      do i=1,2
       do j=1,4
        whccn(i,j)=0.d0
       enddo
      enddo
      endif

      if(igold.ne.0)then
c   hc ---> goldstinos
       do 750 i=1,2
       if (amch.gt.amchar(i)) then
        whcgd(i)=amch**5/axmpl**2/axmgd**2/48.d0/pi*
     .           (1.d0-amchar(i)**2/amch**2)**4*agdc(i)**2
       else
        whcgd(i)=0.d0
       endif
 750   continue
       whcgdt=whcgd(1)+whcgd(2)
      else
       whcgdt=0
      endif
c
c    ==========  total width and branching ratios
c
      wtot=hln+hmn+hsu+hbu+hsc+hbc+hbt+hwh+hwa+whccnt+whcslt+whcsqt
     .    +whcgdt

c mmm changed 21/8/2013
      wtot=wtot+hcd+hst+hdt

      if(i2hdm.eq.1) then
         wtot = wtot+hwhh
         hcbrwhh=hwhh/wtot
      endif
      if(i2hdm.eq.0) then
         hcbrwhh=0.d0
      endif
c end mmm changed 21/8/2013
      hcbrcd=hcd/wtot
      hcbrts=hst/wtot
      hcbrtd=hdt/wtot

c     print*,'wtot',wtot

      hcbrl=hln/wtot
      hcbrm=hmn/wtot
      hcbrs=hsu/wtot
      hcbrbu=hbu/wtot
      hcbrc=hsc/wtot
      hcbrb=hbc/wtot
      hcbrt=hbt/wtot
      hcbrw=hwh/wtot
      hcbra=hwa/wtot
      do 851 i=1,2
      do 851 j=1,4
      hcbrsu(i,j)=whccn(i,j)/wtot
851   continue
      hcbrcnt=whccnt/wtot
      hcbrsl=whcslt/wtot
      hcbrsq=whcsq/wtot
      hcbrsqt=whcsqt/wtot
      do 853 i=1,2
      do 853 j=1,2
      hcbrstb(i,j)=whcstb(i,j)/wtot
853   continue
      hcbrgd=whcgdt/wtot
      hcwdth=wtot

      bhcsl00 = whcsl00/wtot
      bhcsl11 = whcsl11/wtot
      bhcsl21 = whcsl21/wtot
      bhcsq = whcsq/wtot
      do i = 1,2
       do j = 1,2
        bhcstb(i,j) = whcstb(i,j)/wtot
       enddo
      enddo

      gamc0 = wtot

      endif

      if(ihiggs.eq.2.or.ihiggs.eq.5)then

c        =========================================================
c                       heavy cp even higgs decays
c        =========================================================
c     =============  running masses
      rms = runm_hdec(amh,3)
      rmc = runm_hdec(amh,4)
      rmb = runm_hdec(amh,5)
      rmt = runm_hdec(amh,6)
      ratcoup = ght/ghb
      higtop = amh**2/amt**2

      ash=alphas_hdec(amh,3)
      amc0=1.d8
      amb0=2.d8
c     amt0=3.d8
      as3=alphas_hdec(amh,3)
      amc0=amc
      as4=alphas_hdec(amh,3)
      amb0=amb
c     amt0=amt

c     =============== partial widths
c  h ---> g g
       eps=1.d-8
       nfext = 3
       asg = as3
       ctt = 4*amt**2/amh**2*dcmplx(1d0,-eps)
       ctb = 4*amb**2/amh**2*dcmplx(1d0,-eps)
       cat = 2*ctt*(1+(1-ctt)*cf(ctt))*ght
       cab = 2*ctb*(1+(1-ctb)*cf(ctb))*ghb
       ctc = 4*amc**2/amh**2*dcmplx(1d0,-eps)
       cac = 2*ctc*(1+(1-ctc)*cf(ctc))*ght
c
       if(iofsusy.eq.0) then
       csb1= 4*amsb(1)**2/amh**2*dcmplx(1d0,-eps)
       csb2= 4*amsb(2)**2/amh**2*dcmplx(1d0,-eps)
       cst1= 4*amst(1)**2/amh**2*dcmplx(1d0,-eps)
       cst2= 4*amst(2)**2/amh**2*dcmplx(1d0,-eps)
c
       cxb1=-amz**2/amsb(1)**2*csb1*(1-csb1*cf(csb1))*ghbb(1,1)
       cxb2=-amz**2/amsb(2)**2*csb2*(1-csb2*cf(csb2))*ghbb(2,2)
       cxt1=-amz**2/amst(1)**2*cst1*(1-cst1*cf(cst1))*ghtt(1,1)
       cxt2=-amz**2/amst(2)**2*cst2*(1-cst2*cf(cst2))*ghtt(2,2)
c
       csul = 4*amsu(1)**2/amh**2*dcmplx(1d0,-eps)
       csur = 4*amsu(2)**2/amh**2*dcmplx(1d0,-eps)
       csdl = 4*amsd(1)**2/amh**2*dcmplx(1d0,-eps)
       csdr = 4*amsd(2)**2/amh**2*dcmplx(1d0,-eps)
       cxul=-2*(1.d0/2.d0-2.d0/3.d0*ss)*amz**2/amsu(1)**2*dcos(a+b)
     .      *csul*(1-csul*cf(csul))
       cxur=-2*(2.d0/3.d0*ss)*amz**2/amsu(2)**2*dcos(a+b)
     .      *csur*(1-csur*cf(csur))
       cxdl=-2*(-1.d0/2.d0+1.d0/3.d0*ss)*amz**2/amsd(1)**2*dcos(a+b)
     .      *csdl*(1-csdl*cf(csdl))
       cxdr=-2*(-1.d0/3.d0*ss)*amz**2/amsd(2)**2*dcos(a+b)
     .      *csdr*(1-csdr*cf(csdr))
       else
       cxb1=0.d0
       cxb2=0.d0
       cxt1=0.d0
       cxt2=0.d0
       cxul=0.d0
       cxur=0.d0
       cxdl=0.d0
       cxdr=0.d0
       endif

       fqcd=hggqcd(asg,nfext)
       sqcd=sggqcd(asg)
       xfac = cdabs(cat+cab+cac+cxb1+cxb2+cxt1+cxt2
     .             +cxul+cxur+cxdl+cxdr)**2*fqcd
     .      + dreal(dconjg(cat+cab+cac+cxb1+cxb2+cxt1+cxt2
     .                    +cxul+cxur+cxdl+cxdr)
     .             *(cxb1+cxb2+cxt1+cxt2+cxul+cxur+cxdl+cxdr))*sqcd
       hgg=hvv(amh,0.d0)*(asg/pi)**2*xfac/8

c      write(6,*)'ghb, ght: ',ghb,ght

c      print*,''
c      print*,'h decay widths'
c      print*,'hgg_nlo',hgg

c  h ---> g g* ---> g cc   to be added to h ---> cc
       nfext = 4
       asg = as4
       fqcd=hggqcd(asg,nfext)
       sqcd=sggqcd(asg)
       xfac = cdabs(cat+cab+cac+cxb1+cxb2+cxt1+cxt2
     .             +cxul+cxur+cxdl+cxdr)**2*fqcd
     .      + dreal(dconjg(cat+cab+cac+cxb1+cxb2+cxt1+cxt2
     .                    +cxul+cxur+cxdl+cxdr)
     .             *(cxb1+cxb2+cxt1+cxt2+cxul+cxur+cxdl+cxdr))*sqcd
       dcc=hvv(amh,0.d0)*(asg/pi)**2*xfac/8 - hgg

c  h ---> g g* ---> g bb   to be added to h ---> bb
       nfext = 5
       asg = ash
       fqcd=hggqcd(asg,nfext)
       sqcd=sggqcd(asg)
       xfac = cdabs(cat+cab+cac+cxb1+cxb2+cxt1+cxt2
     .             +cxul+cxur+cxdl+cxdr)**2*fqcd
     .      + dreal(dconjg(cat+cab+cac+cxb1+cxb2+cxt1+cxt2
     .                    +cxul+cxur+cxdl+cxdr)
     .             *(cxb1+cxb2+cxt1+cxt2+cxul+cxur+cxdl+cxdr))*sqcd
       dbb=hvv(amh,0.d0)*(asg/pi)**2*xfac/8 - hgg - dcc
       hgg=hvv(amh,0.d0)*(asg/pi)**2*xfac/8

c  h ---> g g: full nnnlo corrections to top loops for nf=5
       fqcd0=hggqcd(asg,5)
       fqcd=hggqcd2(asg,5,amh,amt)
       xfac = cdabs(cat+cab+cac)**2*(fqcd-fqcd0)
       hgg=hgg+hvv(amh,0.d0)*(asg/pi)**2*xfac/8

      if(nfgg.eq.3)then
       hgg = hgg - dbb - dcc
      elseif(nfgg.eq.4)then
       hgg = hgg - dbb
       dcc = 0
      else
       dcc = 0
       dbb = 0
      endif

c      print*,'hgg_nnlo',hgg

c  h ---> mu mu
      xglm = glb
      xghm = ghb
      xgam = gab
      if(i2hdm.eq.1) then
         xghm = ghlep
      endif
      if(iofsusy.eq.0) then
       call staususy_hdec(glb,ghb,gab,xglm,xghm,xgam,qsusy,0)
      endif
      if(amh.le.2*ammuon) then
       hmm = 0
      else
      hmm=hff(amh,(ammuon/amh)**2)*xghm**2
      endif

c      print*,'h -> mumu',hmm
c  h ---> ll
      xglt = glb
      xght = ghb
      xgat = gab
      if(i2hdm.eq.1) then
         xght = ghlep
      endif
      if(iofsusy.eq.0) then
       call staususy_hdec(glb,ghb,gab,xglt,xght,xgat,qsusy,1)
      endif
      if(amh.le.2*amtau) then
       hll = 0
      else
      hll=hff(amh,(amtau/amh)**2)*xght**2
      endif

c     write(6,*)'h: tau/mu: ',hll/hmm*ammuon**2/amtau**2,xght**2/xghm**2
c       print*,'h -> tautau',hll
c  h --> ss
      xgls = glb
      xghs = ghb
      xgas = gab
      ssusy = (amsd(1)+amsd(2)+amglu)/3*qsusy
c     ssusy = (amsd(1)+amsd(2))/3*qsusy
      if(iofsusy.eq.0) then
       call strsusy_hdec(glb,ghb,gab,xgls,xghs,xgas,ssusy,loop)
      endif
      if(amh.le.2*ams) then
       hss = 0
      else
       hs1=3.d0*hff(amh,(ams/amh)**2)
     .    *xghs**2
     .    *tqcdh(ams**2/amh**2)
       hs2=3.d0*hff(amh,(rms/amh)**2)*xghs**2
     .    *qcdh(rms**2/amh**2)
       if(hs2.lt.0.d0) hs2 = 0
       rat = 2*ams/amh
       hss = qqint_hdec(rat,hs1,hs2)
      endif

c      print*,'h -> ss',hss
c  h --> cc
      ratcoup = 1
      if(amh.le.2*amc) then
       hcc = 0
      else
       hc1=3.d0*hff(amh,(amc/amh)**2)
     .    *ght**2
     .    *tqcdh(amc**2/amh**2)
       hc2=3.d0*hff(amh,(rmc/amh)**2)*ght**2
     .    *qcdh(rmc**2/amh**2)
     .   + dcc
       if(hc2.lt.0.d0) hc2 = 0
       rat = 2*amc/amh
       hcc = qqint_hdec(rat,hc1,hc2)
      endif

c      print*,'h -> cc',hcc
c  h --> bb :
      qq = amb
      susy = 0
      xghb = ghb
      ssusy = (amsb(1)+amsb(2)+amglu)/3*qsusy
      fsusy = susyscale
      as0 = alphas_hdec(fsusy,3)
      if(iofsusy.eq.0) then
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       i0 = 0
       call dmbapp_hdec(i0,dglb,dghb,dgab,fsusy,loop)
       delb1 = -dgab/(1+1/tgbet**2)
       delb0 = delb1/(1-delb1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       i0 = 1
       call dmbapp_hdec(i0,dglb,dghb,dgab,fsusy,loop)
       i0 = 2
       bsc = (amsq+amur+amdr)/3
       xmb = runm_hdec(fsusy,5)/(1+delb0)
c      xmb = amb
c1357
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       if(islhai.ne.0) xmb = amb
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       susy = cofsusy_hdec(i0,amb,xmb,qq)*as0/pi - 2*dghb
       call botsusy_hdec(glb,ghb,gab,xglb,xghb,xgab,ssusy,loop)
      endif
      ratcoup = ght/xghb
      if(amh.le.2*amb) then
       hbb = 0
      else
       hb1=3.d0*hff(amh,(amb/amh)**2)
     .    *(xghb**2+xghb*ghb*susy)
     .    *tqcdh(amb**2/amh**2)
       hb2=3.d0*hff(amh,(rmb/amh)**2)
     .    *(xghb**2+xghb*ghb*susy)
     .    *qcdh(rmb**2/amh**2)
     .   + dbb
       if(hb2.lt.0.d0) hb2 = 0
       rat = 2*amb/amh
       hbb = qqint_hdec(rat,hb1,hb2)
c     write(6,*)'h -> bb:  ',rat,hb1,hb2
c     write(6,*)'h -> bb:  ',ghb,xghb,susy
c     write(6,*)'h -> bb1: ',cofsusy_hdec(i0,amb,xmb,qq),dghb

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      xxx = cofsusy_hdec(i0,amb,xmb,qq)
c      write(1,*)'h -> bb: ',xxx*as0/pi
c      write(1,*)'h -> bb: ',2*dghb
c      write(1,*)'h -> bb: ',ghb,xghb,susy
c      write(6,('a3,4(1x,g15.8)'))'h: ',ama,amh,susy+2*dghb,
c    .                             susy/(susy+2*dghb)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      endif
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)'h -> bb: ',xghb**2,xghb*ghb*susy/xghb**2,
c    .                      (xghb**2+xghb*ghb*susy)/xghb**2
c     write(6,*)'approx:  ',susy+2*dghb,2*dghb,susy
c     fac = as0/pi
c     write(6,*)'approx2: ',(susy+2*dghb)/fac,2*dghb/fac,susy/fac
c     write(51,*)amh,hbb
c     write(51,*)ghb,xghb
c     write(51,*)susy,cofsusy_hdec(i0,amb,xmb,qq)*as0/pi,-2*dghb
c     write(51,*)ssusy,as0,qsusy,(amsb(1)+amsb(2)+amglu)/3
c     write(6,*)'h -> bb: ',amh,hbb,qsusy1,loop
c     write(6,*)ghb,xghb,xghb**2+xghb*ghb*susy
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c      print*,'h -> bb',hbb
c      print*
c      print*,'h -> bb:',hbb,ghb,xghb,ghb*susy,2*dghb,susy,2*dghb+susy
c  h ---> tt
      ratcoup = 0
      call topsusy_hdec(glt,ght,gat,xgltop,xghtop,xgatop,scale,1)
c     write(6,*)xgltop,xghtop,xgatop
      if(i2hdm.eq.0) then
      if(ionsh.eq.0)then
       dld=3d0
       dlu=5d0
       xm1 = 2d0*amt-dld
       xm2 = 2d0*amt+dlu
       if (amh.le.amt+amw+amb) then
        htt=0.d0
       elseif (amh.le.xm1) then
        factt=6.d0*gf**2*amh**3*amt**2/2.d0/128.d0/pi**3
        call htott_hdec(amh,amt,amb,amw,amch,ght,ghb,gat,gab,
     .              ghvv,gzal,htt0)
        htt=factt*htt0
       elseif (amh.le.xm2) then
        zzma=amar
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        call amhama_hdec(2,xx(1),tgbet)
        factt=6.d0*gf**2*xx(1)**3*amt**2/2.d0/128.d0/pi**3
        call htott_hdec(xx(1),amt,amb,amw,amch,ght,ghb,gat,gab,
     .              ghvv,gzal,htt0)
        yy(1)=factt*htt0
        call amhama_hdec(2,xx(2),tgbet)
        factt=6.d0*gf**2*xx(2)**3*amt**2/2.d0/128.d0/pi**3
        call htott_hdec(xx(2),amt,amb,amw,amch,ght,ghb,gat,gab,
     .              ghvv,gzal,htt0)
        yy(2)=factt*htt0
        call amhama_hdec(2,xx(3),tgbet)
        xmt = runm_hdec(xx(3),6)
        ht1=3.d0*hff(xx(3),(amt/xx(3))**2)*ght**2
     .    *tqcdh(amt**2/xx(3)**2)
        ht2=3.d0*hff(xx(3),(xmt/xx(3))**2)*ght**2
     .    *qcdh(xmt**2/xx(3)**2)
        if(ht2.lt.0.d0) ht2 = 0
        rat = 2*amt/xx(3)
        yy(3) = qqint_hdec(rat,ht1,ht2)
        call amhama_hdec(2,xx(4),tgbet)
        xmt = runm_hdec(xx(4),6)
        ht1=3.d0*hff(xx(4),(amt/xx(4))**2)*ght**2
     .    *tqcdh(amt**2/xx(4)**2)
        ht2=3.d0*hff(xx(4),(xmt/xx(4))**2)*ght**2
     .    *qcdh(xmt**2/xx(4)**2)
        if(ht2.lt.0.d0) ht2 = 0
        rat = 2*amt/xx(4)
        yy(4) = qqint_hdec(rat,ht1,ht2)
        ama = zzma
        call twohdmcp_hdec(tgbet)
        htt=fint_hdec(amh,xx,yy)
       else
        ht1=3.d0*hff(amh,(amt/amh)**2)*ght**2
     .    *tqcdh(amt**2/amh**2)
        ht2=3.d0*hff(amh,(rmt/amh)**2)*ght**2
     .    *qcdh(rmt**2/amh**2)
        if(ht2.lt.0.d0) ht2 = 0
        rat = 2*amt/amh
        htt = qqint_hdec(rat,ht1,ht2)
       endif
      else
       if (amh.le.2.d0*amt) then
        htt=0.d0
       else
        ht1=3.d0*hff(amh,(amt/amh)**2)*ght**2
     .    *tqcdh(amt**2/amh**2)
        ht2=3.d0*hff(amh,(rmt/amh)**2)*ght**2
     .    *qcdh(rmt**2/amh**2)
        if(ht2.lt.0.d0) ht2 = 0
        rat = 2*amt/amh
        htt = qqint_hdec(rat,ht1,ht2)
       endif
      endif
      endif

c mmm changed 21/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0)then
            dld=5.d0
            dlu=3.d0
            xm1 = 2d0*amt-dld
            xm2 = 2d0*amt+dlu
            if (amh.le.amt+amw+amb) then
               htt=0.d0
            elseif (amh.le.xm1) then
               factt=6.d0*gf**2*amh**3*amt**2/2.d0/128.d0/pi**3
               call htott_hdec(amh,amt,amb,amw,amch,ght,ghb,gat,gab,
     .              ghvv,gzal,htt0)
               htt=factt*htt0
            elseif (amh.le.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0

               factt=6.d0*gf**2*xx(1)**3*amt**2/2.d0/128.d0/pi**3
               call htott_hdec(xx(1),amt,amb,amw,amch,ght,ghb,gat,gab,
     .              ghvv,gzal,htt0)
               yy(1)=factt*htt0

               factt=6.d0*gf**2*xx(2)**3*amt**2/2.d0/128.d0/pi**3
               call htott_hdec(xx(2),amt,amb,amw,amch,ght,ghb,gat,gab,
     .              ghvv,gzal,htt0)
               yy(2)=factt*htt0

               xmt = runm_hdec(xx(3),6)
               ht1=3.d0*hff(xx(3),(amt/xx(3))**2)*ght**2
     .              *tqcdh(amt**2/xx(3)**2)
               ht2=3.d0*hff(xx(3),(xmt/xx(3))**2)*ght**2
     .              *qcdh(xmt**2/xx(3)**2)
               if(ht2.lt.0.d0) ht2 = 0
               rat = 2*amt/xx(3)
               yy(3) = qqint_hdec(rat,ht1,ht2)

               xmt = runm_hdec(xx(4),6)
               ht1=3.d0*hff(xx(4),(amt/xx(4))**2)*ght**2
     .              *tqcdh(amt**2/xx(4)**2)
               ht2=3.d0*hff(xx(4),(xmt/xx(4))**2)*ght**2
     .              *qcdh(xmt**2/xx(4)**2)
               if(ht2.lt.0.d0) ht2 = 0
               rat = 2*amt/xx(4)
               yy(4) = qqint_hdec(rat,ht1,ht2)

               htt=fint_hdec(amh,xx,yy)
            else
               ht1=3.d0*hff(amh,(amt/amh)**2)*ght**2
     .              *tqcdh(amt**2/amh**2)
               ht2=3.d0*hff(amh,(rmt/amh)**2)*ght**2
     .              *qcdh(rmt**2/amh**2)
               if(ht2.lt.0.d0) ht2 = 0
               rat = 2.d0*amt/amh
               htt = qqint_hdec(rat,ht1,ht2)
            endif
         else
            if (amh.le.2.d0*amt) then
               htt=0.d0
            else
               ht1=3.d0*hff(amh,(amt/amh)**2)*ght**2
     .              *tqcdh(amt**2/amh**2)
               ht2=3.d0*hff(amh,(rmt/amh)**2)*ght**2
     .              *qcdh(rmt**2/amh**2)
               if(ht2.lt.0.d0) ht2 = 0
               rat = 2.d0*amt/amh
               htt = qqint_hdec(rat,ht1,ht2)
            endif
         endif
      endif
c end mmm changed 21/8/2103

c      print*,'h -> tt',htt

c  h ---> gamma gamma
       eps=1.d-8
       xrmc = runm_hdec(amh/2,4)*amc/runm_hdec(amc,4)
       xrmb = runm_hdec(amh/2,5)*amb/runm_hdec(amb,5)
       xrmt = runm_hdec(amh/2,6)*amt/runm_hdec(amt,6)
       ctt = 4*xrmt**2/amh**2*dcmplx(1d0,-eps)
       ctb = 4*xrmb**2/amh**2*dcmplx(1d0,-eps)
       ctl = 4*amtau**2/amh**2*dcmplx(1d0,-eps)
       ctw = 4*amw**2/amh**2*dcmplx(1d0,-eps)
       cth = 4*amch**2/amh**2*dcmplx(1d0,-eps)
       ctc = 4*xrmc**2/amh**2*dcmplx(1d0,-eps)
       cac = 4/3d0 * 2*ctc*(1+(1-ctc)*cf(ctc))*ght
     .     * cfacq_hdec(0,amh,xrmc)
       cat = 4/3d0 * 2*ctt*(1+(1-ctt)*cf(ctt))*ght
     .     * cfacq_hdec(0,amh,xrmt)
       cab = 1/3d0 * 2*ctb*(1+(1-ctb)*cf(ctb))*ghb
     .     * cfacq_hdec(0,amh,xrmb)
       cal = 1.d0  * 2*ctl*(1+(1-ctl)*cf(ctl))*ghb
       if(i2hdm.eq.1) then
          cal = 1.d0  * 2*ctl*(1+(1-ctl)*cf(ctl))*ghlep
       endif
       caw = -(2+3*ctw+3*ctw*(2-ctw)*cf(ctw))*ghvv
       cah = -amz**2/2/amch**2*cth*(1-cth*cf(cth))*ghpm
       if(iofsusy.eq.0) then
        rmsu1 = runms_hdec(amh/2,amsu(1))
        rmsu2 = runms_hdec(amh/2,amsu(2))
        rmsd1 = runms_hdec(amh/2,amsd(1))
        rmsd2 = runms_hdec(amh/2,amsd(2))
        rmsb1 = runms_hdec(amh/2,amsb(1))
        rmsb2 = runms_hdec(amh/2,amsb(2))
        rmst1 = runms_hdec(amh/2,amst(1))
        rmst2 = runms_hdec(amh/2,amst(2))
        cx1 = 4*amchar(1)**2/amh**2*dcmplx(1d0,-eps)
        cx2 = 4*amchar(2)**2/amh**2*dcmplx(1d0,-eps)
        cax1= amw/xmchar(1) * 2*cx1*(1+(1-cx1)*cf(cx1))*2*ac1(1,1)
        cax2= amw/xmchar(2) * 2*cx2*(1+(1-cx2)*cf(cx2))*2*ac1(2,2)
        csl1= 4*amsl(1)**2/amh**2*dcmplx(1d0,-eps)
        csl2= 4*amsl(2)**2/amh**2*dcmplx(1d0,-eps)
        csb1= 4*rmsb1**2/amh**2*dcmplx(1d0,-eps)
        csb2= 4*rmsb2**2/amh**2*dcmplx(1d0,-eps)
        cst1= 4*rmst1**2/amh**2*dcmplx(1d0,-eps)
        cst2= 4*rmst2**2/amh**2*dcmplx(1d0,-eps)

        csel = 4*amse(1)**2/amh**2*dcmplx(1d0,-eps)
        cser = 4*amse(2)**2/amh**2*dcmplx(1d0,-eps)
        csul = 4*rmsu1**2/amh**2*dcmplx(1d0,-eps)
        csur = 4*rmsu2**2/amh**2*dcmplx(1d0,-eps)
        csdl = 4*rmsd1**2/amh**2*dcmplx(1d0,-eps)
        csdr = 4*rmsd2**2/amh**2*dcmplx(1d0,-eps)
        cxel=-2*(-1/2d0+ss)*amz**2/amse(1)**2*dcos(a+b)
     .       *csel*(1-csel*cf(csel))
        cxer=2*(ss)*amz**2/amse(2)**2*dcos(a+b)
     .       *cser*(1-cser*cf(cser))
        cxul=-2*4.d0/3.d0*(1.d0/2.d0-2.d0/3.d0*ss)
     .       *amz**2/amsu(1)**2*dcos(a+b)*csul*(1-csul*cf(csul))
     .      * cfacsq_hdec(amh,rmsu1)
        cxur=-2*4.d0/3.d0*(2.d0/3.d0*ss)
     .       *amz**2/amsu(2)**2*dcos(a+b)*csur*(1-csur*cf(csur))
     .      * cfacsq_hdec(amh,rmsu2)
        cxdl=-2/3.d0*(-1.d0/2.d0+1.d0/3.d0*ss)
     .       *amz**2/amsd(1)**2*dcos(a+b)*csdl*(1-csdl*cf(csdl))
     .      * cfacsq_hdec(amh,rmsd1)
        cxdr=-2/3.d0*(-1.d0/3.d0*ss)
     .       *amz**2/amsd(2)**2*dcos(a+b)*csdr*(1-csdr*cf(csdr))
     .      * cfacsq_hdec(amh,rmsd2)

        cxb1= -1/3d0*amz**2/amsb(1)**2*csb1*(1-csb1*cf(csb1))*ghbb(1,1)
     .      * cfacsq_hdec(amh,rmsb1)
        cxb2= -1/3d0*amz**2/amsb(2)**2*csb2*(1-csb2*cf(csb2))*ghbb(2,2)
     .      * cfacsq_hdec(amh,rmsb2)
        cxt1= -4/3d0*amz**2/amst(1)**2*cst1*(1-cst1*cf(cst1))*ghtt(1,1)
     .      * cfacsq_hdec(amh,rmst1)
        cxt2= -4/3d0*amz**2/amst(2)**2*cst2*(1-cst2*cf(cst2))*ghtt(2,2)
     .      * cfacsq_hdec(amh,rmst2)
        cxl1=       -amz**2/amsl(1)**2*csl1*(1-csl1*cf(csl1))*ghee(1,1)
        cxl2=       -amz**2/amsl(2)**2*csl2*(1-csl2*cf(csl2))*ghee(2,2)
        xfac = cdabs(cat+cab+cac+cal+caw+cah+cax1+cax2
     .       +  cxel+cxer+cxul+cxur+cxdl+cxdr
     .       +  cxb1+cxb2+cxt1+cxt2+cxl1+cxl2)**2
       else
        xfac = cdabs(cat+cab+cac+cal+caw+cah)**2
       endif
       hga=hvv(amh,0.d0)*(alph/pi)**2/16.d0*xfac

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      cac0= 4/3d0 * 2*ctc*(1+(1-ctc)*cf(ctc))*ght
c      cat0= 4/3d0 * 2*ctt*(1+(1-ctt)*cf(ctt))*ght
c      cab0= 1/3d0 * 2*ctb*(1+(1-ctb)*cf(ctb))*ghb

c      cxul0=-2*4.d0/3.d0*(1.d0/2.d0-2.d0/3.d0*ss)
c    .       *amz**2/amsu(1)**2*dcos(a+b)*csul*(1-csul*cf(csul))
c      cxur0=-2*4.d0/3.d0*(2.d0/3.d0*ss)
c    .       *amz**2/amsu(2)**2*dcos(a+b)*csur*(1-csur*cf(csur))
c      cxdl0=-2/3.d0*(-1.d0/2.d0+1.d0/3.d0*ss)
c    .       *amz**2/amsd(1)**2*dcos(a+b)*csdl*(1-csdl*cf(csdl))
c      cxdr0=-2/3.d0*(-1.d0/3.d0*ss)
c    .       *amz**2/amsd(2)**2*dcos(a+b)*csdr*(1-csdr*cf(csdr))
c      cxb10= -1/3d0*amz**2/amsb(1)**2*csb1*(1-csb1*cf(csb1))*ghbb(1,1)
c      cxb20= -1/3d0*amz**2/amsb(2)**2*csb2*(1-csb2*cf(csb2))*ghbb(2,2)
c      cxt10= -4/3d0*amz**2/amst(1)**2*cst1*(1-cst1*cf(cst1))*ghtt(1,1)
c      cxt20= -4/3d0*amz**2/amst(2)**2*cst2*(1-cst2*cf(cst2))*ghtt(2,2)

c      xfac0 = cdabs(cat0+cab0+cac0+cal+caw+cah+cax1+cax2
c    .       +  cxel+cxer+cxul0+cxur0+cxdl0+cxdr0
c    .       +  cxb10+cxb20+cxt10+cxt20+cxl1+cxl2)**2
c      xfac0 = cdabs(cat0+cab0+cac0+cal+caw+cah+cax1+cax2
c    .       +  cxel+cxer+cxul+cxur+cxdl+cxdr
c    .       +  cxb1+cxb2+cxt1+cxt2+cxl1+cxl2)**2
c      xchgaga = xfac/xfac0
c      write(6,*)'hgaga: ',amh,xchgaga,hga,hga*xchgaga
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      cat0 = cat/ght
c      cab0 = cab/ghb
c      cac0 = cac/ght
c      cal0 = cal/ghb
c      caw0 = caw/ghvv
c      xfac0 = cdabs(cat0+cab0+cac0+cal0+caw0)**2
c      xchgaga = xfac/xfac0
c      write(6,*)'hgaga: ',amh,xchgaga
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c       print*,'h -> gamgam',hga
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      xfacq = cdabs(cat+cab+cac+cal+caw+cah)**2
c      xfacs = cdabs(cat+cab+cac+cal+caw+cah+cax1+cax2
c    .      +  cxl1+cxl2)**2
c      xfacsq = cdabs(cat+cab+cac+cal+caw+cah+cax1+cax2
c    .      +  cxb1+cxb2+cxt1+cxt2+cxl1+cxl2)**2
c      hga0 = hga*xfacsq/xfac
c      cac0 = 4/3d0 * 2*ctc*(1+(1-ctc)*cf(ctc))*ght
c      cat0 = 4/3d0 * 2*ctt*(1+(1-ctt)*cf(ctt))*ght
c      cab0 = 1/3d0 * 2*ctb*(1+(1-ctb)*cf(ctb))*ghb
c      cxb10= -1/3d0*amz**2/amsb(1)**2*csb1*(1-csb1*cf(csb1))*ghbb(1,1)
c      cxb20= -1/3d0*amz**2/amsb(2)**2*csb2*(1-csb2*cf(csb2))*ghbb(2,2)
c      cxt10= -4/3d0*amz**2/amst(1)**2*cst1*(1-cst1*cf(cst1))*ghtt(1,1)
c      cxt20= -4/3d0*amz**2/amst(2)**2*cst2*(1-cst2*cf(cst2))*ghtt(2,2)
c      xfacloq = cdabs(cat0+cab0+cac0+cal+caw+cah)**2
c      cxul0=-2*4.d0/3.d0*(1.d0/2.d0-2.d0/3.d0*ss)
c    .      *amz**2/amsu(1)**2*dcos(a+b)*csul*(1-csul*cf(csul))
c      cxur0=-2*4.d0/3.d0*(2.d0/3.d0*ss)
c    .      *amz**2/amsu(2)**2*dcos(a+b)*csur*(1-csur*cf(csur))
c      cxdl0=-2/3.d0*(-1.d0/2.d0+1.d0/3.d0*ss)
c    .      *amz**2/amsd(1)**2*dcos(a+b)*csdl*(1-csdl*cf(csdl))
c      cxdr0=-2/3.d0*(-1.d0/3.d0*ss)
c    .      *amz**2/amsd(2)**2*dcos(a+b)*csdr*(1-csdr*cf(csdr))
c      xfaclo = cdabs(cat0+cab0+cac0+cal+caw+cah+cax1+cax2
c    .      +  cxel+cxer+cxul0+cxur0+cxdl0+cxdr0
c    .      +  cxb10+cxb20+cxt10+cxt20+cxl1+cxl2)**2
c      csq = 1+3*alphas_hdec(amh,3)
c      xfacsql = cdabs(cat+cab+cac+cal+caw+cah+cax1+cax2
c    .      +  cxel+cxer+(cxul0+cxur0+cxdl0+cxdr0
c    .      +  cxb10+cxb20+cxt10+cxt20)*csq+cxl1+cxl2)**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c  h ---> z gamma
      xrmc = runm_hdec(amh/2,4)*amc/runm_hdec(amc,4)
      xrmb = runm_hdec(amh/2,5)*amb/runm_hdec(amb,5)
      xrmt = runm_hdec(amh/2,6)*amt/runm_hdec(amt,6)
c     print*,'xrmc,xrmb,xrmt ',xrmc,xrmb,xrmt
      if(amh.le.amz)then
       hzga=0
      else
       ts = ss/cs
       ft = -3*2d0/3*(1-4*2d0/3*ss)/dsqrt(ss*cs)*ght
       fb = 3*1d0/3*(-1+4*1d0/3*ss)/dsqrt(ss*cs)*ghb
       fc = -3*2d0/3*(1-4*2d0/3*ss)/dsqrt(ss*cs)*ght
       fl = (-1+4*ss)/dsqrt(ss*cs)*ghb
       if(i2hdm.eq.1) then
          fl = (-1+4*ss)/dsqrt(ss*cs)*ghlep
       endif
       eps=1.d-8
c      ctt = 4*xrmt**2/amh**2*dcmplx(1d0,-eps)
c      ctb = 4*xrmb**2/amh**2*dcmplx(1d0,-eps)
c      ctc = 4*xrmc**2/amh**2*dcmplx(1d0,-eps)
       ctt = 4*amt**2/amh**2*dcmplx(1d0,-eps)
       ctb = 4*amb**2/amh**2*dcmplx(1d0,-eps)
       ctc = 4*amc**2/amh**2*dcmplx(1d0,-eps)
       ctl = 4*amtau**2/amh**2*dcmplx(1d0,-eps)
       ctw = 4*amw**2/amh**2*dcmplx(1d0,-eps)
       cth = 4*amch**2/amh**2*dcmplx(1d0,-eps)
c      clt = 4*xrmt**2/amz**2*dcmplx(1d0,-eps)
c      clb = 4*xrmb**2/amz**2*dcmplx(1d0,-eps)
c      clc = 4*xrmc**2/amz**2*dcmplx(1d0,-eps)
       clt = 4*amt**2/amz**2*dcmplx(1d0,-eps)
       clb = 4*amb**2/amz**2*dcmplx(1d0,-eps)
       clc = 4*amc**2/amz**2*dcmplx(1d0,-eps)
       cle = 4*amtau**2/amz**2*dcmplx(1d0,-eps)
       clw = 4*amw**2/amz**2*dcmplx(1d0,-eps)
       clh = 4*amch**2/amz**2*dcmplx(1d0,-eps)
       cat = ft*(ci1(ctt,clt) - ci2(ctt,clt))
       cab = fb*(ci1(ctb,clb) - ci2(ctb,clb))
       cac = fc*(ci1(ctc,clc) - ci2(ctc,clc))
       cal = fl*(ci1(ctl,cle) - ci2(ctl,cle))
       caw = -1/dsqrt(ts)*(4*(3-ts)*ci2(ctw,clw)
     .     + ((1+2/ctw)*ts - (5+2/ctw))*ci1(ctw,clw))*ghvv
       cah = (1-2*ss)/dsqrt(ss*cs)*amz**2/2/amch**2*ci1(cth,clh)*ghpm
       xfac = cdabs(cat+cab+cac+cal+caw+cah)**2
       acoup = dsqrt(2d0)*gf*amz**2*ss*cs/pi**2
       hzga = gf/(4.d0*pi*dsqrt(2.d0))*amh**3*(alph/pi)*acoup/16.d0
     .        *xfac*(1-amz**2/amh**2)**3
      endif

c      print*,'h -> zgam',hzga
c  h ---> w w
      if(ionwz.eq.0)then
       call htovv_hdec(0,amh,amw,gamw,htww)
       hww = 3d0/2d0*gf*amw**4/dsqrt(2d0)/pi/amh**3*htww*ghvv**2
      elseif(ionwz.eq.-1)then
       dld=2d0
       dlu=2d0
       xm1 = 2d0*amw-dld
       xm2 = 2d0*amw+dlu
       if (amh.le.xm1) then
        call htovv_hdec(0,amh,amw,gamw,htww)
        hww = 3d0/2d0*gf*amw**4/dsqrt(2d0)/pi/amh**3*htww*ghvv**2
       elseif (amh.le.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        call htovv_hdec(0,xx(1),amw,gamw,htww)
        yy(1)=3d0/2d0*gf*amw**4/dsqrt(2d0)/pi/xx(1)**3*htww
        call htovv_hdec(0,xx(2),amw,gamw,htww)
        yy(2)=3d0/2d0*gf*amw**4/dsqrt(2d0)/pi/xx(2)**3*htww
        yy(3)=hvv(xx(3),amw**2/xx(3)**2)
        yy(4)=hvv(xx(4),amw**2/xx(4)**2)
        hww = fint_hdec(amh,xx,yy)*ghvv**2
       else
        hww=hvv(amh,amw**2/amh**2)*ghvv**2
       endif
      else
      dld=2d0
      dlu=2d0
      xm1 = 2d0*amw-dld
      xm2 = 2d0*amw+dlu
      if (amh.le.amw) then
       hww=0
      else if (amh.le.xm1) then
       cww=3.d0*gf**2*amw**4/16.d0/pi**3
       hww=hv(amw**2/amh**2)*cww*amh*ghvv**2
      else if (amh.lt.xm2) then
       cww=3.d0*gf**2*amw**4/16.d0/pi**3
       xx(1) = xm1-1d0
       xx(2) = xm1
       xx(3) = xm2
       xx(4) = xm2+1d0
       yy(1)=hv(amw**2/xx(1)**2)*cww*xx(1)
       yy(2)=hv(amw**2/xx(2)**2)*cww*xx(2)
       yy(3)=hvv(xx(3),amw**2/xx(3)**2)
       yy(4)=hvv(xx(4),amw**2/xx(4)**2)
       hww = fint_hdec(amh,xx,yy)*ghvv**2
      else
       hww=hvv(amh,amw**2/amh**2)*ghvv**2
      endif
      endif

c      print*,'h -> ww',hww
c  h ---> z z
      if(ionwz.eq.0)then
       call htovv_hdec(0,amh,amz,gamz,htzz)
       hzz = 3d0/4d0*gf*amz**4/dsqrt(2d0)/pi/amh**3*htzz*ghvv**2
      elseif(ionwz.eq.-1)then
       dld=2d0
       dlu=2d0
       xm1 = 2d0*amz-dld
       xm2 = 2d0*amz+dlu
       if (amh.le.xm1) then
        call htovv_hdec(0,amh,amz,gamz,htzz)
        hzz = 3d0/4d0*gf*amz**4/dsqrt(2d0)/pi/amh**3*htzz*ghvv**2
       elseif (amh.le.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        call htovv_hdec(0,xx(1),amz,gamz,htzz)
        yy(1)=3d0/4d0*gf*amz**4/dsqrt(2d0)/pi/xx(1)**3*htzz
        call htovv_hdec(0,xx(2),amz,gamz,htzz)
        yy(2)=3d0/4d0*gf*amz**4/dsqrt(2d0)/pi/xx(2)**3*htzz
        yy(3)=hvv(xx(3),amz**2/xx(3)**2)/2
        yy(4)=hvv(xx(4),amz**2/xx(4)**2)/2
        hzz = fint_hdec(amh,xx,yy)*ghvv**2
       else
        hzz=hvv(amh,amz**2/amh**2)/2.d0*ghvv**2
       endif
      else
      dld=2d0
      dlu=2d0
      xm1 = 2d0*amz-dld
      xm2 = 2d0*amz+dlu
      if (amh.le.amz) then
       hzz=0
      else if (amh.le.xm1) then
       czz=3.d0*gf**2*amz**4/192.d0/pi**3*(7-40/3.d0*ss+160/9.d0*ss**2)
       hzz=hv(amz**2/amh**2)*czz*amh*ghvv**2
      else if (amh.lt.xm2) then
       czz=3.d0*gf**2*amz**4/192.d0/pi**3*(7-40/3.d0*ss+160/9.d0*ss**2)
       xx(1) = xm1-1d0
       xx(2) = xm1
       xx(3) = xm2
       xx(4) = xm2+1d0
       yy(1)=hv(amz**2/xx(1)**2)*czz*xx(1)
       yy(2)=hv(amz**2/xx(2)**2)*czz*xx(2)
       yy(3)=hvv(xx(3),amz**2/xx(3)**2)/2d0
       yy(4)=hvv(xx(4),amz**2/xx(4)**2)/2d0
       hzz = fint_hdec(amh,xx,yy)*ghvv**2
      else
       hzz=hvv(amh,amz**2/amh**2)/2.d0*ghvv**2
      endif
      endif

c      print*,'h -> zz',hzz
c  h ---> h h
      if(i2hdm.eq.0) then
      if(ionsh.eq.0)then
      if(islhai.eq.0)then
       zzma = amar
       amreal = amh
       ama = 1.d0
       amlow = amh
12345  call twohdmcp_hdec(tgbet)
       if(amlr.lt.0.d0)then
        ama = amar + 1
        goto 12345
       endif
       amlow = amh
       amdel = amreal - amlow
       dld = 0.3d0*(tgbet-1.3d0)
       dld = dmax1(0.1d0,dld)
       dlu=dld
       ama = zzma
       call twohdmcp_hdec(tgbet)
       xm1 = 2*aml-dld
       xm2 = 2*aml+dlu
       if (amh.le.aml) then
        hhh=0
       elseif (amh.lt.xm1) then
        xh=aml**2/amh**2
        xh1=(xh-1.d0)*(2.d0-.5d0*dlog(xh))+(1.d0-5.d0*xh)
     .    *(datan((2.d0*xh-1.d0)/dsqrt(4.d0*xh-1.d0))
     .     -datan(1.d0/dsqrt(4.d0*xh-1.d0)))/dsqrt(4.d0*xh-1.d0)
        xh2=3*gf**2/32.d0/pi**3*amz**4/amh*ghll**2*glb**2*amb**2
        hhh=xh1*xh2
       elseif (amh.lt.xm2) then
        iflon0 = 0
        iflon1 = 0
        zzma=amar
        amacrit = amar
        ama0 = amar
        ama1 = amar
510     ama0 = ama0 - 1
        ama1 = ama1 + 1
        ama = ama0
        call twohdmcp_hdec(tgbet)
        if(amh.lt.2*aml) then
         iflon0 = -1
        else
         iflon0 = 1
        endif
        ama = ama1
        call twohdmcp_hdec(tgbet)
        if(amh.lt.2*aml) then
         iflon1 = -1
        else
         iflon1 = 1
        endif
        if(iflon0*iflon1.ne.-1) goto 510
501     ama = (ama0+ama1)/2
        call twohdmcp_hdec(tgbet)
        if(amh.lt.2*aml) then
         if(iflon0.eq.-1) then
          ama0 = amar
         else
          ama1 = amar
         endif
        else
         if(iflon0.eq.-1) then
          ama1 = amar
         else
          ama0 = amar
         endif
        endif
        amacrit = (ama0+ama1)/2
        del = 1.d-8
        amdel = 2*dabs(ama1-ama0)/(ama1+ama0)
        if(amdel.gt.del) goto 501
       ama = amacrit
       call twohdmcp_hdec(tgbet)
       ym1 = amacrit
       ym2 = amacrit
       ama0 = amacrit
       ama1 = amacrit
       delstep = 1.d0
511    ama0 = ama0 - delstep
       ama1 = ama1 + delstep
       ama = amacrit
       call twohdmcp_hdec(tgbet)
       if(amh.lt.2*aml-dld) then
        iflonc = -1
       else
        iflonc = 1
       endif
       ama = ama0
       call twohdmcp_hdec(tgbet)
       if(amh.lt.2*aml-dld) then
        iflon0 = -1
       else
        iflon0 = 1
       endif
       ama = ama1
       call twohdmcp_hdec(tgbet)
       if(amh.lt.2*aml-dld) then
        iflon1 = -1
       else
        iflon1 = 1
       endif
       if(iflon0*iflonc.ne.-1.and.iflonc*iflon1.ne.-1) goto 511
       if(iflon0*iflonc.eq.-1) then
         ama1 = amacrit
         iflon1 = iflonc
       else
         ama0 = amacrit
         iflon0 = iflonc
       endif
512    ama = (ama0+ama1)/2
       call twohdmcp_hdec(tgbet)
       if(amh.lt.2*aml-dld) then
        if(iflon0.eq.-1) then
         ama0 = amar
        else
         ama1 = amar
        endif
       else
        if(iflon0.eq.-1) then
         ama1 = amar
        else
         ama0 = amar
        endif
       endif
       ym1 = (ama0+ama1)/2
       del = 1.d-8
       amdel = 2*dabs(ama1-ama0)/(ama1+ama0)
       if(amdel.gt.del) goto 512
       ama = ym1
       call twohdmcp_hdec(tgbet)
       ama0 = amacrit
       ama1 = amacrit
       delstep = 1.d0
513    ama0 = ama0 - delstep
       ama1 = ama1 + delstep
       ama = amacrit
       call twohdmcp_hdec(tgbet)
       if(amh.lt.2*aml+dlu) then
        iflonc = -1
       else
        iflonc = 1
       endif
       ama = ama0
       call twohdmcp_hdec(tgbet)
       if(amh.lt.2*aml+dlu) then
        iflon0 = -1
       else
        iflon0 = 1
       endif
       ama = ama1
       call twohdmcp_hdec(tgbet)
       if(amh.lt.2*aml+dlu) then
        iflon1 = -1
       else
        iflon1 = 1
       endif
       if(iflon0*iflonc.ne.-1.and.iflonc*iflon1.ne.-1) goto 513
       if(iflon0*iflonc.eq.-1) then
         ama1 = amacrit
         iflon1 = iflonc
       else
         ama0 = amacrit
         iflon0 = iflonc
       endif
514    ama = (ama0+ama1)/2
       call twohdmcp_hdec(tgbet)
       if(amh.lt.2*aml+dlu) then
        if(iflon0.eq.-1) then
         ama0 = amar
        else
         ama1 = amar
        endif
       else
        if(iflon0.eq.-1) then
         ama1 = amar
        else
         ama0 = amar
        endif
       endif
       ym2 = (ama0+ama1)/2
       del = 1.d-8
       amdel = 2*dabs(ama1-ama0)/(ama1+ama0)
       if(amdel.gt.del) goto 514
       ama = ym2
       call twohdmcp_hdec(tgbet)
       del = 1.d-4
        xx(1) = ym1 - del
        xx(2) = ym1
        xx(3) = ym2
        xx(4) = ym2 + del
        amar = zzma
        do j=1,4
         ama = xx(j)
         call twohdmcp_hdec(tgbet)
         xx(j) = amh
         if(amh.ge.2*aml)then
          yy(j)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(j)
     .          *beta_hdec(aml**2/xx(j)**2)
         elseif(amh.le.aml)then
          yy(j) = 0
         else
          xh=aml**2/xx(j)**2
          xh1=(xh-1.d0)*(2.d0-.5d0*dlog(xh))+(1.d0-5.d0*xh)
     .    *(datan((2.d0*xh-1.d0)/dsqrt(4.d0*xh-1.d0))
     .     -datan(1.d0/dsqrt(4.d0*xh-1.d0)))/dsqrt(4.d0*xh-1.d0)
          xh2=3*gf**2/32.d0/pi**3*amz**4/xx(j)*glb**2*amb**2
          yy(j)=xh1*xh2
         endif
        enddo
        ama = zzma
        call twohdmcp_hdec(tgbet)
        hhh = fint_hdec(amh,xx,yy)*ghll**2
       else
        hhh=gf/16d0/dsqrt(2d0)/pi*amz**4/amh*beta_hdec(aml**2/amh**2)
     .      *ghll**2
        ii0 = 1
        if(imodel.eq.10)then
         ii0 = 0
        endif
        corr = h2hh_hdec(ii0)
c       corr1 = cofhll_hdec(ii0,amh,aml,amt)
c       write(6,*)'h --> hh: ',hhh,corr,corr1
c       write(6,*)'h --> hh: ',imodel,ii0,hhh,corr
        hhh=hhh*(1+corr/2)**2
       endif
      else
       dld=0.1d0
       dlu=0.1d0
       xm1 = 2d0*aml-dld
       xm2 = 2d0*aml+dlu
       if (amh.le.aml) then
        hhh = 0d0
       elseif (amh.le.xm1) then
        xh=aml**2/amh**2
        xh1=(xh-1.d0)*(2.d0-.5d0*dlog(xh))+(1.d0-5.d0*xh)
     .  *(datan((2.d0*xh-1.d0)/dsqrt(4.d0*xh-1.d0))
     .   -datan(1.d0/dsqrt(4.d0*xh-1.d0)))/dsqrt(4.d0*xh-1.d0)
        xh2=3*gf**2/32.d0/pi**3*amz**4/amh*glb**2*amb**2
        hhh=xh1*xh2
       elseif (amh.le.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        xh=aml**2/xx(1)**2
        xh1=(xh-1.d0)*(2.d0-.5d0*dlog(xh))+(1.d0-5.d0*xh)
     .  *(datan((2.d0*xh-1.d0)/dsqrt(4.d0*xh-1.d0))
     .   -datan(1.d0/dsqrt(4.d0*xh-1.d0)))/dsqrt(4.d0*xh-1.d0)
        xh2=3*gf**2/32.d0/pi**3*amz**4/xx(1)*glb**2*amb**2
        yy(1)=xh1*xh2
        xh=aml**2/xx(2)**2
        xh1=(xh-1.d0)*(2.d0-.5d0*dlog(xh))+(1.d0-5.d0*xh)
     .  *(datan((2.d0*xh-1.d0)/dsqrt(4.d0*xh-1.d0))
     .   -datan(1.d0/dsqrt(4.d0*xh-1.d0)))/dsqrt(4.d0*xh-1.d0)
        xh2=3*gf**2/32.d0/pi**3*amz**4/xx(2)*glb**2*amb**2
        yy(2)=xh1*xh2
        yy(3)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(3)
     .        *beta_hdec(aml**2/xx(3)**2)
        yy(4)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(4)
     .        *beta_hdec(aml**2/xx(4)**2)
        hhh = fint_hdec(amh,xx,yy)*ghll**2
       else
        hhh=gf/16d0/dsqrt(2d0)/pi*amz**4/amh*beta_hdec(aml**2/amh**2)
     .      *ghll**2
       endif
      endif
      else
       if (amh.le.2*aml) then
        hhh=0
       else
        hhh=gf/16d0/dsqrt(2d0)/pi*amz**4/amh*beta_hdec(aml**2/amh**2)
     .      *ghll**2
       endif
      endif
      endif

c mmm changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0) then
            dld=0.1d0
            dlu=0.1d0
            xm1 = 2d0*aml-dld
            xm2 = 2d0*aml+dlu
            if (amh.le.aml) then
               hhh = 0d0
            elseif (amh.le.xm1) then
               xh=aml**2/amh**2
               xh1=(xh-1.d0)*(2.d0-.5d0*dlog(xh))+(1.d0-5.d0*xh)
     .              *(datan((2.d0*xh-1.d0)/dsqrt(4.d0*xh-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xh-1.d0)))
     .              /dsqrt(4.d0*xh-1.d0)
               xh2=3*gf**2/32.d0/pi**3*amz**4/amh*glb**2*amb**2
               hhh=xh1*xh2
            elseif (amh.le.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               xh=aml**2/xx(1)**2
               xh1=(xh-1.d0)*(2.d0-.5d0*dlog(xh))+(1.d0-5.d0*xh)
     .              *(datan((2.d0*xh-1.d0)/dsqrt(4.d0*xh-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xh-1.d0)))
     .              /dsqrt(4.d0*xh-1.d0)
               xh2=3*gf**2/32.d0/pi**3*amz**4/xx(1)*glb**2*amb**2
               yy(1)=xh1*xh2
               xh=aml**2/xx(2)**2
               xh1=(xh-1.d0)*(2.d0-.5d0*dlog(xh))+(1.d0-5.d0*xh)
     .              *(datan((2.d0*xh-1.d0)/dsqrt(4.d0*xh-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xh-1.d0)))
     .              /dsqrt(4.d0*xh-1.d0)
               xh2=3*gf**2/32.d0/pi**3*amz**4/xx(2)*glb**2*amb**2
               yy(2)=xh1*xh2
               yy(3)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(3)
     .              *beta_hdec(aml**2/xx(3)**2)
               yy(4)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(4)
     .              *beta_hdec(aml**2/xx(4)**2)
               hhh = fint_hdec(amh,xx,yy)*ghll**2
            else
               hhh=gf/16d0/dsqrt(2d0)/pi*amz**4/amh*
     .              beta_hdec(aml**2/amh**2)*ghll**2
            endif
         else
            if (amh.le.2*aml) then
               hhh=0
            else
               hhh=gf/16d0/dsqrt(2d0)/pi*amz**4/amh*
     .              beta_hdec(aml**2/amh**2)*ghll**2
            endif
         endif
      endif
c end mmm changed 22/8/2013

c      print*,'h -> hh',hhh,ghll

c  h ---> a a
      if(i2hdm.eq.0) then
      if(ionsh.eq.0)then
      if(islhai.eq.0)then
       dld = 0.3d0*(tgbet-1.3d0)
       dld = dmax1(0.1d0,dld)
       dlu=dld
       ald = dld/2
       alu = dlu/2
       xm1 = 2*ama-dld
       xm2 = 2*ama+dlu
       if (amh.le.ama) then
        haa=0
       elseif (amh.lt.xm1) then
        xa=ama**2/amh**2
        xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .    *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .     -datan(1.d0/dsqrt(4.d0*xa-1.d0)))/dsqrt(4.d0*xa-1.d0)
        xa2=3*gf**2/32.d0/pi**3*amz**4/amh*ghaa**2*gab**2*amb**2
        haa=xa1*xa2
       elseif (amh.lt.xm2) then
        zzma=amar
        amacrit = amar
        ama0 = 10.d0
        ama1 = amar + 50.d0
        ama = ama0
        call twohdmcp_hdec(tgbet)
        if(amh.lt.2*ama) then
         iflon0 = -1
        elseif(amh.eq.2*ama) then
         iflon0 = 0
         amacrit = amar
        else
         iflon0 = 1
        endif
        ama = ama1
        call twohdmcp_hdec(tgbet)
        if(amh.lt.2*ama) then
         iflon1 = -1
        elseif(amh.eq.2*ama) then
         iflon1 = 0
         amacrit = amar
        else
         iflon1 = 1
        endif
        if(iflon0*iflon1.eq.0)then
         iflon0 = 0
         iflon1 = 0
        endif
        if(iflon0.ne.iflon1)then
502      ama = (ama0+ama1)/2
         call twohdmcp_hdec(tgbet)
         if(amh.lt.2*ama) then
          if(iflon0.eq.-1) then
           ama0 = amar
          else
           ama1 = amar
          endif
         elseif(amh.eq.2*ama) then
          iflon0 = 0
          iflon1 = 0
          amacrit = amar
         else
          if(iflon0.eq.-1) then
           ama1 = amar
          else
           ama0 = amar
          endif
         endif
         if(iflon0.ne.0)then
          amacrit = (ama0+ama1)/2
          del = 1.d-8
          amdel = 2*dabs(ama1-ama0)/(ama1+ama0)
          if(amdel.gt.del) goto 502
         endif
        endif
        del = 1.d-4
        xx(1) = amacrit - ald - del
        xx(2) = amacrit - ald
        xx(3) = amacrit + alu
        xx(4) = amacrit + alu + del
        do j=1,4
         ama = xx(j)
         call twohdmcp_hdec(tgbet)
         xx(j) = amh
         if(amh.ge.2*ama)then
          yy(j)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(j)
     .          *beta_hdec(ama**2/xx(j)**2)
         elseif(amh.le.ama)then
          yy(j) = 0
         else
          xa=ama**2/xx(j)**2
          xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .    *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .     -datan(1.d0/dsqrt(4.d0*xa-1.d0)))/dsqrt(4.d0*xa-1.d0)
          xa2=3*gf**2/32.d0/pi**3*amz**4/xx(j)*gab**2*amb**2
          yy(j)=xa1*xa2
         endif
        enddo
        ama = zzma
        call twohdmcp_hdec(tgbet)
        haa = fint_hdec(amh,xx,yy)*ghaa**2
       else
        haa=gf/16d0/dsqrt(2d0)/pi*amz**4/amh*beta_hdec(ama**2/amh**2)
     .       *ghaa**2
       endif
      else
       dld=0.1d0
       dlu=0.1d0
       xm1 = 2d0*ama-dld
       xm2 = 2d0*ama+dlu
       if (amh.le.ama) then
        haa = 0d0
       elseif (amh.le.xm1) then
        xa=ama**2/amh**2
        xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .    *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .     -datan(1.d0/dsqrt(4.d0*xa-1.d0)))/dsqrt(4.d0*xa-1.d0)
        xa2=3*gf**2/32.d0/pi**3*amz**4/amh*ghaa**2*gab**2*amb**2
        haa=xa1*xa2
       elseif (amh.le.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        xa=ama**2/xx(1)**2
        xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .  *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .   -datan(1.d0/dsqrt(4.d0*xa-1.d0)))/dsqrt(4.d0*xa-1.d0)
        xa2=3*gf**2/32.d0/pi**3*amz**4/xx(1)*gab**2*amb**2
        yy(1)=xa1*xa2
        xa=ama**2/xx(2)**2
        xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .  *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .   -datan(1.d0/dsqrt(4.d0*xa-1.d0)))/dsqrt(4.d0*xa-1.d0)
        xa2=3*gf**2/32.d0/pi**3*amz**4/xx(2)*gab**2*amb**2
        yy(2)=xa1*xa2
        yy(3)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(3)
     .        *beta_hdec(ama**2/xx(3)**2)
        yy(4)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(4)
     .        *beta_hdec(ama**2/xx(4)**2)
        haa = fint_hdec(amh,xx,yy)*ghaa**2
       else
        haa=gf/16d0/dsqrt(2d0)/pi*amz**4/amh*beta_hdec(ama**2/amh**2)
     .       *ghaa**2
       endif
      endif
      else
       if (amh.le.2*ama) then
        haa=0
       else
        haa=gf/16d0/dsqrt(2d0)/pi*amz**4/amh*beta_hdec(ama**2/amh**2)
     .       *ghaa**2
       endif
      endif
      endif

c mmm changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0) then
            dld=0.1d0
            dlu=0.1d0
            xm1 = 2d0*ama-dld
            xm2 = 2d0*ama+dlu
            if (amh.le.ama) then
               haa = 0d0
            elseif (amh.le.xm1) then
               xa=ama**2/amh**2
               xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .              *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xa-1.d0)))
     .              /dsqrt(4.d0*xa-1.d0)
               xa2=3*gf**2/32.d0/pi**3*amz**4/amh*ghaa**2*gab**2*amb**2
               haa=xa1*xa2
            elseif (amh.le.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               xa=ama**2/xx(1)**2
               xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .              *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xa-1.d0)))
     .              /dsqrt(4.d0*xa-1.d0)
               xa2=3*gf**2/32.d0/pi**3*amz**4/xx(1)*gab**2*amb**2
               yy(1)=xa1*xa2
               xa=ama**2/xx(2)**2
               xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .              *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xa-1.d0)))
     .              /dsqrt(4.d0*xa-1.d0)
               xa2=3*gf**2/32.d0/pi**3*amz**4/xx(2)*gab**2*amb**2
               yy(2)=xa1*xa2
               yy(3)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(3)
     .              *beta_hdec(ama**2/xx(3)**2)
               yy(4)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(4)
     .              *beta_hdec(ama**2/xx(4)**2)
               haa = fint_hdec(amh,xx,yy)*ghaa**2
            else
               haa=gf/16d0/dsqrt(2d0)/pi*amz**4/amh*
     .              beta_hdec(ama**2/amh**2)*ghaa**2
            endif
         else
            if (amh.le.2*ama) then
               haa=0
            else
               haa=gf/16d0/dsqrt(2d0)/pi*amz**4/amh*
     .              beta_hdec(ama**2/amh**2)*ghaa**2
            endif
         endif
      endif
c end mmm changed 22/8/2013

c      print*,'h -> aa',haa,ghaa

c  h ---> h+ h-

      if(i2hdm.eq.1) then
         if (amh.le.2*amch) then
            hchch=0.d0
         else
            hchch=gf/8d0/dsqrt(2d0)/pi*amz**4/amh*
     .           beta_hdec(amch**2/amh**2)*ghpm**2
         endif
      elseif(i2hdm.eq.0) then
         hchch=0.d0
      endif

c      print*,'h -> h+h-',hchch,ghpm

c  h ---> a z
      if(i2hdm.eq.0)then
      if(ionsh.eq.0)then
      if(islhai.eq.0)then
       dld=1d0
       dlu=8d0
       xm1 = ama+amz-dld
       xm2 = ama+amz+dlu
       if (amh.lt.ama) then
        haz=0
       elseif (amh.lt.xm1) then
        if(amh.le.dabs(amz-ama))then
         haz=0
        else
        haz=9.d0*gf**2/8.d0/pi**3*amz**4*amh*gzah**2*
     .      (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .      *hvh((ama/amh)**2,(amz/amh)**2)
        endif
       elseif (amh.lt.xm2) then
        zzma=amar
165     ama = amar - 1.d0
        call twohdmcp_hdec(tgbet)
        if(amh.lt.ama+amz+dlu.and.amh.gt.ama+amz-dld) goto 165
        xx(1) = amar-1d0
        xx(2) = amar
        ama = zzma
        call twohdmcp_hdec(tgbet)
166     ama = amar + 1.d0
        call twohdmcp_hdec(tgbet)
        if(amh.lt.ama+amz+dlu.and.amh.gt.ama+amz-dld) goto 166
        xx(3) = amar
        xx(4) = amar+1d0
        do ij=1,4
         ama = xx(ij)
         call twohdmcp_hdec(tgbet)
         xx(ij) = amh
         if(amh.le.ama+amz) then
          yy(ij)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(ij)*
     .          (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .          *hvh((ama/xx(ij))**2,(amz/xx(ij))**2)
         else
          caz=lamb_hdec(ama**2/xx(ij)**2,amz**2/xx(ij)**2)
     .       *lamb_hdec(xx(ij)**2/amz**2,ama**2/amz**2)**2
          yy(ij)=gf/8.d0/dsqrt(2d0)/pi*amz**4/xx(ij)*caz
         endif
        enddo
        ama = zzma
        call twohdmcp_hdec(tgbet)
        haz = fint_hdec(amh,xx,yy)*gzah**2
       else
        caz=lamb_hdec(ama**2/amh**2,amz**2/amh**2)
     .     *lamb_hdec(amh**2/amz**2,ama**2/amz**2)**2
        haz=gf/8.d0/dsqrt(2d0)/pi*amz**4/amh*caz*gzah**2
       endif
      else
       dld=1d0
       dlu=8d0
       xm1 = ama+amz-dld
       xm2 = ama+amz+dlu
       if (amh.lt.ama) then
        haz=0
       elseif (amh.lt.xm1) then
        if(amh.le.dabs(amz-ama))then
         haz=0
        else
        haz=9.d0*gf**2/8.d0/pi**3*amz**4*amh*gzah**2*
     .      (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .      *hvh((ama/amh)**2,(amz/amh)**2)
        endif
       elseif (amh.lt.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        yy(1)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(1)*
     .        (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .        *hvh((ama/xx(1))**2,(amz/xx(1))**2)
        yy(2)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(2)*
     .        (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .        *hvh((ama/xx(2))**2,(amz/xx(2))**2)
        caz=lamb_hdec(ama**2/xx(3)**2,amz**2/xx(3)**2)
     .     *lamb_hdec(xx(3)**2/amz**2,ama**2/amz**2)**2
        yy(3)=gf/8.d0/dsqrt(2d0)/pi*amz**4/xx(3)*caz
        caz=lamb_hdec(ama**2/xx(4)**2,amz**2/xx(4)**2)
     .     *lamb_hdec(xx(4)**2/amz**2,ama**2/amz**2)**2
        yy(4)=gf/8.d0/dsqrt(2d0)/pi*amz**4/xx(4)*caz
        haz = fint_hdec(amh,xx,yy)*gzah**2
       else
        caz=lamb_hdec(ama**2/amh**2,amz**2/amh**2)
     .     *lamb_hdec(amh**2/amz**2,ama**2/amz**2)**2
        haz=gf/8.d0/dsqrt(2d0)/pi*amz**4/amh*caz*gzah**2
       endif
      endif
      else
       if (amh.lt.amz+ama) then
        haz=0
       else
        caz=lamb_hdec(ama**2/amh**2,amz**2/amh**2)
     .     *lamb_hdec(amh**2/amz**2,ama**2/amz**2)**2
        haz=gf/8.d0/dsqrt(2d0)/pi*amz**4/amh*caz*gzah**2
       endif
      endif
      endif

c mmm changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0) then
            dld=1d0
            dlu=8d0
            xm1 = ama+amz-dld
            xm2 = ama+amz+dlu
            if (amh.lt.ama) then
               haz=0
            elseif (amh.lt.xm1) then
               if(amh.le.dabs(amz-ama))then
                  haz=0
               else
                  haz=9.d0*gf**2/8.d0/pi**3*amz**4*amh*gzah**2*
     .                 (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .                 *hvh((ama/amh)**2,(amz/amh)**2)
               endif
            elseif (amh.lt.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               yy(1)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(1)*
     .              (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .              *hvh((ama/xx(1))**2,(amz/xx(1))**2)
               yy(2)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(2)*
     .              (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .              *hvh((ama/xx(2))**2,(amz/xx(2))**2)
               caz=lamb_hdec(ama**2/xx(3)**2,amz**2/xx(3)**2)
     .              *lamb_hdec(xx(3)**2/amz**2,ama**2/amz**2)**2
               yy(3)=gf/8.d0/dsqrt(2d0)/pi*amz**4/xx(3)*caz
               caz=lamb_hdec(ama**2/xx(4)**2,amz**2/xx(4)**2)
     .              *lamb_hdec(xx(4)**2/amz**2,ama**2/amz**2)**2
               yy(4)=gf/8.d0/dsqrt(2d0)/pi*amz**4/xx(4)*caz
               haz = fint_hdec(amh,xx,yy)*gzah**2
            else
               caz=lamb_hdec(ama**2/amh**2,amz**2/amh**2)
     .              *lamb_hdec(amh**2/amz**2,ama**2/amz**2)**2
               haz=gf/8.d0/dsqrt(2d0)/pi*amz**4/amh*caz*gzah**2
            endif
         else
            if (amh.lt.amz+ama) then
               haz=0
            else
               caz=lamb_hdec(ama**2/amh**2,amz**2/amh**2)
     .              *lamb_hdec(amh**2/amz**2,ama**2/amz**2)**2
               haz=gf/8.d0/dsqrt(2d0)/pi*amz**4/amh*caz*gzah**2
            endif
         endif
      endif
c end mmm changed 22/8/2013

c      print*,'h -> az',haz

c  h ---> h+ w-
      if(i2hdm.eq.0)then
      if(ionsh.eq.0)then
      if(islhai.eq.0)then
       dld=3d0
       dlu=9d0
       xm1 = amch+amw-dld
       xm2 = amch+amw+dlu
       if (amh.lt.amch) then
        hhw=0.d0
       elseif (amh.lt.xm1) then
        if(amh.le.dabs(amw-amch))then
         hhw=0
        else
        hhw=9.d0*gf**2/16.d0/pi**3*amw**4*amh*glvv**2*2
     .      *hvh((amch/amh)**2,(amw/amh)**2)
        endif
       elseif (amh.lt.xm2) then
        zzma=amar
167     ama = amar - 1.d0
        call twohdmcp_hdec(tgbet)
        if(amh.lt.amch+amw+dlu) goto 167
        xx(1) = amar-1d0
        xx(2) = amar
        ama = zzma
        call twohdmcp_hdec(tgbet)
168     ama = amar + 1.d0
        call twohdmcp_hdec(tgbet)
        if(amh.gt.amch+amw-dld) goto 168
        xx(3) = amar
        xx(4) = amar+1d0
        ama = xx(1)
        call twohdmcp_hdec(tgbet)
        xx(1) = amh
        chw=lamb_hdec(amch**2/xx(1)**2,amw**2/xx(1)**2)
     .     *lamb_hdec(xx(1)**2/amw**2,amch**2/amw**2)**2
        yy(1)=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(1)*chw
        ama = xx(2)
        call twohdmcp_hdec(tgbet)
        xx(2) = amh
        chw=lamb_hdec(amch**2/xx(2)**2,amw**2/xx(2)**2)
     .     *lamb_hdec(xx(2)**2/amw**2,amch**2/amw**2)**2
        yy(2)=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(2)*chw
        ama = xx(3)
        call twohdmcp_hdec(tgbet)
        xx(3) = amh
        yy(3)=9.d0*gf**2/16.d0/pi**3*amw**4*xx(3)*2
     .       *hvh((amch/xx(3))**2,(amw/xx(3))**2)
        ama = xx(4)
        call twohdmcp_hdec(tgbet)
        xx(4) = amh
        yy(4)=9.d0*gf**2/16.d0/pi**3*amw**4*xx(4)*2
     .       *hvh((amch/xx(4))**2,(amw/xx(4))**2)
        ama = zzma
        call twohdmcp_hdec(tgbet)
        hhw=fint_hdec(amh,xx,yy)*glvv**2
       else
        chw=lamb_hdec(amch**2/amh**2,amw**2/amh**2)
     .     *lamb_hdec(amh**2/amw**2,amch**2/amw**2)**2
        hhw=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/amh*chw*glvv**2
       endif
      else
       dld=3d0
       dlu=9d0
       xm1 = amch+amw-dld
       xm2 = amch+amw+dlu
       if (amh.lt.amch) then
        hhw=0.d0
       elseif (amh.lt.xm1) then
        if(amh.le.dabs(amw-amch))then
         hhw=0
        else
        hhw=9.d0*gf**2/16.d0/pi**3*amw**4*amh*glvv**2*2
     .      *hvh((amch/amh)**2,(amw/amh)**2)
        endif
       elseif (amh.lt.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        yy(1)=9.d0*gf**2/16.d0/pi**3*amw**4*xx(1)*2
     .       *hvh((amch/xx(1))**2,(amw/xx(1))**2)
        yy(2)=9.d0*gf**2/16.d0/pi**3*amw**4*xx(2)*2
     .       *hvh((amch/xx(2))**2,(amw/xx(2))**2)
        chw=lamb_hdec(amch**2/xx(3)**2,amw**2/xx(3)**2)
     .     *lamb_hdec(xx(3)**2/amw**2,amch**2/amw**2)**2
        yy(3)=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(3)*chw
        chw=lamb_hdec(amch**2/xx(4)**2,amw**2/xx(4)**2)
     .     *lamb_hdec(xx(4)**2/amw**2,amch**2/amw**2)**2
        yy(4)=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(4)*chw
        hhw=fint_hdec(amh,xx,yy)*glvv**2
       else
        chw=lamb_hdec(amch**2/amh**2,amw**2/amh**2)
     .     *lamb_hdec(amh**2/amw**2,amch**2/amw**2)**2
        hhw=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/amh*chw*glvv**2
       endif
      endif
      else
       if (amh.lt.amw+amch) then
        hhw=0.d0
       else
        chw=lamb_hdec(amch**2/amh**2,amw**2/amh**2)
     .     *lamb_hdec(amh**2/amw**2,amch**2/amw**2)**2
        hhw=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/amh*chw*glvv**2
       endif
      endif
      endif

c mmm changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0) then
            dld=3d0
            dlu=9d0
            xm1 = amch+amw-dld
            xm2 = amch+amw+dlu
            if (amh.lt.amch) then
               hhw=0.d0
            elseif (amh.lt.xm1) then
               if(amh.le.dabs(amw-amch))then
                  hhw=0
               else
                  hhw=9.d0*gf**2/16.d0/pi**3*amw**4*amh*glvv**2*2
     .                 *hvh((amch/amh)**2,(amw/amh)**2)
               endif
            elseif (amh.lt.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               yy(1)=9.d0*gf**2/16.d0/pi**3*amw**4*xx(1)*2
     .              *hvh((amch/xx(1))**2,(amw/xx(1))**2)
               yy(2)=9.d0*gf**2/16.d0/pi**3*amw**4*xx(2)*2
     .              *hvh((amch/xx(2))**2,(amw/xx(2))**2)
               chw=lamb_hdec(amch**2/xx(3)**2,amw**2/xx(3)**2)
     .              *lamb_hdec(xx(3)**2/amw**2,amch**2/amw**2)**2
               yy(3)=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(3)*chw
               chw=lamb_hdec(amch**2/xx(4)**2,amw**2/xx(4)**2)
     .              *lamb_hdec(xx(4)**2/amw**2,amch**2/amw**2)**2
               yy(4)=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(4)*chw
               hhw=fint_hdec(amh,xx,yy)*glvv**2
            else
               chw=lamb_hdec(amch**2/amh**2,amw**2/amh**2)
     .              *lamb_hdec(amh**2/amw**2,amch**2/amw**2)**2
               hhw=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/amh*chw*glvv**2
            endif
         else
            if (amh.lt.amw+amch) then
               hhw=0.d0
            else
               chw=lamb_hdec(amch**2/amh**2,amw**2/amh**2)
     .              *lamb_hdec(amh**2/amw**2,amch**2/amw**2)**2
               hhw=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/amh*chw*glvv**2
            endif
         endif
      endif
c end mmm changed 22/8/2013

c      print*,'h -> h+w-',hhw,hhw/2.d0

c ========================== susy decays
c
      if(iofsusy.eq.0) then
c  hh ----> charginos
      do 741 i=1,2
      do 741 j=1,2
      if (amh.gt.amchar(i)+amchar(j)) then
      whhch(i,j)=gf*amw**2/(2*pi*dsqrt(2.d0))/amh
     .     *lamb_hdec(amchar(i)**2/amh**2,amchar(j)**2/amh**2)
     .     *( (ac1(i,j)**2+ac1(j,i)**2)*(amh**2-amchar(i)
     .         **2-amchar(j)**2)-4.d0*ac1(i,j)*ac1(j,i)*
     .         xmchar(i)*xmchar(j) )
      else
      whhch(i,j)=0.d0
      endif
 741  continue
      whhcht=whhch(1,1)+whhch(1,2)+whhch(2,1)+whhch(2,2)
c
c  hh ----> neutralinos
      do 742 i=1,4
      do 742 j=1,4
      if (amh.gt.amneut(i)+amneut(j)) then
      whhne(i,j)=gf*amw**2/(2*pi*dsqrt(2.d0))/amh
     .         *an1(i,j)**2*(amh**2-(xmneut(i)+xmneut(j))**2)
     .         *lamb_hdec(amneut(i)**2/amh**2,amneut(j)**2/amh**2)
      else
      whhne(i,j)=0.d0
      endif
 742  continue
      whhnet= whhne(1,1)+whhne(1,2)+whhne(1,3)+whhne(1,4)
     .       +whhne(2,1)+whhne(2,2)+whhne(2,3)+whhne(2,4)
     .       +whhne(3,1)+whhne(3,2)+whhne(3,3)+whhne(3,4)
     .       +whhne(4,1)+whhne(4,2)+whhne(4,3)+whhne(4,4)
c
c  hh ----> sleptons
c
      if (amh.gt.2.d0*amse(1)) then
      whhslel=2*gf/2.d0/dsqrt(2d0)/pi*amz**4/amh*dcos(b+a)**2
     .      *beta_hdec(amse(1)**2/amh**2)*(-0.5d0+ss)**2
      else
      whhslel=0.d0
      endif

      if (amh.gt.2.d0*amse(2)) then
      whhsler=2*gf/2.d0/dsqrt(2d0)/pi*amz**4/amh*dcos(b+a)**2
     .      *beta_hdec(amse(2)**2/amh**2)*ss**2
      else
      whhsler=0.d0
      endif

      whhslnl=0.d0
      if (amh.gt.2.d0*amsn1(1)) then
      whhslnl=2*gf/2.d0/dsqrt(2d0)/pi*amz**4/amh*dcos(b+a)**2
     .      *beta_hdec(amsn1(1)**2/amh**2)*0.5d0**2
      endif
      if (amh.gt.2.d0*amsn(1)) then
      whhslnl=whhslnl + gf/2.d0/dsqrt(2d0)/pi*amz**4/amh*dcos(b+a)**2
     .      *beta_hdec(amsn(1)**2/amh**2)*0.5d0**2
      endif

      do 748 i=1,2
      do 748 j=1,2
      if(amh.gt.amsl(i)+amsl(j)) then
      whhstau(i,j)=gf*amz**4/2.d0/dsqrt(2.d0)/pi*ghee(i,j)**2*
     .      lamb_hdec(amsl(i)**2/amh**2,amsl(j)**2/amh**2)/amh
      else
      whhstau(i,j)=0.d0
      endif
 748  continue

      whhslt=whhstau(1,1)+whhstau(1,2)+whhstau(2,1)+whhstau(2,2)
     .       +whhslel+whhsler+whhslnl
c
c  hh ----> squarks
c
      if (amh.gt.2.d0*amsu(1)) then
      whhsqul=6*gf/2.d0/dsqrt(2d0)/pi*amz**4/amh*dcos(b+a)**2
     .      *beta_hdec(amsu(1)**2/amh**2)*(0.5d0-2.d0/3.d0*ss)**2
      else
      whhsqul=0.d0
      endif

      if (amh.gt.2.d0*amsu(2)) then
      whhsqur=6*gf/2.d0/dsqrt(2d0)/pi*amz**4/amh*dcos(b+a)**2
     .      *beta_hdec(amsu(2)**2/amh**2)*(-2.d0/3.d0*ss)**2
      else
      whhsqur=0.d0
      endif

      if (amh.gt.2.d0*amsd(1)) then
      whhsqdl=6*gf/2.d0/dsqrt(2d0)/pi*amz**4/amh*dcos(b+a)**2
     .      *beta_hdec(amsd(1)**2/amh**2)*(-0.5d0+1.d0/3.d0*ss)**2
      else
      whhsqdl=0.d0
      endif

      if (amh.gt.2.d0*amsd(2)) then
      whhsqdr=6*gf/2.d0/dsqrt(2d0)/pi*amz**4/amh*dcos(b+a)**2
     .      *beta_hdec(amsd(2)**2/amh**2)*(+1.d0/3.d0*ss)**2
      else
      whhsqdr=0.d0
      endif

      whhsq=whhsqul+whhsqur+whhsqdl+whhsqdr
c
c  hh ----> stops
      susy = 1
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     do k=-10,10
c     do k=-1,1
c     do i=1,2
c     do j=1,2
c     qsq = amh*10.d0**(k/10.d0)
c     qsq = amh*2.d0**(k)
c     if(amh.gt.ymst(i)+ymst(j)) then
c      call sqmbapp_hdec(qsq)
c      susy = 1+sqsusy_hdec(2,1,i,j,qsq,0,1)
c      whhst0(i,j)=3*gf*amz**4/2.d0/dsqrt(2.d0)/pi*yhtt(i,j)**2*
c    .       lamb_hdec(ymst(i)**2/amh**2,ymst(j)**2/amh**2)/amh
c      whhst(i,j)=3*gf*amz**4/2.d0/dsqrt(2.d0)/pi*yhtt(i,j)**2*
c    .       lamb_hdec(ymst(i)**2/amh**2,ymst(j)**2/amh**2)/amh
c    .      *susy
c     else
c     whhst(i,j)=0.d0
c     endif
c     enddo
c     enddo
c     write(9,*)'h -> t1 t1: ',qsq/amh,whhst0(1,1),whhst(1,1)
c     write(9,*)'numbers: ',3*gf/2.d0/dsqrt(2.d0)/pi/amh,
c    .yhtt(1,1)**2*amz**4,lamb_hdec(ymst(1)**2/amh**2,ymst(1)**2/amh**2)
c    .,yhtt(1,1)*amz**2
c    .,3*gf/2.d0/dsqrt(2.d0)/pi/amh*
c    .yhtt(1,1)**2*amz**4*lamb_hdec(ymst(1)**2/amh**2,ymst(1)**2/amh**2)
c     write(9,*)'h -> t1 t2: ',qsq/amh,whhst0(1,2),whhst(1,2)
c     write(9,*)'h -> t2 t2: ',qsq/amh,whhst0(2,2),whhst(2,2)
c     write(901,('1x,g10.4,6(1x,g10.4)'))qsq/amh,whhst0(1,1),whhst(1,1),
c    .                    whhst0(1,2),whhst(1,2),whhst0(2,2),whhst(2,2)
c     enddo
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      do 743 i=1,2
      do 743 j=1,2
c     qsq = (ymst(i)+ymst(j))/2
      qsq = amh
      if(amh.gt.ymst(i)+ymst(j)) then
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       call sqmbapp_hdec(qsq)
       susy = 1+sqsusy_hdec(2,1,i,j,qsq,0,1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       whhst(i,j)=3*gf*amz**4/2.d0/dsqrt(2.d0)/pi*yhtt(i,j)**2*
     .       lamb_hdec(ymst(i)**2/amh**2,ymst(j)**2/amh**2)/amh
     .      *susy
c     write(6,*)'h -> stop: ',i,j,amh,ymst(i),ymst(j),100*(susy-1),'% ',
c    .          whhst(i,j)/susy,whhst(i,j)
c      if(i.eq.1.and.j.eq.1)write(511,*)amh,whhst(i,j),whhst(i,j)/susy
c      if(i.eq.1.and.j.eq.2)write(512,*)amh,whhst(i,j),whhst(i,j)/susy
c      if(i.eq.2.and.j.eq.1)write(521,*)amh,whhst(i,j),whhst(i,j)/susy
c      if(i.eq.2.and.j.eq.2)write(522,*)amh,whhst(i,j),whhst(i,j)/susy
c      write(6,*)'h -> stop: ',i,j,amh,ymst(i),ymst(j),susy-1
      else
      whhst(i,j)=0.d0
      endif
 743  continue
c
c  hh ----> sbottoms
      susy = 1
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     do k=-10,10
c     do k=-1,1
c     do i=1,2
c     do j=1,2
c     qsq = amh*10.d0**(k/10.d0)
c     qsq = amh*2.d0**(k)
c     if(amh.gt.ymsb(i)+ymsb(j)) then
c      call sqmbapp_hdec(qsq)
c      susy = 1+sqsusy_hdec(2,2,i,j,qsq,0,1)
c      whhsb0(i,j)=3*gf*amz**4/2.d0/dsqrt(2.d0)/pi*yhbb(i,j)**2*
c    .       lamb_hdec(ymsb(i)**2/amh**2,ymsb(j)**2/amh**2)/amh
c      whhsb(i,j)=3*gf*amz**4/2.d0/dsqrt(2.d0)/pi*yhbb(i,j)**2*
c    .       lamb_hdec(ymsb(i)**2/amh**2,ymsb(j)**2/amh**2)/amh
c    .      *susy
c     else
c      whhsb(i,j)=0.d0
c     endif
c     enddo
c     enddo
c     write(9,*)'h -> b1 b1: ',qsq/amh,whhsb0(1,1),whhsb(1,1)
c     write(9,*)'h -> b1 b2: ',qsq/amh,whhsb0(1,2),whhsb(1,2)
c     write(9,*)'h -> b2 b2: ',qsq/amh,whhsb0(2,2),whhsb(2,2)
c     write(902,('1x,g10.4,6(1x,g10.4)'))qsq/amh,whhsb0(1,1),whhsb(1,1),
c    .        whhsb0(1,2),whhsb(1,2),whhsb0(2,2),whhsb(2,2)
c     enddo
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     dummy0 = 0
c     dummy1 = 0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      do 744 i=1,2
      do 744 j=1,2
c     qsq = (ymsb(i)+ymsb(j))/2
      qsq = amh
      if(amh.gt.ymsb(i)+ymsb(j)) then
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       call sqmbapp_hdec(qsq)
       susy = 1+sqsusy_hdec(2,2,i,j,qsq,0,1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       whhsb(i,j)=3*gf*amz**4/2.d0/dsqrt(2.d0)/pi*yhbb(i,j)**2*
     .       lamb_hdec(ymsb(i)**2/amh**2,ymsb(j)**2/amh**2)/amh
     .      *susy
c     write(6,*)'h -> sbot: ',i,j,amh,ymsb(i),ymsb(j),100*(susy-1),'% ',
c    .          whhsb(i,j)/susy,whhsb(i,j)
c      if(i.eq.1.and.j.eq.1)write(611,*)amh,whhsb(i,j),whhsb(i,j)/susy
c      if(i.eq.1.and.j.eq.2)write(612,*)amh,whhsb(i,j),whhsb(i,j)/susy
c      if(i.eq.2.and.j.eq.1)write(621,*)amh,whhsb(i,j),whhsb(i,j)/susy
c      if(i.eq.2.and.j.eq.2)write(622,*)amh,whhsb(i,j),whhsb(i,j)/susy
c      write(6,*)'h -> sbot: ',i,j,amh,ymsb(i),ymsb(j),susy-1
c      dummy0 = dummy0 + whhsb(i,j)/susy
c      dummy1 = dummy1 + whhsb(i,j)
      else
      whhsb(i,j)=0.d0
      endif
 744  continue
c     write(6,*)'h -> sbot0: ',ymsb(1),ymsb(2),dummy0,dummy1,
c    .                        100*(dummy1/dummy0-1),'%'
c
      whhstt=whhst(1,1)+whhst(1,2)+whhst(2,1)+whhst(2,2)
      whhsbb=whhsb(1,1)+whhsb(1,2)+whhsb(2,1)+whhsb(2,2)
      whhsqt=whhstt+whhsbb+whhsq
c
      else
      whhcht=0.d0
      whhnet=0.d0
      whhslt=0.d0
      whhsqt=0.d0
c--change thanks to elzbieta richter-was
      do i=1,2
       do j=1,2
        whhch(i,j)=0.d0
        whhst(i,j)=0.d0
        whhsb(i,j)=0.d0
        whhstau(i,j)=0.d0
       enddo
      enddo
      do i=1,4
       do j=1,4
        whhne(i,j)=0.d0
       enddo
      enddo
      endif

      if(igold.ne.0)then
c   hh ---> goldstinos
       do 740 i=1,4
       if (amh.gt.amneut(i)) then
        whhgd(i)=amh**5/axmpl**2/axmgd**2/48.d0/pi*
     .           (1.d0-amneut(i)**2/amh**2)**4*agdh(i)**2
       else
        whhgd(i)=0.d0
       endif
 740   continue
       whhgdt=whhgd(1)+whhgd(2)+whhgd(3)+whhgd(4)
      else
       whhgdt=0
      endif
c
c    ==========  total width and branching ratios
      wtot=hll+hmm+hss+hcc+hbb+htt+hgg+hga+hzga+hww+hzz+hhh+haa+haz
     .    +hhw+whhcht+whhnet+whhslt+whhsqt + whhgdt

c     write(6,*)'h: ',wtot
c     write(6,*)'h: ',hll,hmm,hss
c     write(6,*)'h: ',hcc,hbb,htt
c     write(6,*)'h: ',hgg,hga,hzga
c     write(6,*)'h: ',hww,hzz,hhh
c     write(6,*)'h: ',haa,haz,hhw
c     write(6,*)'h: ',whhcht,whhnet,whhslt
c     write(6,*)'h: ',whhsqt , whhgdt
c     write(6,*)

c     print*,'wtot',wtot

c maggie changed 21/10/2013
      wtot=wtot+hchch
      hhbrchch=hchch/wtot
c end maggie changed 21/10/2013

c     print*,'wtot',wtot

      hhbrt=htt/wtot
      hhbrb=hbb/wtot
      hhbrl=hll/wtot
      hhbrm=hmm/wtot
      hhbrs=hss/wtot
      hhbrc=hcc/wtot
      hhbrg=hgg/wtot
      hhbrga=hga/wtot
      hhbrzga=hzga/wtot
      hhbrw=hww/wtot
      hhbrz=hzz/wtot
      hhbrh=hhh/wtot
      hhbra=haa/wtot
      hhbraz=haz/wtot
      hhbrhw=hhw/wtot
      do 841 i=1,2
      do 841 j=1,2
      hhbrsc(i,j)=whhch(i,j)/wtot
841   continue
      do 842 i=1,4
      do 842 j=1,4
      hhbrsn(i,j)=whhne(i,j)/wtot
842   continue
      hhbrcht=whhcht/wtot
      hhbrnet=whhnet/wtot
      hhbrsl=whhslt/wtot
      hhbrsq=whhsq/wtot
      hhbrsqt=whhsqt/wtot
      do 843 i=1,2
      do 843 j=1,2
      hhbrst(i,j)=whhst(i,j)/wtot
843   continue
      do 844 i=1,2
      do 844 j=1,2
      hhbrsb(i,j)=whhsb(i,j)/wtot
844   continue
      hhbrgd =whhgdt/wtot
      hhwdth=wtot

      bhhslnl = whhslnl/wtot
      bhhslel = whhslel/wtot
      bhhsler = whhsler/wtot
      bhhsqul = whhsqul/wtot
      bhhsqur = whhsqur/wtot
      bhhsqdl = whhsqdl/wtot
      bhhsqdr = whhsqdr/wtot
      do i = 1,2
       do j = 1,2
        bhhst(i,j) = whhst(i,j)/wtot
        bhhsb(i,j) = whhsb(i,j)/wtot
        bhhstau(i,j) = whhstau( i,j)/wtot
       enddo
      enddo

      endif

      if(ihiggs.eq.3.or.ihiggs.eq.5)then
c
c        =========================================================
c                       cp odd  higgs decays
c        =========================================================
c     =============  running masses
      rms = runm_hdec(ama,3)
      rmc = runm_hdec(ama,4)
      rmb = runm_hdec(ama,5)
      rmt = runm_hdec(ama,6)
      ratcoup = gat/gab
      higtop = ama**2/amt**2

      ash=alphas_hdec(ama,3)
      amc0=1.d8
      amb0=2.d8
c     amt0=3.d8
      as3=alphas_hdec(ama,3)
      amc0=amc
      as4=alphas_hdec(ama,3)
      amb0=amb
c     amt0=amt

c     =============== partial widths
c  a ---> g g
       eps=1.d-8
       nfext = 3
       asg = as3
       ctt = 4*amt**2/ama**2*dcmplx(1d0,-eps)
       ctb = 4*amb**2/ama**2*dcmplx(1d0,-eps)
       cat = ctt*cf(ctt)*gat
       cab = ctb*cf(ctb)*gab
       ctc = 4*amc**2/ama**2*dcmplx(1d0,-eps)
       cac = ctc*cf(ctc)*gat
       fqcd=aggqcd(asg,nfext)
       xfac = cdabs(cat+cab+cac)**2*fqcd
       hgg=gf/(16.d0*pi*dsqrt(2.d0))*ama**3*(asg/pi)**2*xfac

c      print*
c      print*,'a decay widths'
c      print*,'hgg_nlo',hgg

c  a ---> g g* ---> g cc   to be added to a ---> cc
       nfext = 4
       asg = as4
       fqcd=aggqcd(asg,nfext)
       xfac = cdabs(cat+cab+cac)**2*fqcd
       dcc=gf/(16.d0*pi*dsqrt(2.d0))*ama**3*(asg/pi)**2*xfac
     .     - hgg

c  a ---> g g* ---> g bb   to be added to a ---> bb
       nfext = 5
       asg = ash
       fqcd=aggqcd(asg,nfext)
       xfac = cdabs(cat+cab+cac)**2*fqcd
       dbb=gf/(16.d0*pi*dsqrt(2.d0))*ama**3*(asg/pi)**2*xfac
     .     - hgg - dcc
       hgg=gf/(16.d0*pi*dsqrt(2.d0))*ama**3*(asg/pi)**2*xfac

c  a ---> g g: full nnlo corrections to top loops for nf=5
       fqcd0=aggqcd(asg,5)
       fqcd=aggqcd2(asg,5,ama,amt)
       xfac = cdabs(cat+cab+cac)**2*(fqcd-fqcd0)
       hgg=hgg+gf/(16.d0*pi*dsqrt(2.d0))*ama**3*(asg/pi)**2*xfac

      if(nfgg.eq.3)then
       hgg = hgg - dbb - dcc
      elseif(nfgg.eq.4)then
       hgg = hgg - dbb
       dcc = 0
      else
       dcc = 0
       dbb = 0
      endif

c     write(6,*)ama,fqcd0-1,fqcd-fqcd0
c     print*,'hgg_nnlo',hgg

c  a ---> mu mu
      xglm = glb
      xghm = ghb
      xgam = gab
      if(i2hdm.eq.1) then
         xgam = galep
      endif
      if(iofsusy.eq.0) then
       call staususy_hdec(glb,ghb,gab,xglm,xghm,xgam,qsusy,0)
      endif
      if(ama.le.2*ammuon) then
       hmm = 0
      else
      hmm=aff(ama,(ammuon/ama)**2)*xgam**2
      endif

c     print*,'a -> mumu',hmm

c  a ---> ll
      xglt = glb
      xght = ghb
      xgat = gab
      if(i2hdm.eq.1) then
         xgat = galep
      endif
      if(iofsusy.eq.0) then
       call staususy_hdec(glb,ghb,gab,xglt,xght,xgat,qsusy,1)
      endif
      if(ama.le.2*amtau) then
       hll = 0
      else
      hll=aff(ama,(amtau/ama)**2)*xgat**2
      endif

c     write(6,*)'a: tau/mu: ',hll/hmm*ammuon**2/amtau**2,xgat**2/xgam**2
c     print*,'a -> tautau',hll
c  a --> ss
      xgls = glb
      xghs = ghb
      xgas = gab
      ssusy = (amsd(1)+amsd(2)+amglu)/3*qsusy
      if(iofsusy.eq.0) then
       call strsusy_hdec(glb,ghb,gab,xgls,xghs,xgas,ssusy,loop)
      endif
      if(ama.le.2*ams) then
       hss = 0
      else
       hs1=3.d0*aff(ama,(ams/ama)**2)
     .    *xgas**2
     .    *tqcda(ams**2/ama**2)
       hs2=3.d0*aff(ama,(rms/ama)**2)
     .    *xgas**2
     .    *qcda(rms**2/ama**2)
       if(hs2.lt.0.d0) hs2 = 0
       rat = 2*ams/ama
       hss = qqint_hdec(rat,hs1,hs2)
      endif

c     print*,'a -> ss',hs1,hs2
c  a --> cc
      ratcoup = 1
      if(ama.le.2*amc) then
       hcc = 0
      else
       hc1=3.d0*aff(ama,(amc/ama)**2)
     .    *gat**2
     .    *tqcda(amc**2/ama**2)
       hc2=3.d0*aff(ama,(rmc/ama)**2)
     .    *gat**2
     .    *qcda(rmc**2/ama**2)
     .   + dcc
       if(hc2.lt.0.d0) hc2 = 0
       rat = 2*amc/ama
       hcc = qqint_hdec(rat,hc1,hc2)
      endif

c     print*,'a -> cc',hcc

c  a --> bb :
      qq = amb
      susy = 0
      xgab = gab
      ssusy = (amsb(1)+amsb(2)+amglu)/3*qsusy
      fsusy = susyscale
      as0 = alphas_hdec(fsusy,3)
      if(iofsusy.eq.0) then
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       i0 = 0
       call dmbapp_hdec(i0,dglb,dghb,dgab,fsusy,loop)
       delb1 = -dgab/(1+1/tgbet**2)
       delb0 = delb1/(1-delb1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       i0 = 1
       call dmbapp_hdec(i0,dglb,dghb,dgab,fsusy,loop)
       i0 = 3
       bsc = (amsq+amur+amdr)/3
       xmb = runm_hdec(fsusy,5)/(1+delb0)
c      xmb = amb
c1357
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       if(islhai.ne.0) xmb = amb
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       susy = cofsusy_hdec(i0,amb,xmb,qq)*as0/pi - 2*dgab
       call botsusy_hdec(glb,ghb,gab,xglb,xghb,xgab,ssusy,loop)
      endif
      ratcoup = gat/xgab
      if(ama.le.2*amb) then
       hbb = 0
      else
       hb1=3.d0*aff(ama,(amb/ama)**2)
     .    *(xgab**2+xgab*gab*susy)
     .    *tqcda(amb**2/ama**2)
       hb2=3.d0*aff(ama,(rmb/ama)**2)
     .    *(xgab**2+xgab*gab*susy)
     .    *qcda(rmb**2/ama**2)
     .   + dbb
       if(hb2.lt.0.d0) hb2 = 0
       rat = 2*amb/ama
       hbb = qqint_hdec(rat,hb1,hb2)

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      xxx = cofsusy_hdec(i0,amb,xmb,qq)
c      write(1,*)'a -> bb: ',xxx*as0/pi
c      write(1,*)'a -> bb: ',2*dgab
c      write(1,*)'a -> bb: ',gab,xgab,susy
c      write(6,('a3,4(1x,g15.8)'))'a: ',ama,ama,susy+2*dgab,
c    .                             susy/(susy+2*dgab)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      endif
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)'a -> bb: ',xgab**2,xgab*gab*susy/xgab**2,
c    .                      (xgab**2+xgab*gab*susy)/xgab**2
c     write(6,*)'approx:  ',susy+2*dgab,2*dgab,susy
c     fac = as0/pi
c     write(6,*)'approx2: ',(susy+2*dgab)/fac,2*dgab/fac,susy/fac
c     write(52,*)ama,hbb
c     write(6,*)'a -> bb: ',ama,hbb,qsusy1,loop
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     print*,'a -> bb',hbb
c      print*
c      print*,'a -> bb:',hbb,gab,xgab,gab*susy,2*dgab,susy,2*dgab+susy
c  a --> tt :
      ratcoup = 0
      call topsusy_hdec(glt,ght,gat,xgltop,xghtop,xgatop,scale,1)
c     write(6,*)xgltop,xghtop,xgatop
      if(ionsh.eq.0)then
       dld=3d0
       dlu=4d0
       xm1 = 2d0*amt-dld
       xm2 = 2d0*amt+dlu
       if (ama.le.amt+amw+amb) then
        htt=0.d0
       elseif (ama.le.xm1) then
        factt=6.d0*gf**2*ama**3*amt**2/2.d0/128.d0/pi**3*gat**2
        call atott_hdec(ama,amt,amb,amw,amch,att0,gab,gat)
        htt=factt*att0
       elseif (ama.le.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        factt=6.d0*gf**2*xx(1)**3*amt**2/2.d0/128.d0/pi**3
        call atott_hdec(xx(1),amt,amb,amw,amch,att0,gab,gat)
        yy(1)=factt*att0
        factt=6.d0*gf**2*xx(2)**3*amt**2/2.d0/128.d0/pi**3
        call atott_hdec(xx(2),amt,amb,amw,amch,att0,gab,gat)
        yy(2)=factt*att0
        xmt = runm_hdec(xx(3),6)
        xyz1 =3.d0*aff(xx(3),(amt/xx(3))**2)
     .    *tqcda(amt**2/xx(3)**2)
        xyz2 =3.d0*aff(xx(3),(xmt/xx(3))**2)
     .    *qcda(xmt**2/xx(3)**2)
        if(xyz2.lt.0.d0) xyz2 = 0
        rat = 2*amt/xx(3)
        yy(3) = qqint_hdec(rat,xyz1,xyz2)
        xmt = runm_hdec(xx(4),6)
        xyz1 =3.d0*aff(xx(4),(amt/xx(4))**2)
     .    *tqcda(amt**2/xx(4)**2)
        xyz2 =3.d0*aff(xx(4),(xmt/xx(4))**2)
     .    *qcda(xmt**2/xx(4)**2)
        if(xyz2.lt.0.d0) xyz2 = 0
        rat = 2*amt/xx(4)
        yy(4) = qqint_hdec(rat,xyz1,xyz2)
        htt = fint_hdec(ama,xx,yy)*gat**2
       else
        ht1=3.d0*aff(ama,(amt/ama)**2)*gat**2
     .    *tqcda(amt**2/ama**2)
        ht2=3.d0*aff(ama,(rmt/ama)**2)*gat**2
     .    *qcda(rmt**2/ama**2)
        if(ht2.lt.0.d0) ht2 = 0
        rat = 2*amt/ama
        htt = qqint_hdec(rat,ht1,ht2)
       endif
      else
       if (ama.le.2.d0*amt) then
        htt=0.d0
       else
        ht1=3.d0*aff(ama,(amt/ama)**2)*gat**2
     .    *tqcda(amt**2/ama**2)
        ht2=3.d0*aff(ama,(rmt/ama)**2)*gat**2
     .    *qcda(rmt**2/ama**2)
        if(ht2.lt.0.d0) ht2 = 0
        rat = 2*amt/ama
        htt = qqint_hdec(rat,ht1,ht2)
       endif
      endif

c     print*,'a -> tt',htt

c  a ---> gamma gamma
       eps=1.d-8
       xrmc = runm_hdec(ama/2,4)*amc/runm_hdec(amc,4)
       xrmb = runm_hdec(ama/2,5)*amb/runm_hdec(amb,5)
       xrmt = runm_hdec(ama/2,6)*amt/runm_hdec(amt,6)
       ctt = 4*xrmt**2/ama**2*dcmplx(1d0,-eps)
       ctb = 4*xrmb**2/ama**2*dcmplx(1d0,-eps)
       cat = 4/3d0 * ctt*cf(ctt)*gat
     .     * cfacq_hdec(1,ama,xrmt)
       cab = 1/3d0 * ctb*cf(ctb)*gab
     .     * cfacq_hdec(1,ama,xrmb)
       ctc = 4*xrmc**2/ama**2*dcmplx(1d0,-eps)
       cac = 4/3d0 * ctc*cf(ctc)*gat
     .     * cfacq_hdec(1,ama,xrmc)
       ctl = 4*amtau**2/ama**2*dcmplx(1d0,-eps)
       cal = 1.d0  * ctl*cf(ctl)*gab
       if(i2hdm.eq.1) then
          cal = 1.d0  * ctl*cf(ctl)*galep
       endif
       if(iofsusy.eq.0) then
        cx1 = 4*amchar(1)**2/ama**2*dcmplx(1d0,-eps)
        cx2 = 4*amchar(2)**2/ama**2*dcmplx(1d0,-eps)
        cax1= amw/xmchar(1) * cx1*cf(cx1) * 2*ac3(1,1)
        cax2= amw/xmchar(2) * cx2*cf(cx2) * 2*ac3(2,2)
        xfac = cdabs(cat+cab+cac+cal+cax1+cax2)**2
       else
        xfac = cdabs(cat+cab+cac+cal)**2
       endif
       hga=gf/(32.d0*pi*dsqrt(2.d0))*ama**3*(alph/pi)**2*xfac

c      print*,'a -> gamgam',hga
c  a ---> z gamma
      xrmc = runm_hdec(ama/2,4)*amc/runm_hdec(amc,4)
      xrmb = runm_hdec(ama/2,5)*amb/runm_hdec(amb,5)
      xrmt = runm_hdec(ama/2,6)*amt/runm_hdec(amt,6)
c     print*,'xrmc,xrmb,xrmt ',xrmc,xrmb,xrmt
      if(ama.le.amz)then
       hzga=0
      else
       ts = ss/cs
       ft = -3*2d0/3*(1-4*2d0/3*ss)/dsqrt(ss*cs)*gat
       fb = 3*1d0/3*(-1+4*1d0/3*ss)/dsqrt(ss*cs)*gab
       fc = -3*2d0/3*(1-4*2d0/3*ss)/dsqrt(ss*cs)*gat
       fl = (-1+4*ss)/dsqrt(ss*cs)*gab
       if(i2hdm.eq.1) then
          fl = (-1+4*ss)/dsqrt(ss*cs)*galep
       endif
       eps=1.d-8
c      ctt = 4*xrmt**2/ama**2*dcmplx(1d0,-eps)
c      ctb = 4*xrmb**2/ama**2*dcmplx(1d0,-eps)
c      ctc = 4*xrmc**2/ama**2*dcmplx(1d0,-eps)
       ctt = 4*amt**2/ama**2*dcmplx(1d0,-eps)
       ctb = 4*amb**2/ama**2*dcmplx(1d0,-eps)
       ctc = 4*amc**2/ama**2*dcmplx(1d0,-eps)
       ctl = 4*amtau**2/ama**2*dcmplx(1d0,-eps)
c      clt = 4*xrmt**2/amz**2*dcmplx(1d0,-eps)
c      clb = 4*xrmb**2/amz**2*dcmplx(1d0,-eps)
c      clc = 4*xrmc**2/amz**2*dcmplx(1d0,-eps)
       clt = 4*amt**2/amz**2*dcmplx(1d0,-eps)
       clb = 4*amb**2/amz**2*dcmplx(1d0,-eps)
       clc = 4*amc**2/amz**2*dcmplx(1d0,-eps)
       cle = 4*amtau**2/amz**2*dcmplx(1d0,-eps)
       cat = ft*(- ci2(ctt,clt))
       cab = fb*(- ci2(ctb,clb))
       cac = fc*(- ci2(ctc,clc))
       cal = fl*(- ci2(ctl,cle))
c
c       ctc = 4*amc**2/ama**2*dcmplx(1d0,-eps)
c       clc = 4*amc**2/amz**2*dcmplx(1d0,-eps)
c       cac = ft*(- ci2(ctc,clc))
c
       xfac = cdabs(cat+cab+cac+cal)**2
       acoup = dsqrt(2d0)*gf*amz**2*ss*cs/pi**2
       hzga = gf/(4.d0*pi*dsqrt(2.d0))*ama**3*(alph/pi)*acoup/16.d0
     .        *xfac*(1-amz**2/ama**2)**3
      endif

c     print*,'a -> zgam',hzga
c  a ---> h z* ---> hff
      if(ionsh.eq.0)then
       dld=3d0
       dlu=5d0
       xm1 = aml+amz-dld
       xm2 = aml+amz+dlu
       if (ama.le.aml) then
        haz=0
       elseif (ama.le.xm1) then
        if (ama.le.dabs(amz-aml)) then
         haz=0
        else
         haz=9.d0*gf**2/8.d0/pi**3*amz**4*ama*gzal**2*
     .      (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .      *hvh((aml/ama)**2,(amz/ama)**2)
        endif
       elseif (ama.le.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        yy(1)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(1)*
     .      (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .      *hvh((aml/xx(1))**2,(amz/xx(1))**2)
        yy(2)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(2)*
     .      (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .      *hvh((aml/xx(2))**2,(amz/xx(2))**2)
        caz=lamb_hdec(aml**2/xx(3)**2,amz**2/xx(3)**2)
     .     *lamb_hdec(xx(3)**2/amz**2,aml**2/amz**2)**2
        yy(3)=gf/8d0/dsqrt(2d0)/pi*amz**4/xx(3)*caz
        caz=lamb_hdec(aml**2/xx(4)**2,amz**2/xx(4)**2)
     .     *lamb_hdec(xx(4)**2/amz**2,aml**2/amz**2)**2
        yy(4)=gf/8d0/dsqrt(2d0)/pi*amz**4/xx(4)*caz
        haz = fint_hdec(ama,xx,yy)*gzal**2
       else
        caz=lamb_hdec(aml**2/ama**2,amz**2/ama**2)
     .     *lamb_hdec(ama**2/amz**2,aml**2/amz**2)**2
        haz=gf/8d0/dsqrt(2d0)/pi*amz**4/ama*gzal**2*caz
       endif
      else
       if (ama.le.amz+aml) then
        haz=0
       else
        caz=lamb_hdec(aml**2/ama**2,amz**2/ama**2)
     .     *lamb_hdec(ama**2/amz**2,aml**2/amz**2)**2
        haz=gf/8d0/dsqrt(2d0)/pi*amz**4/ama*gzal**2*caz
       endif
      endif

c     print*,'a -> zh',haz

c mmm changed 23/8/2013
c  a ---> h z* ---> hff
      if(i2hdm.eq.1) then
      if(ionsh.eq.0)then
       dld=3d0
       dlu=5d0
       xm1 = amh+amz-dld
       xm2 = amh+amz+dlu
       if (ama.le.amh) then
        hhaz=0
       elseif (ama.le.xm1) then
        if (ama.le.dabs(amz-amh)) then
         hhaz=0
        else
         hhaz=9.d0*gf**2/8.d0/pi**3*amz**4*ama*gzah**2*
     .      (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .      *hvh((amh/ama)**2,(amz/ama)**2)
        endif
       elseif (ama.le.xm2) then
        xx(1) = xm1-1d0
        xx(2) = xm1
        xx(3) = xm2
        xx(4) = xm2+1d0
        yy(1)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(1)*
     .      (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .      *hvh((amh/xx(1))**2,(amz/xx(1))**2)
        yy(2)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(2)*
     .      (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .      *hvh((amh/xx(2))**2,(amz/xx(2))**2)
        caz=lamb_hdec(amh**2/xx(3)**2,amz**2/xx(3)**2)
     .     *lamb_hdec(xx(3)**2/amz**2,amh**2/amz**2)**2
        yy(3)=gf/8d0/dsqrt(2d0)/pi*amz**4/xx(3)*caz
        caz=lamb_hdec(amh**2/xx(4)**2,amz**2/xx(4)**2)
     .     *lamb_hdec(xx(4)**2/amz**2,amh**2/amz**2)**2
        yy(4)=gf/8d0/dsqrt(2d0)/pi*amz**4/xx(4)*caz
        hhaz = fint_hdec(ama,xx,yy)*gzah**2
       else
        caz=lamb_hdec(amh**2/ama**2,amz**2/ama**2)
     .     *lamb_hdec(ama**2/amz**2,amh**2/amz**2)**2
        hhaz=gf/8d0/dsqrt(2d0)/pi*amz**4/ama*gzah**2*caz
       endif
      else
       if (ama.le.amz+amh) then
        hhaz=0
       else
        caz=lamb_hdec(amh**2/ama**2,amz**2/ama**2)
     .     *lamb_hdec(ama**2/amz**2,amh**2/amz**2)**2
        hhaz=gf/8d0/dsqrt(2d0)/pi*amz**4/ama*gzah**2*caz
       endif
      endif
      endif

      if(i2hdm.eq.0) then
         hhaz=0.d0
      endif

c     print*,'a -> zh',hhaz

c  a ---> w+ h-
      if(i2hdm.eq.1) then
         if(ionsh.eq.0)then
            dld=3d0
            dlu=5d0
            xm1 = amw+amch-dld
            xm2 = amw+amch+dlu
            if (ama.lt.amch) then
               hawphm=0
            elseif (ama.le.xm1) then
               if(ama.le.dabs(amw-amch))then
                  hawphm=0
               else
                  hawphm=9.d0*gf**2/16.d0/pi**3*amw**4*ama
     .                 *hvh((amch/ama)**2,(amw/ama)**2)
               endif
            elseif (ama.lt.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               yy(1) = 9.d0*gf**2/16.d0/pi**3*amw**4*xx(1)
     .              *hvh((amch/xx(1))**2,(amw/xx(1))**2)
               yy(2) = 9.d0*gf**2/16.d0/pi**3*amw**4*xx(2)
     .              *hvh((amch/xx(2))**2,(amw/xx(2))**2)
               cwh=lamb_hdec(amch**2/xx(3)**2,amw**2/xx(3)**2)
     .              *lamb_hdec(xx(3)**2/amw**2,amch**2/amw**2)**2
               yy(3)=gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(3)*cwh
               cwh=lamb_hdec(amch**2/xx(4)**2,amw**2/xx(4)**2)
     .              *lamb_hdec(xx(4)**2/amw**2,amch**2/amw**2)**2
               yy(4)=gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(4)*cwh
               hawphm = fint_hdec(ama,xx,yy)
            else
               cwh=lamb_hdec(amch**2/ama**2,amw**2/ama**2)
     .              *lamb_hdec(ama**2/amw**2,amch**2/amw**2)**2
               hawphm=gf/8.d0/dsqrt(2d0)/pi*amw**4/ama*cwh
            endif
         else
            if (ama.lt.amw+amch) then
               hawphm=0
            else
               cwh=lamb_hdec(amch**2/ama**2,amw**2/ama**2)
     .              *lamb_hdec(ama**2/amw**2,amch**2/amw**2)**2
               hawphm=gf/8.d0/dsqrt(2d0)/pi*amw**4/ama*cwh
            endif
         endif
      endif

      if(i2hdm.eq.0) then
         hawphm = 0.d0
      endif

      hawphm = 2.d0*hawphm

c     print*,'a -> w+h- + w-h+',hawphm,hawphm/2.d0
c end mmm changed 23/8/2013

c
c ========================== susy decays
c
      if(iofsusy.eq.0) then
c  a ----> charginos
      do 731 i=1,2
      do 731 j=1,2
      if (ama.gt.amchar(i)+amchar(j)) then
      whach(i,j)=gf*amw**2/(2*pi*dsqrt(2.d0))/ama
     .     *lamb_hdec(amchar(i)**2/ama**2,amchar(j)**2/ama**2)
     .     *( (ac3(i,j)**2+ac3(j,i)**2)*(ama**2-amchar(i)
     .         **2-amchar(j)**2)+4.d0*ac3(i,j)*ac3(j,i)*
     .         xmchar(i)*xmchar(j) )
      else
      whach(i,j)=0.d0
      endif
 731  continue
      whacht=whach(1,1)+whach(1,2)+whach(2,1)+whach(2,2)
c  a ----> neutralinos
      do 732 i=1,4
      do 732 j=1,4
      if (ama.gt.amneut(i)+amneut(j)) then
      whane(i,j)=gf*amw**2/(2*pi*dsqrt(2.d0))/ama
     .         *an3(i,j)**2*(ama**2-(xmneut(i)-xmneut(j))**2)
     .         *lamb_hdec(amneut(i)**2/ama**2,amneut(j)**2/ama**2)
      else
      whane(i,j)=0.d0
      endif
 732  continue
      whanet= whane(1,1)+whane(1,2)+whane(1,3)+whane(1,4)
     .       +whane(2,1)+whane(2,2)+whane(2,3)+whane(2,4)
     .       +whane(3,1)+whane(3,2)+whane(3,3)+whane(3,4)
     .       +whane(4,1)+whane(4,2)+whane(4,3)+whane(4,4)

c  a ----> stau's
c
      if(ama.gt.amsl(1)+amsl(2)) then
      whasl=gf*amz**4/dsqrt(2.d0)/pi*gaee**2*
     .      lamb_hdec(amsl(1)**2/ama**2,amsl(2)**2/ama**2)/ama
      else
      whasl=0.d0
      endif
c
c  a ----> stops
c
      susy = 1
c     qsq = (ymst(1)+ymst(2))/2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     do k=-10,10
c     qsq = ama*10.d0**(k/10.d0)
c     do k=-1,1
c     qsq = ama*2.d0**(k)
c     if(ama.gt.ymst(1)+ymst(2)) then
c      call sqmbapp_hdec(qsq)
c      susy = 1+sqsusy_hdec(3,1,1,2,qsq,0,1)
c      whast0=3*gf*amz**4/dsqrt(2.d0)/pi*yatt**2*
c    .       lamb_hdec(ymst(1)**2/ama**2,ymst(2)**2/ama**2)/ama
c      whast=3*gf*amz**4/dsqrt(2.d0)/pi*yatt**2*
c    .       lamb_hdec(ymst(1)**2/ama**2,ymst(2)**2/ama**2)/ama
c    .      *susy
c     else
c      whast=0.d0
c     endif
c     write(9,*)'a -> t1 t2: ',qsq/ama,whast0,whast
c     write(903,('1x,g10.4,2(1x,g10.4)'))qsq/ama,whast0,whast
c     enddo
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      qsq = ama
      if(ama.gt.ymst(1)+ymst(2)) then
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       call sqmbapp_hdec(qsq)
       susy = 1+sqsusy_hdec(3,1,1,2,qsq,0,1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       whast=3*gf*amz**4/dsqrt(2.d0)/pi*yatt**2*
     .       lamb_hdec(ymst(1)**2/ama**2,ymst(2)**2/ama**2)/ama
     .      *susy
c      write(6,*)'a -> stop: ',ama,amst(1),amst(2),100*(susy-1),'% ',
c    .           whast/susy,whast
c      write(712,*)ama,whast/2,whast/susy/2
c      write(721,*)ama,whast/2,whast/susy/2
c      write(6,*)'a -> stop: ',ama,amst(1),amst(2),susy-1
      else
      whast=0.d0
      endif
c
c  a ----> sbottoms
c
      susy = 1
c     qsq = (ymsb(1)+ymsb(2))/2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     do k=-10,10
c     qsq = ama*10.d0**(k/10.d0)
c     do k=-1,1
c     qsq = ama*2.d0**(k)
c     if(ama.gt.ymsb(1)+ymsb(2)) then
c      call sqmbapp_hdec(qsq)
c      susy = 1+sqsusy_hdec(3,2,1,2,qsq,0,1)
c      whasb0=3*gf*amz**4/dsqrt(2.d0)/pi*yabb**2*
c    .       lamb_hdec(ymsb(1)**2/ama**2,ymsb(2)**2/ama**2)/ama
c      whasb=3*gf*amz**4/dsqrt(2.d0)/pi*yabb**2*
c    .       lamb_hdec(ymsb(1)**2/ama**2,ymsb(2)**2/ama**2)/ama
c    .      *susy
c     else
c      whasb=0.d0
c     endif
c     write(9,*)'a -> b1 b2: ',qsq/ama,whasb0,whasb
c     write(904,('1x,g10.4,2(1x,g10.4)'))qsq/ama,whasb0,whasb
c     enddo
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      qsq = ama
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     dummy0 = 0
c     dummy1 = 0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      if(ama.gt.ymsb(1)+ymsb(2)) then
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       call sqmbapp_hdec(qsq)
       susy = 1+sqsusy_hdec(3,2,1,2,qsq,0,1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       whasb=3*gf*amz**4/dsqrt(2.d0)/pi*yabb**2*
     .       lamb_hdec(ymsb(1)**2/ama**2,ymsb(2)**2/ama**2)/ama
     .      *susy
c      write(6,*)'a -> sbot: ',ama,ymsb(1),ymsb(2),100*(susy-1),'% ',
c    .           whasb/susy,whasb
c      write(812,*)ama,whasb/2,whasb/susy/2
c      write(821,*)ama,whasb/2,whasb/susy/2
c      write(6,*)'a -> sbot: ',ama,amsb(1),amsb(2),susy-1
c      dummy0 = dummy0 + whasb/susy
c      dummy1 = dummy1 + whasb
      else
      whasb=0.d0
      endif
c     write(6,*)'a -> sbot0: ',ymsb(1),ymsb(2),dummy0,dummy1,
c    .                        100*(dummy1/dummy0-1),'%'
c
      else
      whacht=0.d0
      whanet=0.d0
      whasl=0.d0
      whast=0.d0
      whasb=0.d0
c--change thanks to elzbieta richter-was
      do i=1,2
       do j=1,2
        whach(i,j)=0.d0
       enddo
      enddo
      do i=1,4
       do j=1,4
        whane(i,j)=0.d0
       enddo
      enddo
      endif

      if(igold.ne.0)then
c   ha ---> goldstinos
       do 730 i=1,4
       if (ama.gt.amneut(i)) then
        whagd(i)=ama**5/axmpl**2/axmgd**2/48.d0/pi*
     .           (1.d0-amneut(i)**2/ama**2)**4*agda(i)**2
       else
        whagd(i)=0.d0
       endif
 730   continue
       whagdt=whagd(1)+whagd(2)+whagd(3)+whagd(4)
      else
       whagdt=0
      endif
c
c    ==========  total width and branching ratios
      wtot=hll+hmm+hss+hcc+hbb+hgg+hga+hzga+haz+htt
     .    +whacht+whanet+whasl+whast+whasb + whagdt

c mmm changed 23/8/2013
      if(i2hdm.eq.1) then
         wtot = wtot + hhaz + hawphm
         abrhhaz = hhaz/wtot
         abrhawphm = hawphm/wtot
      endif
      if(i2hdm.eq.0) then
         abrhhaz = 0.d0
         abrhawphm = 0.d0
      endif
c end mmm changed 23/8/2013

c     print*,'wtot',wtot

      abrt=htt/wtot
      abrb=hbb/wtot
      abrl=hll/wtot
      abrm=hmm/wtot
      abrs=hss/wtot
      abrc=hcc/wtot
      abrg=hgg/wtot
      abrga=hga/wtot
      abrzga=hzga/wtot
      abrz=haz/wtot
      do 831 i=1,2
      do 831 j=1,2
      habrsc(i,j)=whach(i,j)/wtot
831   continue
      do 832 i=1,4
      do 832 j=1,4
      habrsn(i,j)=whane(i,j)/wtot
832   continue
      habrcht=whacht/wtot
      habrnet=whanet/wtot
      habrsl=whasl/wtot
      habrst=whast/wtot
      habrsb=whasb/wtot
      habrgd=whagdt/wtot
c
      awdth=wtot

      bhastau = whasl/wtot
      bhasb = whasb/wtot
      bhast = whast/wtot

c    ==============================================================
      endif

      return
      end
