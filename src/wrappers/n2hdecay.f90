subroutine hdecay_n2hdm_broken(type2HDM, tgbet, m12sq, &
                               mA, mhc, mH1, mH2, mH3, &
                               a1, a2, a3, vs, &
                               smparams, &
                               BR) bind(C, name='hdecay_N2HDM_broken')
    use, intrinsic :: iso_c_binding

    implicit none
    integer(kind=c_int), intent(in), value :: type2HDM
    real(kind=c_double), intent(in), value :: tgbet, mA, m12sq, mhc, vs
    real(kind=c_double), intent(in), value :: mH1, mH2, mH3, a1, a2, a3
    real(kind=c_double), intent(in) ::  smparams(22)
    real(kind=c_double), intent(out) :: BR(84)

    double precision ams, amc, amb, amt
    double precision AMSB
    double precision amcb, AMBB
    double precision GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    double precision VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    double precision GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    double precision XLAMBDA, AMC0, AMB0, AMT0
    integer N0, IHIGGS, NNLO
    integer IPOLE, IONSH, IONWZ, IOFSUSY, NFGG
    double precision TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM
    integer ITYPE2HDM, I2HDM, IPARAM2HDM
    double precision ALPH1_N2HDM, ALPH2_N2HDM, &
        ALPH3_N2HDM, AVS_N2HDM, &
        AL7_N2HDM, AL8_N2HDM, AM11SQ_N2HDM, &
        AM22SQ_N2HDM, AMSSQ_N2HDM
    integer IN2HDM, IPHASE
    double precision amhiggs(3)
    COMMON/MASSES_HDEC/AMS, AMC, AMB, AMT
    COMMON/STRANGE_HDEC/AMSB
    COMMON/MSBAR_HDEC/AMCB, AMBB
    COMMON/PARAM_HDEC/GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    COMMON/CKMPAR_HDEC/VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    COMMON/WZWDTH_HDEC/GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    COMMON/ALS_HDEC/XLAMBDA, AMC0, AMB0, AMT0, N0
    COMMON/FLAG_HDEC/IHIGGS, NNLO, IPOLE
    COMMON/ONSHELL_HDEC/IONSH, IONWZ, IOFSUSY
    COMMON/OLDFASH_HDEC/NFGG
    COMMON/THDM_HDEC/TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM, ITYPE2HDM, I2HDM, IPARAM2HDM
    Common/N2HDM_HDEC_PARAM/amhiggs, ALPH1_N2HDM, ALPH2_N2HDM, &
        ALPH3_N2HDM, AVS_N2HDM, &
        AL7_N2HDM, AL8_N2HDM, AM11SQ_N2HDM, &
        AM22SQ_N2HDM, AMSSQ_N2HDM, &
        IN2HDM, IPHASE

    double precision abrb, abrl, abrm, abrs, abrc, abrt, abrg, abrga, &
        abrzga, abrz, awdth
    double precision HCBRB, HCBRL, HCBRM, HCBRBU, HCBRS, HCBRC, HCBRT, &
        HCBRW, HCBRA, HCWDTH
    double precision hcbrcd, hcbrts, hcbrtd
    double precision hcbrwhh, hhbrchch, hlbrchch, abrhhaz, abrhawphm
    double precision h2brh1h1, h3brh1h1, h3brh2h2, h3brh1h2, &
        hibrt(3), hibrb(3), hibrl(3), hibrm(3), hibrs(3), hibrc(3), &
        hibrg(3), hibrga(3), hibrzga(3), hibrw(3), hibrz(3), &
        hibrhpwm(3), hibraz(3), hibrchch(3), hibraa(3), &
        gamtot(3)
    double precision hcbrwh(3), abrhz(3)
    COMMON/WIDTHA_HDEC/ABRB, ABRL, ABRM, ABRS, ABRC, ABRT, ABRG, ABRGA, &
        ABRZGA, ABRZ, AWDTH
    COMMON/WIDTHHC_HDEC/HCBRB, HCBRL, HCBRM, HCBRBU, HCBRS, HCBRC, HCBRT, &
        HCBRW, HCBRA, HCWDTH
    COMMON/WIDTH_HC_ADD/hcbrcd, hcbrts, hcbrtd
    COMMON/WIDTH_2HDM/hcbrwhh, hhbrchch, hlbrchch, abrhhaz, abrhawphm
    Common/N2HDM_BR_H/h2brh1h1, h3brh1h1, h3brh2h2, h3brh1h2, &
        hibrt, hibrb, hibrl, hibrm, hibrs, hibrc, &
        hibrg, hibrga, hibrzga, hibrw, hibrz, &
        hibrhpwm, hibraz, hibrchch, hibraa, &
        gamtot
    Common/N2HDM_BR_Hc_A/hcbrwh, abrhz

    double precision alsmz, acc, del, ambl, ambu, fmb, xmb
    double precision runm_hdec, xitla_hdec
    integer nloop, nber

    ITYPE2HDM = TYPE2HDM
    IPHASE = 1
    TGBET2HDM = TGBET
    AM12SQ = M12SQ
    AMHA2HDM = mA
    AMHC2HDM = mHc
    amhiggs(1) = mH1
    amhiggs(2) = mH2
    amhiggs(3) = mH3
    alph1_N2HDM = a1
    alph2_N2HDM = a2
    alph3_N2HDM = a3
    AVS_N2HDM = vs
    ALPH2HDM = 0.
    AL7_N2HDM = 0.
    AL8_N2HDM = 0.

    ALSMZ = smparams(1)
    AMS = smparams(2)
    AMC = smparams(3)
    AMB = smparams(4)
    AMT = smparams(5)
    AMTAU = smparams(6)
    AMMUON = smparams(7)
    ALPH = smparams(8)
    GF = smparams(9)
    GAMW = smparams(10)
    GAMZ = smparams(11)
    AMZ = smparams(12)
    AMW = smparams(13)
    VTB = smparams(14)
    VTS = smparams(15)
    VTD = smparams(16)
    VCB = smparams(17)
    VCS = smparams(18)
    VCD = smparams(19)
    VUB = smparams(20)
    VUS = smparams(21)
    VUD = smparams(22)

    IHIGGS = 5
    I2HDM = 1
    IN2HDM = 1
    IPARAM2HDM = 1
    ALPH2HDM = 0
    AMHL2HDM = 10
    AMHH2HDM = 10
    A1LAM2HDM = 1
    A2LAM2HDM = 1
    A3LAM2HDM = 1
    A4LAM2HDM = 1
    A5LAM2HDM = 1
    NNLO = 1
    IONSH = 0
    IONWZ = 0
    IPOLE = 0
    IOFSUSY = 1
    NFGG = 5

! **********************************************************************
! ***************   initialize alpha_s and polylogs    *****************
    ALPH = 1.D0/ALPH
    AMSB = AMS
    AMCB = AMC
    AMBB = AMB

    AMC0 = AMC
    AMB0 = AMB
    AMT0 = AMT
    ACC = 1.D-10
    NLOOP = 3
    XLAMBDA = XITLA_HDEC(NLOOP, ALSMZ, ACC)
    N0 = 5
    CALL ALSINI_HDEC(ACC)
!--DECOUPLING THE TOP QUARK FROM ALPHAS
    AMT0 = 3.D8

    del = 1.d-10
    ambl = ambb
    ambu = 2*ambb
    fmb = (ambl + ambu)/2
    acc = 1.d-10
    nloop = 3
12  amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    amc0 = amc
    n0 = 5
    call alsini_hdec(acc)
    xmb = runm_hdec(ambb, 5)
    if (abs(xmb - ambb) .lt. del) then
        ambl = fmb
        ambu = fmb
    elseif (xmb .gt. ambb) then
        ambu = fmb
    else
        ambl = fmb
    endif
    fmb = (ambl + ambu)/2
    if (dabs(xmb/ambb - 1) .gt. del) goto 12
    amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    n0 = 5
    call alsini_hdec(acc)
!--INITIALIZE COEFFICIENTS FOR POLYLOGARITHMS
    NBER = 18
    CALL BERNINI_HDEC(NBER)

    CALL HDEC_N2HDM()
    BR = (/gamtot(1), hibrb(1), hibrl(1), hibrm(1), hibrs(1), &
           hibrc(1), hibrt(1), hibrg(1), hibrga(1), hibrzga(1), &
           hibrw(1), hibrz(1), hibraz(1), hibrhpwm(1), hibraa(1), &
           hibrchch(1), &
           gamtot(2), hibrb(2), hibrl(2), hibrm(2), hibrs(2), &
           hibrc(2), hibrt(2), hibrg(2), hibrga(2), hibrzga(2), &
           hibrw(2), hibrz(2), hibraz(2), hibrhpwm(2), &
           h2brh1h1, hibraa(2), hibrchch(2), &
           gamtot(3), hibrb(3), hibrl(3), hibrm(3), hibrs(3), &
           hibrc(3), hibrt(3), hibrg(3), hibrga(3), hibrzga(3), &
           hibrw(3), hibrz(3), hibraz(3), hibrhpwm(3), &
           h3brh1h1, h3brh1h2, h3brh2h2, hibraa(3), hibrchch(3), &
           awdth, abrb, abrl, abrm, abrs, abrc, abrt, abrg, &
           abrga, abrzga, abrhz(1), abrhz(2), &
           abrhz(3), abrhawphm, &
           hcwdth, hcbrb, hcbrl, hcbrm, hcbrs, hcbrc, hcbrt, &
           hcbrcd, hcbrbu, hcbrts, hcbrtd, &
           hcbrwh(1), hcbrwh(2), hcbrwh(3), hcbra, &
           gamt1, gamt0/gamt1, (gamt1 - gamt0)/gamt1/)
    return
end

subroutine hdecay_N2HDM_darks(type2HDM, tgbet, m12sq, &
                              mA, mhc, mDM, mHl, mHh, alpha, L7, L8, &
                              smparams, &
                              BR) bind(C, name='hdecay_N2HDM_darks')
    use, intrinsic :: iso_c_binding

    implicit none
    integer(kind=c_int), intent(in), value :: type2HDM
    real(kind=c_double), intent(in), value :: tgbet, mA, m12sq, mhc
    real(kind=c_double), intent(in), value :: mDM, mHh, mHl, alpha, L7, L8
    real(kind=c_double), intent(in) ::  smparams(22)
    real(kind=c_double), intent(out) :: BR(65)

    double precision ams, amc, amb, amt
    double precision AMSB
    double precision amcb, AMBB
    double precision GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    double precision VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    double precision GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    double precision XLAMBDA, AMC0, AMB0, AMT0
    integer N0, IHIGGS, NNLO
    integer IPOLE, IONSH, IONWZ, IOFSUSY, NFGG
    double precision TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM
    integer ITYPE2HDM, I2HDM, IPARAM2HDM
    double precision ALPH1_N2HDM, ALPH2_N2HDM, &
        ALPH3_N2HDM, AVS_N2HDM, &
        AL7_N2HDM, AL8_N2HDM, AM11SQ_N2HDM, &
        AM22SQ_N2HDM, AMSSQ_N2HDM
    integer IN2HDM, IPHASE
    double precision amhiggs(3)
    COMMON/MASSES_HDEC/AMS, AMC, AMB, AMT
    COMMON/STRANGE_HDEC/AMSB
    COMMON/MSBAR_HDEC/AMCB, AMBB
    COMMON/PARAM_HDEC/GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    COMMON/CKMPAR_HDEC/VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    COMMON/WZWDTH_HDEC/GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    COMMON/ALS_HDEC/XLAMBDA, AMC0, AMB0, AMT0, N0
    COMMON/FLAG_HDEC/IHIGGS, NNLO, IPOLE
    COMMON/ONSHELL_HDEC/IONSH, IONWZ, IOFSUSY
    COMMON/OLDFASH_HDEC/NFGG
    COMMON/THDM_HDEC/TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM, ITYPE2HDM, I2HDM, IPARAM2HDM
    Common/N2HDM_HDEC_PARAM/amhiggs, ALPH1_N2HDM, ALPH2_N2HDM, &
        ALPH3_N2HDM, AVS_N2HDM, &
        AL7_N2HDM, AL8_N2HDM, AM11SQ_N2HDM, &
        AM22SQ_N2HDM, AMSSQ_N2HDM, &
        IN2HDM, IPHASE

    double precision abrb, abrl, abrm, abrs, abrc, abrt, abrg, abrga, &
        abrzga, abrz, awdth
    double precision HCBRB, HCBRL, HCBRM, HCBRBU, HCBRS, HCBRC, HCBRT, &
        HCBRW, HCBRA, HCWDTH
    double precision hcbrcd, hcbrts, hcbrtd
    double precision hcbrwhh, hhbrchch, hlbrchch, abrhhaz, abrhawphm
    double precision h2brh1h1, h3brh1h1, h3brh2h2, h3brh1h2, &
        hibrt(3), hibrb(3), hibrl(3), hibrm(3), hibrs(3), hibrc(3), &
        hibrg(3), hibrga(3), hibrzga(3), hibrw(3), hibrz(3), &
        hibrhpwm(3), hibraz(3), hibrchch(3), hibraa(3), &
        gamtot(3)
    double precision hcbrwh(3), abrhz(3)
    COMMON/WIDTHA_HDEC/ABRB, ABRL, ABRM, ABRS, ABRC, ABRT, ABRG, ABRGA, &
        ABRZGA, ABRZ, AWDTH
    COMMON/WIDTHHC_HDEC/HCBRB, HCBRL, HCBRM, HCBRBU, HCBRS, HCBRC, HCBRT, &
        HCBRW, HCBRA, HCWDTH
    COMMON/WIDTH_HC_ADD/hcbrcd, hcbrts, hcbrtd
    COMMON/WIDTH_2HDM/hcbrwhh, hhbrchch, hlbrchch, abrhhaz, abrhawphm
    Common/N2HDM_BR_H/h2brh1h1, h3brh1h1, h3brh2h2, h3brh1h2, &
        hibrt, hibrb, hibrl, hibrm, hibrs, hibrc, &
        hibrg, hibrga, hibrzga, hibrw, hibrz, &
        hibrhpwm, hibraz, hibrchch, hibraa, &
        gamtot
    Common/N2HDM_BR_Hc_A/hcbrwh, abrhz

    double precision alsmz, acc, del, ambl, ambu, fmb, xmb
    double precision runm_hdec, xitla_hdec
    integer nloop, nber

    ITYPE2HDM = TYPE2HDM
    IPHASE = 2
    TGBET2HDM = TGBET
    AM12SQ = M12SQ
    AMHA2HDM = mA
    AMHC2HDM = mHc
    amhiggs(1) = mDM
    amhiggs(2) = mHl
    amhiggs(3) = mHh
    ALPH2HDM = alpha
    AL7_N2HDM = L7
    AL8_N2HDM = L8

    ALSMZ = smparams(1)
    AMS = smparams(2)
    AMC = smparams(3)
    AMB = smparams(4)
    AMT = smparams(5)
    AMTAU = smparams(6)
    AMMUON = smparams(7)
    ALPH = smparams(8)
    GF = smparams(9)
    GAMW = smparams(10)
    GAMZ = smparams(11)
    AMZ = smparams(12)
    AMW = smparams(13)
    VTB = smparams(14)
    VTS = smparams(15)
    VTD = smparams(16)
    VCB = smparams(17)
    VCS = smparams(18)
    VCD = smparams(19)
    VUB = smparams(20)
    VUS = smparams(21)
    VUD = smparams(22)

    IHIGGS = 5
    I2HDM = 1
    IN2HDM = 1
    IPARAM2HDM = 1
    AMHL2HDM = 10
    AMHH2HDM = 10
    A1LAM2HDM = 1
    A2LAM2HDM = 1
    A3LAM2HDM = 1
    A4LAM2HDM = 1
    A5LAM2HDM = 1
    NNLO = 1
    IONSH = 0
    IONWZ = 0
    IPOLE = 0
    IOFSUSY = 1
    NFGG = 5

! **********************************************************************
! ***************   initialize alpha_s and polylogs    *****************
    ALPH = 1.D0/ALPH
    AMSB = AMS
    AMCB = AMC
    AMBB = AMB

    AMC0 = AMC
    AMB0 = AMB
    AMT0 = AMT
    ACC = 1.D-10
    NLOOP = 3
    XLAMBDA = XITLA_HDEC(NLOOP, ALSMZ, ACC)
    N0 = 5
    CALL ALSINI_HDEC(ACC)
!--DECOUPLING THE TOP QUARK FROM ALPHAS
    AMT0 = 3.D8

    del = 1.d-10
    ambl = ambb
    ambu = 2*ambb
    fmb = (ambl + ambu)/2
    acc = 1.d-10
    nloop = 3
12  amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    amc0 = amc
    n0 = 5
    call alsini_hdec(acc)
    xmb = runm_hdec(ambb, 5)
    if (abs(xmb - ambb) .lt. del) then
        ambl = fmb
        ambu = fmb
    elseif (xmb .gt. ambb) then
        ambu = fmb
    else
        ambl = fmb
    endif
    fmb = (ambl + ambu)/2
    if (dabs(xmb/ambb - 1) .gt. del) goto 12
    amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    n0 = 5
    call alsini_hdec(acc)
!--INITIALIZE COEFFICIENTS FOR POLYLOGARITHMS
    NBER = 18
    CALL BERNINI_HDEC(NBER)

    CALL HDEC_N2HDM()
    BR = (/gamtot(2), hibrb(2), hibrl(2), hibrm(2), hibrs(2), &
           hibrc(2), hibrt(2), hibrg(2), hibrga(2), hibrzga(2), &
           hibrw(2), hibrz(2), hibraz(2), hibrhpwm(2), &
           h2brh1h1, hibraa(2), hibrchch(2), &
           gamtot(3), hibrb(3), hibrl(3), hibrm(3), hibrs(3), &
           hibrc(3), hibrt(3), hibrg(3), hibrga(3), hibrzga(3), &
           hibrw(3), hibrz(3), hibraz(3), hibrhpwm(3), &
           h3brh1h1, h3brh2h2, hibraa(3), hibrchch(3), &
           awdth, abrb, abrl, abrm, abrs, abrc, abrt, abrg, &
           abrga, abrzga, abrhz(2), &
           abrhz(3), abrhawphm, &
           hcwdth, hcbrb, hcbrl, hcbrm, hcbrs, hcbrc, hcbrt, &
           hcbrcd, hcbrbu, hcbrts, hcbrtd, &
           hcbrwh(2), hcbrwh(3), hcbra, &
           gamt1, gamt0/gamt1, (gamt1 - gamt0)/gamt1/)
    return
end

subroutine hdecay_N2HDM_darkd(mA, mhc, mDM, mHl, mHh, alpha, &
                              L7, m22sq, vs, &
                              smparams, &
                              BR) bind(C, name='hdecay_N2HDM_darkd')
    use, intrinsic:: iso_c_binding

    implicit none
    real(kind=c_double), intent(in), value :: mA, mhc, mDM, mHl, mHh, alpha
    real(kind=c_double), intent(in), value :: L7, m22sq, vs
    real(kind=c_double), intent(in) :: smparams(22)
    real(kind=c_double), intent(out) :: BR(40)

    double precision ams, amc, amb, amt
    double precision AMSB
    double precision amcb, AMBB
    double precision GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    double precision VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    double precision GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    double precision XLAMBDA, AMC0, AMB0, AMT0
    integer N0, IHIGGS, NNLO
    integer IPOLE, IONSH, IONWZ, IOFSUSY, NFGG
    double precision TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM
    integer ITYPE2HDM, I2HDM, IPARAM2HDM
    double precision ALPH1_N2HDM, ALPH2_N2HDM, &
        ALPH3_N2HDM, AVS_N2HDM, &
        AL7_N2HDM, AL8_N2HDM, AM11SQ_N2HDM, &
        AM22SQ_N2HDM, AMSSQ_N2HDM
    integer IN2HDM, IPHASE
    double precision amhiggs(3)
    COMMON/MASSES_HDEC/AMS, AMC, AMB, AMT
    COMMON/STRANGE_HDEC/AMSB
    COMMON/MSBAR_HDEC/AMCB, AMBB
    COMMON/PARAM_HDEC/GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    COMMON/CKMPAR_HDEC/VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    COMMON/WZWDTH_HDEC/GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    COMMON/ALS_HDEC/XLAMBDA, AMC0, AMB0, AMT0, N0
    COMMON/FLAG_HDEC/IHIGGS, NNLO, IPOLE
    COMMON/ONSHELL_HDEC/IONSH, IONWZ, IOFSUSY
    COMMON/OLDFASH_HDEC/NFGG
    COMMON/THDM_HDEC/TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM, ITYPE2HDM, I2HDM, IPARAM2HDM
    Common/N2HDM_HDEC_PARAM/amhiggs, ALPH1_N2HDM, ALPH2_N2HDM, &
        ALPH3_N2HDM, AVS_N2HDM, &
        AL7_N2HDM, AL8_N2HDM, AM11SQ_N2HDM, &
        AM22SQ_N2HDM, AMSSQ_N2HDM, &
        IN2HDM, IPHASE

    double precision abrb, abrl, abrm, abrs, abrc, abrt, abrg, abrga, &
        abrzga, abrz, awdth
    double precision HCBRB, HCBRL, HCBRM, HCBRBU, HCBRS, HCBRC, HCBRT, &
        HCBRW, HCBRA, HCWDTH
    double precision hcbrcd, hcbrts, hcbrtd
    double precision hcbrwhh, hhbrchch, hlbrchch, abrhhaz, abrhawphm
    double precision h2brh1h1, h3brh1h1, h3brh2h2, h3brh1h2, &
        hibrt(3), hibrb(3), hibrl(3), hibrm(3), hibrs(3), hibrc(3), &
        hibrg(3), hibrga(3), hibrzga(3), hibrw(3), hibrz(3), &
        hibrhpwm(3), hibraz(3), hibrchch(3), hibraa(3), &
        gamtot(3)
    double precision hcbrwh(3), abrhz(3)
    COMMON/WIDTHA_HDEC/ABRB, ABRL, ABRM, ABRS, ABRC, ABRT, ABRG, ABRGA, &
        ABRZGA, ABRZ, AWDTH
    COMMON/WIDTHHC_HDEC/HCBRB, HCBRL, HCBRM, HCBRBU, HCBRS, HCBRC, HCBRT, &
        HCBRW, HCBRA, HCWDTH
    COMMON/WIDTH_HC_ADD/hcbrcd, hcbrts, hcbrtd
    COMMON/WIDTH_2HDM/hcbrwhh, hhbrchch, hlbrchch, abrhhaz, abrhawphm
    Common/N2HDM_BR_H/h2brh1h1, h3brh1h1, h3brh2h2, h3brh1h2, &
        hibrt, hibrb, hibrl, hibrm, hibrs, hibrc, &
        hibrg, hibrga, hibrzga, hibrw, hibrz, &
        hibrhpwm, hibraz, hibrchch, hibraa, &
        gamtot
    Common/N2HDM_BR_Hc_A/hcbrwh, abrhz

    double precision alsmz, acc, del, ambl, ambu, fmb, xmb
    double precision runm_hdec, xitla_hdec
    integer nloop, nber

    ITYPE2HDM = 1
    IPHASE = 3
    AMHA2HDM = mA
    AMHC2HDM = mHc
    amhiggs(1) = mDM
    amhiggs(2) = mHl
    amhiggs(3) = mHh
    ALPH2HDM = alpha
    AL7_N2HDM = L7
    Avs_N2HDM = vs
    AM11SQ_N2HDM = m22sq

    ALSMZ = smparams(1)
    AMS = smparams(2)
    AMC = smparams(3)
    AMB = smparams(4)
    AMT = smparams(5)
    AMTAU = smparams(6)
    AMMUON = smparams(7)
    ALPH = smparams(8)
    GF = smparams(9)
    GAMW = smparams(10)
    GAMZ = smparams(11)
    AMZ = smparams(12)
    AMW = smparams(13)
    VTB = smparams(14)
    VTS = smparams(15)
    VTD = smparams(16)
    VCB = smparams(17)
    VCS = smparams(18)
    VCD = smparams(19)
    VUB = smparams(20)
    VUS = smparams(21)
    VUD = smparams(22)

    IHIGGS = 5
    I2HDM = 1
    IN2HDM = 1
    IPARAM2HDM = 1
    AMHL2HDM = 10
    AMHH2HDM = 10
    A1LAM2HDM = 1
    A2LAM2HDM = 1
    A3LAM2HDM = 1
    A4LAM2HDM = 1
    A5LAM2HDM = 1
    NNLO = 1
    IONSH = 0
    IONWZ = 0
    IPOLE = 0
    IOFSUSY = 1
    NFGG = 5

! **********************************************************************
! ***************   initialize alpha_s and polylogs    *****************
    ALPH = 1.D0/ALPH
    AMSB = AMS
    AMCB = AMC
    AMBB = AMB

    AMC0 = AMC
    AMB0 = AMB
    AMT0 = AMT
    ACC = 1.D-10
    NLOOP = 3
    XLAMBDA = XITLA_HDEC(NLOOP, ALSMZ, ACC)
    N0 = 5
    CALL ALSINI_HDEC(ACC)
!--DECOUPLING THE TOP QUARK FROM ALPHAS
    AMT0 = 3.D8

    del = 1.d-10
    ambl = ambb
    ambu = 2*ambb
    fmb = (ambl + ambu)/2
    acc = 1.d-10
    nloop = 3
12  amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    amc0 = amc
    n0 = 5
    call alsini_hdec(acc)
    xmb = runm_hdec(ambb, 5)
    if (abs(xmb - ambb) .lt. del) then
        ambl = fmb
        ambu = fmb
    elseif (xmb .gt. ambb) then
        ambu = fmb
    else
        ambl = fmb
    endif
    fmb = (ambl + ambu)/2
    if (dabs(xmb/ambb - 1) .gt. del) goto 12
    amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    n0 = 5
    call alsini_hdec(acc)
!--INITIALIZE COEFFICIENTS FOR POLYLOGARITHMS
    NBER = 18
    CALL BERNINI_HDEC(NBER)

    CALL HDEC_N2HDM()
    BR = (/gamtot(2), hibrb(2), hibrl(2), hibrm(2), hibrs(2), &
           hibrc(2), hibrt(2), hibrg(2), hibrga(2), hibrzga(2), &
           hibrw(2), hibrz(2), &
           h2brh1h1, hibraa(2), hibrchch(2), &
           gamtot(3), hibrb(3), hibrl(3), hibrm(3), hibrs(3), &
           hibrc(3), hibrt(3), hibrg(3), hibrga(3), hibrzga(3), &
           hibrw(3), hibrz(3), &
           h3brh1h1, h3brh2h2, hibraa(3), hibrchch(3), &
           gamtot(1), hibraz(1), hibrhpwm(1), &
           awdth, abrhz(1), abrhawphm, &
           hcwdth, hcbrwh(1), hcbra/)
    return
end

subroutine hdecay_N2HDM_darksd(mA, mhc, mHsm, mHDD, mHDS, &
                               m22sq, mssq, &
                               smparams, &
                               BR) bind(C, name='hdecay_N2HDM_darksd')
    use, intrinsic:: iso_c_binding

    implicit none
    real(kind=c_double), intent(in), value :: mA, mhc, mHsm, mHDD, mHDs, &
                                              m22sq, mssq
    real(kind=c_double), intent(in) :: smparams(22)
    real(kind=c_double), intent(out) :: BR(25)

    double precision ams, amc, amb, amt
    double precision AMSB
    double precision amcb, AMBB
    double precision GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    double precision VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    double precision GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    double precision XLAMBDA, AMC0, AMB0, AMT0
    integer N0, IHIGGS, NNLO
    integer IPOLE, IONSH, IONWZ, IOFSUSY, NFGG
    double precision TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM
    integer ITYPE2HDM, I2HDM, IPARAM2HDM
    double precision ALPH1_N2HDM, ALPH2_N2HDM, &
        ALPH3_N2HDM, AVS_N2HDM, &
        AL7_N2HDM, AL8_N2HDM, AM11SQ_N2HDM, &
        AM22SQ_N2HDM, AMSSQ_N2HDM
    integer IN2HDM, IPHASE
    double precision amhiggs(3)
    COMMON/MASSES_HDEC/AMS, AMC, AMB, AMT
    COMMON/STRANGE_HDEC/AMSB
    COMMON/MSBAR_HDEC/AMCB, AMBB
    COMMON/PARAM_HDEC/GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    COMMON/CKMPAR_HDEC/VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    COMMON/WZWDTH_HDEC/GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    COMMON/ALS_HDEC/XLAMBDA, AMC0, AMB0, AMT0, N0
    COMMON/FLAG_HDEC/IHIGGS, NNLO, IPOLE
    COMMON/ONSHELL_HDEC/IONSH, IONWZ, IOFSUSY
    COMMON/OLDFASH_HDEC/NFGG
    COMMON/THDM_HDEC/TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM, ITYPE2HDM, I2HDM, IPARAM2HDM
    Common/N2HDM_HDEC_PARAM/amhiggs, ALPH1_N2HDM, ALPH2_N2HDM, &
        ALPH3_N2HDM, AVS_N2HDM, &
        AL7_N2HDM, AL8_N2HDM, AM11SQ_N2HDM, &
        AM22SQ_N2HDM, AMSSQ_N2HDM, &
        IN2HDM, IPHASE

    double precision abrb, abrl, abrm, abrs, abrc, abrt, abrg, abrga, &
        abrzga, abrz, awdth
    double precision HCBRB, HCBRL, HCBRM, HCBRBU, HCBRS, HCBRC, HCBRT, &
        HCBRW, HCBRA, HCWDTH
    double precision hcbrcd, hcbrts, hcbrtd
    double precision hcbrwhh, hhbrchch, hlbrchch, abrhhaz, abrhawphm
    double precision h2brh1h1, h3brh1h1, h3brh2h2, h3brh1h2, &
        hibrt(3), hibrb(3), hibrl(3), hibrm(3), hibrs(3), hibrc(3), &
        hibrg(3), hibrga(3), hibrzga(3), hibrw(3), hibrz(3), &
        hibrhpwm(3), hibraz(3), hibrchch(3), hibraa(3), &
        gamtot(3)
    double precision hcbrwh(3), abrhz(3)
    COMMON/WIDTHA_HDEC/ABRB, ABRL, ABRM, ABRS, ABRC, ABRT, ABRG, ABRGA, &
        ABRZGA, ABRZ, AWDTH
    COMMON/WIDTHHC_HDEC/HCBRB, HCBRL, HCBRM, HCBRBU, HCBRS, HCBRC, HCBRT, &
        HCBRW, HCBRA, HCWDTH
    COMMON/WIDTH_HC_ADD/hcbrcd, hcbrts, hcbrtd
    COMMON/WIDTH_2HDM/hcbrwhh, hhbrchch, hlbrchch, abrhhaz, abrhawphm
    Common/N2HDM_BR_H/h2brh1h1, h3brh1h1, h3brh2h2, h3brh1h2, &
        hibrt, hibrb, hibrl, hibrm, hibrs, hibrc, &
        hibrg, hibrga, hibrzga, hibrw, hibrz, &
        hibrhpwm, hibraz, hibrchch, hibraa, &
        gamtot
    Common/N2HDM_BR_Hc_A/hcbrwh, abrhz

    double precision alsmz, acc, del, ambl, ambu, fmb, xmb
    double precision runm_hdec, xitla_hdec
    integer nloop, nber

    ITYPE2HDM = 1
    IPHASE = 4
    AMHA2HDM = mA
    AMHC2HDM = mHc
    amhiggs(1) = mHDD
    amhiggs(2) = mHDS
    amhiggs(3) = mHsm
    AM22SQ_N2HDM = m22sq
    AMSSQ_N2HDM = mssq

    ALSMZ = smparams(1)
    AMS = smparams(2)
    AMC = smparams(3)
    AMB = smparams(4)
    AMT = smparams(5)
    AMTAU = smparams(6)
    AMMUON = smparams(7)
    ALPH = smparams(8)
    GF = smparams(9)
    GAMW = smparams(10)
    GAMZ = smparams(11)
    AMZ = smparams(12)
    AMW = smparams(13)
    VTB = smparams(14)
    VTS = smparams(15)
    VTD = smparams(16)
    VCB = smparams(17)
    VCS = smparams(18)
    VCD = smparams(19)
    VUB = smparams(20)
    VUS = smparams(21)
    VUD = smparams(22)

    IHIGGS = 5
    I2HDM = 1
    IN2HDM = 1
    IPARAM2HDM = 1
    AMHL2HDM = 10
    AMHH2HDM = 10
    A1LAM2HDM = 1
    A2LAM2HDM = 1
    A3LAM2HDM = 1
    A4LAM2HDM = 1
    A5LAM2HDM = 1
    NNLO = 1
    IONSH = 0
    IONWZ = 0
    IPOLE = 0
    IOFSUSY = 1
    NFGG = 5

! **********************************************************************
! ***************   initialize alpha_s and polylogs    *****************
    ALPH = 1.D0/ALPH
    AMSB = AMS
    AMCB = AMC
    AMBB = AMB

    AMC0 = AMC
    AMB0 = AMB
    AMT0 = AMT
    ACC = 1.D-10
    NLOOP = 3
    XLAMBDA = XITLA_HDEC(NLOOP, ALSMZ, ACC)
    N0 = 5
    CALL ALSINI_HDEC(ACC)
!--DECOUPLING THE TOP QUARK FROM ALPHAS
    AMT0 = 3.D8

    del = 1.d-10
    ambl = ambb
    ambu = 2*ambb
    fmb = (ambl + ambu)/2
    acc = 1.d-10
    nloop = 3
12  amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    amc0 = amc
    n0 = 5
    call alsini_hdec(acc)
    xmb = runm_hdec(ambb, 5)
    if (abs(xmb - ambb) .lt. del) then
        ambl = fmb
        ambu = fmb
    elseif (xmb .gt. ambb) then
        ambu = fmb
    else
        ambl = fmb
    endif
    fmb = (ambl + ambu)/2
    if (dabs(xmb/ambb - 1) .gt. del) goto 12
    amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    n0 = 5
    call alsini_hdec(acc)
!--INITIALIZE COEFFICIENTS FOR POLYLOGARITHMS
    NBER = 18
    CALL BERNINI_HDEC(NBER)

    CALL HDEC_N2HDM()
    BR = (/gamtot(3), hibrb(3), hibrl(3), hibrm(3), hibrs(3), &
           hibrc(3), hibrt(3), hibrg(3), hibrga(3), hibrzga(3), &
           hibrw(3), hibrz(3), &
           h3brh1h1, hibraa(3), hibrchch(3), h3brh2h2, &
           gamtot(1), hibraz(1), hibrhpwm(1), &
           awdth, abrhz(1), abrhawphm, &
           hcwdth, hcbrwh(1), hcbra/)
    return
end
